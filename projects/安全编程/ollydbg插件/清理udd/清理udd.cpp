// 清理udd.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"

#include "plugin.h"

#pragma comment(lib,"ollydbg.lib")
#define PLUGINNAME L"clean udd"  // 插件名称
#define MY_VERSION L"0.00.01"       // 插件版本
t_table   g_stcHelloWorld = { 0 };


/**********************************************************/
/* 函数名：插件信息查询函数【回调函数】
/* 说  明：    此函数是必须存在的函数，也是OllyDBG第1个调用的函数。此函数负
/*         责检查当前的OllyDBG版本是否可以运行此插件，如果不能运行则需要返
/*         回0 。
/* 参  数：
/*   int           ollydbgversion                       : [ IN] OllyDBG的版本
/*   ulong      *features                                : [ IN] 包含一些额外的版本相关信息
/*   wchar_t  pluginname[SHORTNAME]    : [OUT] 插件名称
/*   wchar_t  pluginversion[SHORTNAME] : [OUT] 插件版本
/* 返回值：
/*   int : 返回所需要的API版本，如不匹配则返回0
/**********************************************************/
extc int __cdecl ODBG2_Pluginquery(int ollydbgversion, ulong *features,
	wchar_t pluginname[SHORTNAME], wchar_t pluginversion[SHORTNAME])
{
	// 1. 检查OllyDBG的兼容版本
	if (ollydbgversion < 201)
		return 0;
	// 2. 设置OllyDBG插件的名称与版本

	StrcopyW(pluginname, SHORTNAME, PLUGINNAME);
	StrcopyW(pluginversion, SHORTNAME, MY_VERSION); // 设置插件版本

	
	MessageBox(0, 0, 0, 0);
	// 3. 返回需要的API版本
	return PLUGIN_VERSION;
};

extc int __cdecl ODBG2_Plugininit(void) {

	//插件初始化时清理, 上次产生的bat文件
	DeleteFileW(L"{FB4F9819-FB2E-400D-97B6-E37D533685CA}");
	return 0;
};


#include <string>
#include <fstream>

int  MenuHandler_cleanudd(struct t_table *, wchar_t *, ulong, int  mode)
{
	if (mode == MENU_VERIFY)    // 第一次调用（一般执行初始化操作）
		return MENU_NORMAL;

	if (mode == MENU_EXECUTE) // 第二次调用（一般执行菜单逻辑操作）
	{

		char  uddpath[256]{};
		
		std::ofstream in;
		in.open("{FB4F9819-FB2E-400D-97B6-E37D533685CA}.bat", std::ios::trunc); //ios::trunc表示在打开文件前将文件清空,由于是写入,文件不存在则创建
		in.clear();

		char  currentpath[256];
		GetCurrentDirectoryA(256, currentpath);

		//结束od
		in << "taskkill  " << currentpath << "ollydbg.exe\n";


			//读取ini路径;
			strcat_s(currentpath + strnlen_s(currentpath, 256), 256, "\\ollydbg.ini");
			GetPrivateProfileStringA("History", "UDD path", " ", uddpath, 256, currentpath);


		//执行清理
		in << "del " << uddpath << "*.*  /f/s/q/a  \n";
		in << "start  "<< currentpath << "ollydbg.exe\n"; //重启od
		in.flush();
		in.close();



		STARTUPINFOA si{ sizeof(si) };
		PROCESS_INFORMATION pi{};

		//执行脚本文件.  参数1    udd路径   参数2及以后   od的当前启动命令行
		if (!CreateProcessA(NULL,   // No module name (use command line)
			"{FB4F9819-FB2E-400D-97B6-E37D533685CA}.bat",        // Command line
			NULL,           // Process handle not inheritable
			NULL,           // Thread handle not inheritable
			FALSE,          // Set handle inheritance to FALSE
			0,              // No creation flags
			NULL,           // Use parent's environment block
			NULL,           // Use parent's starting directory 
			&si,            // Pointer to STARTUPINFO structure
			&pi))
		{
			MessageBoxW(0, L"插件[clean udd ] 执行bat失败, 无法完成清理", 0, 0);

		};


		return MENU_NOREDRAW;
	}
	return MENU_ABSENT;
}


t_menu g_MainMenu = {

		L"15PB",
		L"haha",
	0,
	MenuHandler_cleanudd,
	0,
	0
};



//extern "C" _declspec(dllexport)
extc t_menu* ODBG2_Pluginmenu(wchar_t* type)
{
	if (!wcscmp(type, PWM_MAIN))
	{
		return &g_MainMenu;
	}
	return nullptr;
}