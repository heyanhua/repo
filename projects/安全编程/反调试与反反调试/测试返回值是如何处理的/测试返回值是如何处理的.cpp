// 测试返回值是如何处理的.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"





void  fun()
{
	int a = 101;
}

int  fun_retA()
{
	int a = 101;
	return 101;
}

char  fun_retChar()
{
	char a = 101;
	return  a;
}


int main()
{
	fun();

	int  ret = fun_retA();

	char  retchar = fun_retChar();
    return 0;
}

