// 检测调试条件.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <windows.h>
#include <stdint.h>

UCHAR  Getx86Flag_BeingDebugged()
{
	UCHAR isDebugged=0;
	__asm {
		mov  eax,fs:[0x30]
		mov  al, [eax + 0x2]
		mov  isDebugged,al
	}

	return  isDebugged;
}





typedef UCHAR (* call_shellcode )(void);
UCHAR  Getx64Flag_BeingDebugged()
{
	//原型   UCHAR (* call_shellcode )(void);
	unsigned char  Shellcode_Textx64Flag_BeingDebugged[]
	{ 	0x64, 0x48, 0x8B, 0x04,
		0x25, 0x60, 0x00, 0x00,	
		0x00, 0x8A, 0x40, 0x02, 0x5D };
	DWORD hehe = 0;
	VirtualProtect(Shellcode_Textx64Flag_BeingDebugged,
		sizeof(Shellcode_Textx64Flag_BeingDebugged),
		PAGE_EXECUTE_READWRITE, &hehe);
	return  ((call_shellcode)(void *)Shellcode_Textx64Flag_BeingDebugged)();	

}



int main()
{
	

	//编译为x86模式 执行
	if(Getx86Flag_BeingDebugged())
	MessageBox(0, TEXT("被调试"), 0, 0);
	else
		MessageBox(0, TEXT("没有被调试"), 0, 0);
	//编译为x64模式 执行
	//if (Getx64Flag_BeingDebugged())
	//	MessageBox(0,TEXT("被调试"),0,0);
	//else
	//	MessageBox(0, TEXT("没有被调试"), 0, 0);
    return 0;
}

