

template<typename T>
T* single_instance(void)
{
	static bool  cond{false};
	if(cond)
		return nullptr;
	else
		cond = true;
	return new T;
};

