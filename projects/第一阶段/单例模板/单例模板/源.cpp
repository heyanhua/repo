
#include <iostream>
#include"template.h"

//
class MyClass
{

public:
	~MyClass();
	 static MyClass * single_instance() {
		static bool  cond{ false };
		if (cond)
			return nullptr;
		else
			cond = true;
		return (new MyClass);
	};

private:
	MyClass();


};

MyClass::MyClass()
{
	std::cout << "hello,pangzi!";
}

MyClass::~MyClass()
{
	delete this;
}

void main()
{
	MyClass::single_instance();
	MyClass::single_instance();
}