
//用递归方式实现的可变参数模板
#include <tuple>
#include <iostream>
using std::cout;
using std::endl;

//递归终止函数---
void print()
{
	;
}

//展开函数   ----通过模板递归展开并打印参数包------------------
template<typename T, class... Args>
void print(T head, Args... tails)
{
	cout << head << endl;
	print(tails...);
};




//通过type_traits展开并打印参数包-----------------------------------------

//终止条件
template<std::size_t I = 0, typename Tuple>
typename std::enable_if<I == std::tuple_size< Tuple >::value> ::type printtp(Tuple t)
{
	;
}

template<std::size_t I = 0, typename Tuple>
typename std::enable_if<I < std::tuple_size< Tuple >::value> ::type printtp(Tuple t)
{
	cout << std::get<I>(t) << std::endl;
	printtp<I + 1>(t);
}

template<typename... Args>
void printp(Args... args)
{
	printtp(std:: make_tuple(args...));
}


////通过逗号表达式和初始化列表展开参数包
//template<typename T>
//void printarg(T t)
//{
//	cout << t << endl;
//}

template<typename... Args>
void expand(Args... args)
{
	std::initializer_list<int>{ 
		(
		[&](){ cout << args; }(),
		0)... };
}



int main(void)
{
//	 printp(1, 2, 3, 4, "12345");
	 expand(1, 2, 3, 4);
}