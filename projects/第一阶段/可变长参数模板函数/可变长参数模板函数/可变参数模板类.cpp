#include <tuple>
using std::tuple;

#include <vector>
using std::vector;

#include <map>
using std::pair;

#include <string>
using std::string;



//step 0:  可变参模板类的前向声明
//前向声明要求了 container模板的参数至少要有一个
template<typename... _Types>  struct container;


//step 1:  可变参数模板类的基本定义
template<typename _This, typename... _Rest>
struct container<_This, _Rest...>
{
	//计算出的_Size 是编译器的byte总个数
	enum { _Size = container<_This>:: value  
				 + container<_Rest...>::value };

};

//step 2:  特化的递归终止类模板  ----展开到没有参数时终止
template<>
struct container<>
{
	 ;
};

//== step 2:  特化的递归终止类模板   -- 展开到最后一个参数时终止
template<typename T>
struct container<T>
{

};

//== step 2:  特化的递归终止类模板   -- 展开到最后两个参数时终止
template<typename _This, typename _Other>
struct container< _This, _Other>
{

};



void fun()
{
	tuple<int, double, vector<pair<int, int> >,
					   vector<int> ,  string>  tp;
	
	tuple<> tp1;
}