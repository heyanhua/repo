

void main()
{
	int const icNumber = 100; //语句1
	const int ciNumber = 100; //语句2  和 语句1 相同
	const int const const const const  cc = 100;  //详情看编译器
	//const 只能修饰已经存在的类型，  强调 类型。 被明确定义的
	//eg
	int p const = 100;  //错误！！！
	//然而
	typedef int ti;
	ti const hehe = 100; //正确, const 修饰的是 ti;

	int const *  pc = &ciNumber;
	const int  const * const pcc = &ciNumber;
	//  修饰int 修饰*   修饰 *   修饰*  修饰*    修饰 *
	// 说明语法层 有一个字段，修饰  类型是否为const  比如  int const x;   //显示告知为int  const属性
	// int  z; 默认非const属性。
	// 类型的const属性， 默认为false，  使用后置 const 关键字使其变为true,
	int const * const * const * const * const *  const pccc   = ;
	
	
}