class A
{
	int a;
	static void non_public_fun() {
		//	a = 11; //error , 没有权限访问. logic: static属于类而不属于对象, 
		//它可以不存在对象的情况下直接被调用
		//所以访问时, 对象可能不存在, 因此它无法直接访问类内的非静态变量.
	};
public:
	//static 函数只能访问 static对象, 因为他们都属于类,而不属于具体的对象.
	static void public_fun() {
		ss = 1;

	};
	static int  ss;
};
 int A::ss = 0;

class B : public A
{
public:
	static void public_fun() {
		ss = 100;
	};
};

void main()
{
	//访问方法:  类名->::静态函数();
	//受类的访问控制,

	A::public_fun();  //1. 受访问控制限制
	B::public_fun();  //2. 可以被继承
					  //3. 可以被覆盖
					  //	A::non_public_fun(); //error, 无访问权限

					  //
	A::ss = 8;		  //3.  可以加上作用于直接访问

}