

#include<iostream>
using std::cout;
using std::endl;
//目的; 测试赋值构造的次序
//究竟是先默认构造,再调用赋值操作
//还是直接调用copy构造
//又或者是别的

class myclass
{
public:
	myclass(){
		cout <<__LINE__<<"行"<< __FUNCSIG__<<endl;
	};

	myclass(myclass &const ref_obj) {
		cout << __LINE__ << "行" << __FUNCSIG__ << endl;
	};

	myclass & operator = (myclass &const ref_obj)
	{
		cout << __LINE__ << "行" << __FUNCSIG__ << endl;
		return *this;
	}

	myclass & move(myclass &const ref_obj)
	{

	};
	myclass & swap(myclass & obj)
	{

	}
	//ref_obj转移到*this,浅copy, ref_obj置0
	myclass & transfer(myclass & ref_obj)
	{

	}
};







int main()
{
	//测试情况1, B在构造时是否调用了
	myclass A;  //无参构造
	myclass B = A;  //copy构造
	A = B;  //赋值操作
	//结果, B自身没有调用无参构造函数.
	//B调用了copy constractor
	//B也没有调用赋值 = 
	//就是一个简单的copy constractor
	return 0;
}

