#ifndef WIN_HELPER_HPP
#define WIN_HELPER_HPP


#include "cpp_helper.hpp"
#include <windows.h>


//更好用的格式化字符串---------------------Begin
template <typename FIRST, typename... TAIL>
_tstring & to_string(_tstring&  str,  FIRST const &  first,  TAIL const &... tail)
{
	//funs;@@
	return to_string(to_string(str, first), tail...);
}

//变参展开过程以及终止条件
template <typename LAST>
_tstring & to_string(_tstring&  str, LAST const&  last)
{
	_tstringstream   sstream;
	sstream << str.c_str();
	sstream << last;
	sstream >> str;
	return str;
}


//更好用的格式化字符串---------------------End


#ifndef DLL_EXPORT
#define DECLDIR __declspec(dllimport)
#else
#define DECLDIR __declspec(dllexport)
#endif

template <typename FIRST, typename... TAIL>
VOID  WINAPI OutputDebugString(const FIRST & first, const TAIL&... tail)
{
	_tstring  str;
	to_string(str, first, tail...);
	OutputDebugString(str.c_str());
}

#endif 