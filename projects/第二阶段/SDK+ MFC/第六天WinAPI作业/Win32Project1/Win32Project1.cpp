// Win32Project1.cpp : 定义应用程序的入口点。
//

#include "stdafx.h"
#include "win_helper.hpp"
#include "Win32Project1.h"
#include  <CommCtrl.h>
#include  <vector>
using std::vector;
#pragma comment(lib,"Comctl32.lib")

#define MAX_LOADSTRING 100
#define ID_LISTVIEW  10001

struct wininfo_DrivesInfo
{
	struct ItemInfo
	{
	public:

		ItemInfo() :LogicalDriveString(256, 0),
			Title(256, 0), Desicrib(256, 0),
			FileSystemName(256, 0), DriveTypeStr(256, 0),
			FreeSpaceStr(256, 0),
			AllSpaceStr(256, 0),
			FileSystemFlag(0), FreeSpace(0),
			AllSpace(0), MaximumComponentLength(0)
		{

		};
	public:
		enum  K_CONTAINER
		{
			BYTE_TO_GB = 1024 * 1024 * 1024   
		};
		_tstring  LogicalDriveString; //盘符
		DWORD     DriveType;
		_tstring  DriveTypeStr;
		_tstring  Title;//标题
		DWORD     FileSystemFlag; //文件系统标志
		_tstring  FileSystemName;
		unsigned long long     FreeSpace;
		_tstring  FreeSpaceStr;
		_tstring  AllSpaceStr;
		unsigned long long     AllSpace;
		DWORD     MaximumComponentLength; //最大文件名长度
		_tstring  Desicrib;
	};
public:
	wininfo_DrivesInfo(int Init_Type = 0) {
	//Init_TYPE == 0, 默认就地初始化.   //Init_TYPE != 0; 在适当的时机调用Init();手动进行初始化.
	if (0 == Init_Type)
		Init(); };
	void Init()
	{
		_tstring buf(256, 0);
		//1.  获取逻辑驱动器
		GetLogicalDriveStrings(256, (LPWSTR)buf.c_str());
		auto pDrive = buf.c_str();
		auto nType = 0;//磁盘类型




	    int index = 0;
		for (int i = 0; pDrive[i] != TEXT('\0') && i < MAX_PATH; i++)
		{
			

			m_Data.emplace_back();
			DWORD dwSectorsPerCluster = 0;
			DWORD dwBytesPerSector = 0;
			DWORD dwlpNumberOfFreeClusters = 0;
			DWORD dwlpTotalNumberOfClusters = 0;

			//获取总空间和 空间空间
			GetDiskFreeSpace(&pDrive[i],
				&dwSectorsPerCluster,
				&dwBytesPerSector,
				&dwlpNumberOfFreeClusters,
				&dwlpTotalNumberOfClusters);


			//获取标题, 获取磁盘类型，　获取磁盘名字
			GetVolumeInformation(&pDrive[i],
				(LPWSTR)m_Data[index].Title.c_str(),
				256, NULL, &m_Data[index].MaximumComponentLength,
				&m_Data[index].FileSystemFlag,
				(LPWSTR)m_Data[index].FileSystemName.c_str(),
				256);


			m_Data[index].LogicalDriveString = &pDrive[i];
			m_Data[index].FreeSpace = dwlpNumberOfFreeClusters *
				dwSectorsPerCluster *  dwBytesPerSector;
			to_string(m_Data[index].FreeSpaceStr, m_Data[index].FreeSpace / ItemInfo::BYTE_TO_GB, TEXT("GB"));
			m_Data[index].AllSpace = dwlpTotalNumberOfClusters *
				dwSectorsPerCluster *  dwBytesPerSector;
			to_string(m_Data[index].AllSpaceStr, m_Data[index].AllSpace / ItemInfo::BYTE_TO_GB, TEXT("GB"));
			m_Data[index].DriveType = GetDriveType(&pDrive[i]); //获取磁盘驱动器类型
														
			switch (m_Data[index].DriveType)
			{
			case DRIVE_UNKNOWN:
				m_Data[index].Desicrib = TEXT("未知驱动器");
				break;
			case DRIVE_NO_ROOT_DIR:
				m_Data[index].Desicrib = TEXT("没有根目录,可能是没有挂在");
				break;
			case DRIVE_REMOVABLE:
				m_Data[index].Desicrib = TEXT("可移动磁盘");
				break;
			case DRIVE_FIXED:
				m_Data[index].Desicrib = TEXT("本地硬盘");
				break;
			case DRIVE_REMOTE:
				m_Data[index].Desicrib = TEXT("远程驱动器");
				break;
			case DRIVE_CDROM:
				m_Data[index].Desicrib = TEXT("CD_ROM");
				break;
			case DRIVE_RAMDISK:
				m_Data[index].Desicrib = TEXT("DRIVE_RAMDISK");
			default:
				break;
			}
			i += _tcsclen(pDrive);
			index++;
		}

		//--------------逻辑初始化完毕

	};
	vector<ItemInfo>  m_Data;						// 磁盘信息
};


// 全局变量: 
HINSTANCE hInst;                                // 当前实例
WCHAR szTitle[MAX_LOADSTRING];                  // 标题栏文本
WCHAR szWindowClass[MAX_LOADSTRING];            // 主窗口类名
wininfo_DrivesInfo  g_DrivesInfo;

// 此代码模块中包含的函数的前向声明: 
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 在此放置代码。

    // 初始化全局字符串
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_WIN32PROJECT1, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // 执行应用程序初始化: 
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WIN32PROJECT1));

    MSG msg;

    // 主消息循环: 
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WIN32PROJECT1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_WIN32PROJECT1);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释: 
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{

	//逻辑初始化
	//------------------初始化全局变量
   

   hInst = hInstance; // 将实例句柄存储在全局变量中

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的:    处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE:
	{
		//初始化创建列表框
		RECT rt = {};
		GetClientRect(hWnd, &rt);
		InitCommonControls();
		HWND hList = CreateWindow(WC_LISTVIEW, _T("功能"),
			WS_CHILD | WS_VISIBLE | LVS_REPORT | WS_BORDER,
			0, 0, rt.right, rt.bottom, hWnd, (HMENU)ID_LISTVIEW, hInst, NULL);

		//插入多列
		LVCOLUMN  lc;
		lc.mask = LVCF_WIDTH | LVCF_TEXT;

		lc.cx = (rt.right - rt.left) / 6;
		lc.cchTextMax = 10;
		lc.pszText = TEXT("盘符");
		ListView_InsertColumn(hList, 0, &lc);
		lc.pszText = TEXT("标题");
		ListView_InsertColumn(hList, 1, &lc);
		lc.pszText = TEXT("文件系统");
		ListView_InsertColumn(hList, 2, &lc);
		lc.pszText = TEXT("总空间");
		ListView_InsertColumn(hList, 3, &lc);
		lc.pszText = TEXT("可用空间");
		ListView_InsertColumn(hList, 4, &lc);
		lc.pszText = TEXT("描述");
		ListView_InsertColumn(hList, 5, &lc);
		//插入多行
		LVITEM  li = {};
		for (size_t i = 0; i < g_DrivesInfo.m_Data.size();i++)
		{
			ListView_InsertItem(hList, &li);
			ListView_InsertItem(hList, &li);
			ListView_InsertItem(hList, &li);
			ListView_InsertItem(hList, &li);
			ListView_InsertItem(hList, &li);
			ListView_InsertItem(hList, &li);
		}

		auto index = 0;
		for (auto item : g_DrivesInfo.m_Data)
		{
			 
			ListView_SetItemText(hList, index, 0, (LPWSTR)item.LogicalDriveString.c_str());
			ListView_SetItemText(hList, index, 1, (LPWSTR)item.Title.c_str());
			ListView_SetItemText(hList, index, 2, (LPWSTR)item.FileSystemName.c_str());
			ListView_SetItemText(hList, index, 3, (LPWSTR)item.FreeSpaceStr.c_str());
			ListView_SetItemText(hList, index, 4, (LPWSTR)item.AllSpaceStr.c_str());
			ListView_SetItemText(hList, index, 5, (LPWSTR)item.Desicrib.c_str());

			index++;

		}


	}
	break;
	case WM_SIZE:
		MoveWindow(GetDlgItem(hWnd, ID_LISTVIEW), 0, 0,LOWORD(lParam), HIWORD(lParam),FALSE);
		break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // 分析菜单选择: 
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: 在此处添加使用 hdc 的任何绘图代码...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


