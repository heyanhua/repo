

//实际操作的函数 ----放在需要捕获变量的作用域中,
auto real = [&](LPVOID pParam)->DWORD {
	//Do something...
	return 0;
};
using type_real_func = decltype(real);

//这个匿名函数按照回调函数签名编写,
auto fun = [](LPVOID pParam) ->DWORD {
	//lambda表达式的语法糖, 
	return (*((type_real_func *)0))(pParam);
};
