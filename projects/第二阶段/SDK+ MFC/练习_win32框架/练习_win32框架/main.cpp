

#include<windows.h>

TCHAR

LRESULT CALLBACK WindowProc(
	_In_ HWND   hwnd,
	_In_ UINT   uMsg,
	_In_ WPARAM wParam,
	_In_ LPARAM lParam
	);

int CALLBACK WinMain(
	_In_ HINSTANCE hInstance,
	_In_ HINSTANCE hPrevInstance,
	_In_ LPSTR     lpCmdLine,
	_In_ int       nCmdShow
	)
{
	//1. 填充窗口类
	WNDCLASS  wnd_class={};
	wnd_class.lpszClassName = TEXT("{4C00AEFD-A7F0-40F8-90AF-575C844ECC14}");
	wnd_class.lpfnWndProc = WindowProc;


	//2. 注册窗口类
	if (0==RegisterClass(&wnd_class))
	{
		OutputDebugString(TEXT("RegisterClass Error!\nWinMain return!"));
		return 0;
	}
	
	//3. 创建窗口
	HWND hWnd =  CreateWindow(wnd_class.lpszClassName, 
		TEXT("{ 9FCA3EAE - 4F88 - 4DB9 - A493 - 3DD1EFBCE0B8 }"),
		NULL,0,0,600,800,NULL,NULL,hInstance,NULL);
	if(NULL==hWnd)
	{
		OutputDebugString(TEXT("CreateWindow Error!\nWinMain return!"));
		return 0;
	}
	
	//4. 更新并显示窗口
	UpdateWindow(hWnd);
	ShowWindow(hWnd, SW_SHOW);

	//5.获取并处理消息
	MSG  msg = {};

	HACCEL hAccelTable = LoadAccelerators(hInstance, NULL);
	while(GetMessage(&msg, hWnd ,NULL, NULL))
	{
		//if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		//{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		//}
	}
	return 0;
}



LRESULT CALLBACK WindowProc(
	_In_ HWND   hwnd,
	_In_ UINT   uMsg,
	_In_ WPARAM wParam,
	_In_ LPARAM lParam
	)
{
	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}