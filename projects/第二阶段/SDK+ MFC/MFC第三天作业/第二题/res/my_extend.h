#pragma once


// my_extend

class my_extend : public CTabCtrl
{
	DECLARE_DYNAMIC(my_extend)

public:
	my_extend();
	virtual ~my_extend();
	void my_tree_create(CTreeCtrl &TREE_1, CImageList &image_list);
protected:
	DECLARE_MESSAGE_MAP()
	
};


