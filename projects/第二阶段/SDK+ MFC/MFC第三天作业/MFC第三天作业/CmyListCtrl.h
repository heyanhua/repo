#pragma once
#include <vector>
using  std::vector;

// CmyListCtrl

class CmyListCtrl : public CListCtrl
{
	DECLARE_DYNAMIC(CmyListCtrl)

public:
	CmyListCtrl();
	virtual ~CmyListCtrl();

	int InsertItems(DWORD  dwCount, int x,
		int y, LPCTSTR text,
		...);

protected:
	DECLARE_MESSAGE_MAP()
};	


