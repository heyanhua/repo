// CmyListCtrl.cpp : 实现文件
//

#include "stdafx.h"
#include "MFC第三天作业.h"
#include "CmyListCtrl.h"


// CmyListCtrl

IMPLEMENT_DYNAMIC(CmyListCtrl, CListCtrl)

CmyListCtrl::CmyListCtrl()
{

}

CmyListCtrl::~CmyListCtrl()
{
}

int CmyListCtrl::InsertItems(DWORD dwCount, int x, int y, LPCTSTR text, ...)
{
	va_list  vl;
	va_start(vl, dwCount);
	LVITEM  li = {};
	li.mask = LVCF_WIDTH | LVCF_TEXT;
	for (size_t i = 0; i < dwCount; i++)
	{  
		auto _x = va_arg(vl, int);
		auto _y = va_arg(vl, int);
		auto _p = va_arg(vl, LPCTSTR);
		//while (GetItemCount()<= _x)
		//	InsertItem(&li);
		SetItemText(_x, 
			_y,
			_p);

	}
	va_end(vl);
	return 0;
}




BEGIN_MESSAGE_MAP(CmyListCtrl, CListCtrl)
END_MESSAGE_MAP()



// CmyListCtrl 消息处理程序


