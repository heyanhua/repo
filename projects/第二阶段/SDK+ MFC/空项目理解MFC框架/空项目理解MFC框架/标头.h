#pragma once


#include <afxWin.h>


class CMFCApplication1App : public CWinApp
{

	

public:
	//重写Init执行初始化
	virtual BOOL InitInstance();

	
    //为类消息节点
	DECLARE_MESSAGE_MAP()

	virtual int Run();
	virtual void DoWaitCursor(int nCode);
	virtual int ExitInstance();
};


CMFCApplication1App  theApp;
