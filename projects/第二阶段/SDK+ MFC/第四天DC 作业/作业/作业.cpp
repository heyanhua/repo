// 作业.cpp : 定义应用程序的入口点。
//

#include "stdafx.h"
#include "作业.h"

#define MAX_LOADSTRING 100
#define DRAW_LINE     10000
#define DRAW_RECT     10001
#define DRAW_ROUND    10002
#define DRAW_ANY	  10003		  

// 全局变量: 
HINSTANCE hInst;                                // 当前实例
WCHAR szTitle[MAX_LOADSTRING];                  // 标题栏文本
WCHAR szWindowClass[MAX_LOADSTRING];            // 主窗口类名

// 此代码模块中包含的函数的前向声明: 
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 在此放置代码。

    // 初始化全局字符串
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MY, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // 执行应用程序初始化: 
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY));

    MSG msg;

    // 主消息循环: 
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MY);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释: 
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // 将实例句柄存储在全局变量中

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的:    处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static int check_id = 0;
	static int x, y;
    switch (message)
    {
	case WM_MOUSEMOVE:
		if (DRAW_ANY == check_id)
		{
			HDC hdc = GetDC(hWnd);
			SetPixel(hdc, LOWORD(lParam), HIWORD(lParam),0);
			ReleaseDC(hWnd, hdc);
		}
		break;
	case WM_LBUTTONDOWN:
		x = LOWORD(lParam); 
		y = HIWORD(lParam);
		break;
	case WM_LBUTTONUP:
		switch (check_id)
		{
			break;
		case DRAW_LINE:
		{
			HDC hdc = GetDC(hWnd);
			MoveToEx(hdc, x, y, 0);
			LineTo(hdc, LOWORD(lParam), HIWORD(lParam));
			ReleaseDC(hWnd, hdc);
		}
			break;
		case DRAW_RECT:
		{
			HDC hdc = GetDC(hWnd);
			Rectangle(hdc, x,y, LOWORD(lParam), HIWORD(lParam));
			ReleaseDC(hWnd, hdc);
		}
			break;
		case DRAW_ROUND:
		{
			HDC hdc = GetDC(hWnd);
			Ellipse(hdc, x, y, LOWORD(lParam), HIWORD(lParam));
			ReleaseDC(hWnd, hdc);
		}
			break;
		}

		break;
	
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // 分析菜单选择: 
            switch (wmId)
            {
			case DRAW_ANY:
			case DRAW_LINE:
			case DRAW_RECT:
			case DRAW_ROUND:
				if (BN_CLICKED == HIWORD(wParam))
					check_id = wmId;
				break;
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: 在此处添加使用 hdc 的任何绘图代码...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
	case WM_CREATE:
		//创建按钮
		CreateWindow(
			TEXT("button"),
			TEXT("画线"),
			WS_CHILD | WS_VISIBLE |BS_AUTORADIOBUTTON,
			0, 0, 55, 20,
			hWnd,
			(HMENU)DRAW_LINE,
			NULL,
			NULL
			);
		//创建按钮
		CreateWindow(
			TEXT("button"),
			TEXT("矩形"),
			WS_CHILD | WS_VISIBLE| BS_AUTORADIOBUTTON,
			80, 0, 55, 20,
			hWnd,
			(HMENU)DRAW_RECT,
			NULL,
			NULL
			);
		//创建按钮
		CreateWindow(
			TEXT("button"),
			TEXT("圆形"),
			WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
			160, 0, 55, 20,
			hWnd,
			(HMENU)DRAW_ROUND,
			NULL,
			NULL
			);
		//创建按钮
		CreateWindow(
			TEXT("button"),
			TEXT("自由绘制"),
			WS_CHILD | WS_VISIBLE | BS_AUTORADIOBUTTON,
			240, 0, 100, 20,
			hWnd,
			(HMENU)DRAW_ANY,
			NULL,
			NULL
			);

		break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
