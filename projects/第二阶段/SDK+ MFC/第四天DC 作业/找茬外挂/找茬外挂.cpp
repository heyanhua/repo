// 找茬外挂.cpp : 定义应用程序的入口点。
//

#include "stdafx.h"
#include "Shellapi.h"
#include "win_helper.hpp"
#include <vector>
using std::vector;
#include "找茬外挂.h"

#define MAX_LOADSTRING 100

// 全局变量: 
HINSTANCE hInst;                                // 当前实例
WCHAR szTitle[MAX_LOADSTRING];                  // 标题栏文本
WCHAR szWindowClass[MAX_LOADSTRING];            // 主窗口类名

// 此代码模块中包含的函数的前向声明: 
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 在此放置代码。

    // 初始化全局字符串
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MY, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // 执行应用程序初始化: 
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY));

    MSG msg;

    // 主消息循环: 
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = NULL;
    wcex.hCursor        = NULL;
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = NULL;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = NULL;

    return RegisterClassExW(&wcex);
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释: 
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // 将实例句柄存储在全局变量中

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的:    处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

	static _tstring file_nameA;
	file_nameA.resize(256, 0);
	static _tstring file_nameB(file_nameA);


    switch (message)
    {
	case WM_KEYDOWN:
	{
		if (VK_RETURN == wParam)
		{
			RECT		rc = {};
			vector<POINT> vt;
			HDC  hdc = GetDC(hWnd);
			GetClientRect(hWnd, &rc);
			
			for (int i = 0; i < rc.right / 2; i++)
				for (int j = 0; j < rc.bottom; j++)
				{
					DWORD left = GetPixel(hdc, j, i);
					DWORD right = GetPixel(hdc, j + rc.right / 2 + 1, i);
					if (left != right)
					{
						POINT pt; pt.x = i;  pt.y = j;
						vt.push_back(pt);
					}
				}


		}
	}

		break;
	case WM_DROPFILES:
	{


		DragQueryFile((HDROP)wParam, 0, (LPWSTR)file_nameA.c_str(), 256);
		DragQueryFile((HDROP)wParam, 1, (LPWSTR)file_nameB.c_str(), 256);
		POINT pt;
		DragQueryPoint((HDROP)wParam,&pt);
		DragFinish((HDROP)wParam);
		//获取图片完毕
		break;
	}
	case WM_CREATE:

		 DragAcceptFiles(  hWnd, TRUE  );
		break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // 分析菜单选择: 
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: 在此处添加使用 hdc 的任何绘图代码...
			HDC hmem_dc = CreateCompatibleDC(hdc); //创建当前DC兼容DC
			BITMAP      bt_info;
			RECT		rc = {};
			GetClientRect(hWnd, &rc);

			HBITMAP hbmp = (HBITMAP)LoadImage(NULL, (LPWSTR)file_nameA.c_str(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

			SelectObject(hmem_dc, hbmp); //选进图片到兼容DC
			GetObject(hbmp, sizeof(BITMAP), &bt_info);
			StretchBlt(hdc, 0, 0, rc.right/2 - 1, rc.bottom,
				hmem_dc, 0, 0,bt_info.bmWidth,bt_info.bmHeight, SRCCOPY);

			//处理第二张图
			hbmp = (HBITMAP)LoadImage(NULL, (LPWSTR)file_nameB.c_str(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			// hbmp = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_BITMAP3));
			SelectObject(hmem_dc, hbmp); //选进图片到兼容DC
			GetObject(hbmp, sizeof(BITMAP), &bt_info);
			StretchBlt(hdc, rc.right / 2 + 1 , 0, rc.right / 2 - 1, rc.bottom,
				hmem_dc, 0, 0, bt_info.bmWidth, bt_info.bmHeight, SRCCOPY);

			DeleteDC(hmem_dc);
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
