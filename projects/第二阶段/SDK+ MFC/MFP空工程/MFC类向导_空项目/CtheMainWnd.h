#pragma once
#include <afxwin.h>
#include <string>
using std::string;
#include "afxdialogex.h"



// CtheMainWnd 对话框

class CtheMainWnd : public CDialogEx
{
	DECLARE_DYNAMIC(CtheMainWnd)

public:
	CtheMainWnd(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CtheMainWnd();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton_Number1();
	afx_msg void OnBnClickedButton_Number2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();
	afx_msg void OnBnClickedButton8();
	afx_msg void OnBnClickedButton9();
	afx_msg void OnBnClickedButton0();
	afx_msg void OnBnClickedButton_Add();
	afx_msg void OnBnClickedButton_Sub();
	afx_msg void OnBnClickedButton_Mul();
	afx_msg void OnBnClickedButton_Dev();
	afx_msg void OnBnClickedButton_Equ();
	// 第一个操作数
	long m_OperandFirst;
	// 第二个操作数
	long m_OperandSecond;
	// 操作符号
	char m_Operator;
	// 结果
	long long m_Equ;
//	long *Operand;
	bool isFirstOpeand;
private:
	// 设置操作数
	void SetOperand(long i);
public:
	// 用于更新结果
	string OutStr;
};
