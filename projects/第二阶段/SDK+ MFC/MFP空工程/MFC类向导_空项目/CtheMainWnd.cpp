// CtheMainWnd.cpp : 实现文件
//

#include "stdafx.h"
#include "CtheMainWnd.h"
#include "resource.h"
#include "afxdialogex.h"
#include <string>
using std::wstring;
using std::to_wstring;


// CtheMainWnd 对话框

IMPLEMENT_DYNAMIC(CtheMainWnd, CDialogEx)

CtheMainWnd::CtheMainWnd(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DIALOG1, pParent)
	, m_OperandFirst(0)
	, m_OperandSecond(0)
	, m_Operator(0)
	, m_Equ(0)
	, OutStr()
{

}

CtheMainWnd::~CtheMainWnd()
{
}

void CtheMainWnd::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CtheMainWnd, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON1, &CtheMainWnd::OnBnClickedButton_Number1)
	ON_BN_CLICKED(IDC_BUTTON4, &CtheMainWnd::OnBnClickedButton_Number2)
	ON_BN_CLICKED(IDC_BUTTON5, &CtheMainWnd::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON2, &CtheMainWnd::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON3, &CtheMainWnd::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON8, &CtheMainWnd::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON6, &CtheMainWnd::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON7, &CtheMainWnd::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON9, &CtheMainWnd::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON15, &CtheMainWnd::OnBnClickedButton0)
	ON_BN_CLICKED(IDC_BUTTON10, &CtheMainWnd::OnBnClickedButton_Add)
	ON_BN_CLICKED(IDC_BUTTON13, &CtheMainWnd::OnBnClickedButton_Sub)
	ON_BN_CLICKED(IDC_BUTTON11, &CtheMainWnd::OnBnClickedButton_Mul)
	ON_BN_CLICKED(IDC_BUTTON12, &CtheMainWnd::OnBnClickedButton_Dev)
	ON_BN_CLICKED(IDC_BUTTON14, &CtheMainWnd::OnBnClickedButton_Equ)
END_MESSAGE_MAP()


// CtheMainWnd 消息处理程序







void CtheMainWnd::OnBnClickedButton_Number1()
{
	// TODO: 在此添加控件通知处理程序代码
	SetOperand(1);
}


void CtheMainWnd::OnBnClickedButton_Number2()
{
	// TODO: 在此添加控件通知处理程序代码
	SetOperand(2);
}


void CtheMainWnd::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码
	SetOperand(3);
}


void CtheMainWnd::OnBnClickedButton4()
{
	// TODO: 在此添加控件通知处理程序代码
	SetOperand(4);
}


void CtheMainWnd::OnBnClickedButton5()
{
	// TODO: 在此添加控件通知处理程序代码
	SetOperand(5);
}


void CtheMainWnd::OnBnClickedButton6()
{
	// TODO: 在此添加控件通知处理程序代码
	SetOperand(6);
}


void CtheMainWnd::OnBnClickedButton7()
{
	// TODO: 在此添加控件通知处理程序代码
	SetOperand(7);
}


void CtheMainWnd::OnBnClickedButton8()
{
	// TODO: 在此添加控件通知处理程序代码
	SetOperand(8);
}


void CtheMainWnd::OnBnClickedButton9()
{
	// TODO: 在此添加控件通知处理程序代码
	SetOperand(9);
}


void CtheMainWnd::OnBnClickedButton0()
{
	// TODO: 在此添加控件通知处理程序代码
	SetOperand(0);
}


void CtheMainWnd::OnBnClickedButton_Add()
{
	// TODO: 在此添加控件通知处理程序代码
	m_Operator = TEXT('+');
}


void CtheMainWnd::OnBnClickedButton_Sub()
{
	// TODO: 在此添加控件通知处理程序代码
	m_Operator = TEXT('-');
}


void CtheMainWnd::OnBnClickedButton_Mul()
{
	// TODO: 在此添加控件通知处理程序代码
	m_Operator = TEXT('*');
}


void CtheMainWnd::OnBnClickedButton_Dev()
{
	// TODO: 在此添加控件通知处理程序代码
	m_Operator = TEXT('/');
}


void CtheMainWnd::OnBnClickedButton_Equ()
{
	wstring str;
	// TODO: 在此添加控件通知处理程序代码
	switch (m_Operator)
	{
	case  TEXT('+'):		
		str = to_wstring( m_OperandFirst + m_OperandSecond);	
		break;
	case  TEXT('-'):
		str = to_wstring( m_OperandFirst - m_OperandSecond);
		break;
	case  TEXT('*'):
		str = to_wstring( m_OperandFirst * m_OperandSecond);
		break;
	case  TEXT('/'):
		str = to_wstring(m_OperandFirst / m_OperandSecond);
		break;
	default:
		break;
	}

	//更新edit控件消息
	::SetWindowText( ::GetDlgItem(m_hWnd,IDC_EDIT3),(LPCWSTR)str.c_str());

}


// 设置操作数
void CtheMainWnd::SetOperand(long i)
{
	if (isFirstOpeand)
	{
		m_OperandFirst = i;
		isFirstOpeand = false;
	}
	else
	{
		m_OperandSecond = i;
		isFirstOpeand = true;
	}
}

