#pragma once
#include <afxdialogex.h>
#include "afxwin.h"

// C15pbDialogEx 对话框

class C15pbDialogEx : public CDialogEx
{
	DECLARE_DYNAMIC(C15pbDialogEx)

public:
	C15pbDialogEx(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~C15pbDialogEx();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnIdclose();
	afx_msg void OnIddDialog1();
//	afx_msg void OnBnClickedCancel();
//	afx_msg void OnDoubleclickedIdcancel();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	CString str;
	afx_msg void OnCompacting(UINT nCpuTime);
	afx_msg void OnKillfocusIdok();
	virtual HRESULT accLocation(long *pxLeft, long *pyTop, long *pcxWidth, long *pcyHeight, VARIANT varChild);
	CButton xafdsaf;
};
