// C15pbDialogEx.cpp : 实现文件
//

#include "stdafx.h"
#include "C15pbDialogEx.h"
#include "afxdialogex.h"
#include "resource.h"


// C15pbDialogEx 对话框

IMPLEMENT_DYNAMIC(C15pbDialogEx, CDialogEx)

C15pbDialogEx::C15pbDialogEx(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DIALOG1, pParent)
	, str(_T(""))
{

}

C15pbDialogEx::~C15pbDialogEx()
{
}

void C15pbDialogEx::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, str);
	DDX_Control(pDX, IDOK, xafdsaf);
}


BEGIN_MESSAGE_MAP(C15pbDialogEx, CDialogEx)
	ON_COMMAND(IDCLOSE, &C15pbDialogEx::OnIdclose)
	ON_COMMAND(IDD_DIALOG1, &C15pbDialogEx::OnIddDialog1)
	ON_WM_CREATE()
	ON_WM_COMPACTING()
	ON_BN_KILLFOCUS(IDOK, &C15pbDialogEx::OnKillfocusIdok)
END_MESSAGE_MAP()


// C15pbDialogEx 消息处理程序


void C15pbDialogEx::OnIdclose()
{
	// TODO: 在此添加命令处理程序代码
}


void C15pbDialogEx::OnIddDialog1()
{
	// TODO: 在此添加命令处理程序代码
}




int C15pbDialogEx::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  在此添加您专用的创建代码

	return 0;
}


void C15pbDialogEx::OnCompacting(UINT nCpuTime)
{
	CDialogEx::OnCompacting(nCpuTime);

	// TODO: 在此处添加消息处理程序代码
}


void C15pbDialogEx::OnKillfocusIdok()
{
	// TODO: 在此添加控件通知处理程序代码
}


HRESULT C15pbDialogEx::accLocation(long *pxLeft, long *pyTop, long *pcxWidth, long *pcyHeight, VARIANT varChild)
{
	// TODO: 在此添加专用代码和/或调用基类

	return CDialogEx::accLocation(pxLeft, pyTop, pcxWidth, pcyHeight, varChild);
}
