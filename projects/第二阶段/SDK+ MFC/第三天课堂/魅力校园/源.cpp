
#include <TCHAR.h>
#include <windows.h>
#include <Windowsx.h>
#include  <CommCtrl.h>
#pragma comment(lib,"Comctl32.lib")


#include "resource.h"


//次Dlg窗口处理函数
INT_PTR CALLBACK InfoDialogProc(
	_In_ HWND   hwndDlg,
	_In_ UINT   uMsg,
	_In_ WPARAM wParam,
	_In_ LPARAM lParam
	)
{
	switch (uMsg)
	{
	case WM_NOTIFY:
		switch (((LPNMHDR)lParam)->code)
		{
		//case CUSTOM_SELCHANGE:
		//	if (((LPNMHDR)lParam)->idFrom == IDC_CUSTOMLISTBOX1)
		//	{
		//		   // Respond to message.
		//			return TRUE;
		//	}
		//	break;
			 // More cases on WM_NOTIFY switch.
		default:
		  break;
		}
		  // More cases on message switch.



	break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{

		default:
			break;
		}
		break;
	case WM_INITDIALOG:
	{
		//获取窗口大小
		InitCommonControls();
		RECT rt = {};
		HWND hList = GetDlgItem(hwndDlg, IDC_LIST2);
		GetClientRect(hwndDlg, &rt);

		//插入多列
		LVCOLUMN  lc;
		lc.mask = LVCF_WIDTH | LVCF_TEXT;
		lc.pszText = TEXT("年份");
		lc.cx = (rt.right - rt.left) / 4;
		lc.cchTextMax = 3;
		ListView_InsertColumn(hList, 0, &lc);
		lc.pszText = TEXT("时间");
		ListView_InsertColumn(hList, 1, &lc);
		lc.pszText = TEXT("经历");
		ListView_InsertColumn(hList, 2, &lc);
		lc.pszText = TEXT("备注");
		ListView_InsertColumn(hList, 3, &lc);
		//插入多行
		LVITEM  li = {};
		ListView_InsertItem(hList, &li);
		ListView_SetItemText(hList, 0, 0, TEXT("2009"));
		ListView_SetItemText(hList, 0, 1, TEXT("9月5日"));
		ListView_SetItemText(hList, 0, 2, TEXT("蓝翔大学附中学"));
		ListView_SetItemText(hList, 0, 3, TEXT("班长一职"));

		ListView_InsertItem(hList, &li);
		ListView_SetItemText(hList, 0, 0, TEXT("2011"));
		ListView_SetItemText(hList, 0, 1, TEXT("9月5日"));
		ListView_SetItemText(hList, 0, 2, TEXT("蓝翔大学"));
		ListView_SetItemText(hList, 0, 3, TEXT("学生会主席"));
		;
	}
		break;
	case WM_CLOSE:
		EndDialog(hwndDlg, 0);
		PostMessage(GetParent( hwndDlg), WM_SHOWWINDOW, true, SW_OTHERUNZOOM);
		break;

	default:
		break;
	}
	return 0;
}


//主Dlg窗口处理函数
INT_PTR CALLBACK DialogProc(
	_In_ HWND   hwndDlg,
	_In_ UINT   uMsg,
	_In_ WPARAM wParam,
	_In_ LPARAM lParam
	)
{
	switch (uMsg)
	{
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case  IDC_COMBO:   
		{

			switch (HIWORD(wParam))
			{
			case CBN_EDITUPDATE:
			{
				//判断是否为正确字符串,正确则设置并 改变 radio_box 的状态
				static bool cond = false;
				if (cond)
				{
					cond = false;
					break;
				}
				static TCHAR text[256] = {};
				GetWindowText((HWND)lParam, text, 256);


				TCHAR *  combo_string1 = TEXT("网通");
				TCHAR *  combo_string2 = TEXT("电信");
				TCHAR *  combo_string3 = TEXT("联通");

				if (0 == _tcscmp(combo_string1, text) || 0 == _tcscmp(combo_string2, text) || 0 == _tcscmp(combo_string3, text))
				{
					Button_SetCheck(GetDlgItem(hwndDlg, IDC_RADIO_WT), BST_UNCHECKED);
					Button_SetCheck(GetDlgItem(hwndDlg, IDC_RADIO_DX), BST_UNCHECKED);
					Button_SetCheck(GetDlgItem(hwndDlg, IDC_RADIO_LT), BST_UNCHECKED);
				}
				else
				{
					cond = true;
					//此处只能是Send而不能是Post,因为cond条件依赖两条消息 第二条是附加消息, 
					//执行实际的 SetText操作. 进队消息的话可能会落在别的消息后面,而导致错误
					SendMessage((HWND)lParam,WM_SETTEXT,0,(LPARAM)TEXT(""));
					break;
				}


				if (0 == _tcscmp(combo_string1, text))
					Button_SetCheck(GetDlgItem(hwndDlg, IDC_RADIO_WT), BST_CHECKED);
				if (0 == _tcscmp(combo_string2, text))
					Button_SetCheck(GetDlgItem(hwndDlg, IDC_RADIO_DX), BST_CHECKED);
				if (0 == _tcscmp(combo_string3, text))
					Button_SetCheck(GetDlgItem(hwndDlg, IDC_RADIO_LT), BST_CHECKED);
				
			}
			break;
			case  CBN_SELCHANGE:
			{
				//通知父窗口 TEXT将要改变				
				PostMessage(hwndDlg, WM_COMMAND, MAKELONG(IDC_COMBO,CBN_EDITUPDATE),lParam);			
			}
			default:
				break;
			}

		}
			break; 
		case  IDC_RADIO_LT:   
		{
			TCHAR str[256] = {};
			Button_GetText(GetDlgItem(hwndDlg, IDC_RADIO_LT), str,256);
			ComboBox_SetText(GetDlgItem(hwndDlg, IDC_COMBO), str);
		}
			break;
		case  IDC_RADIO_WT:  
		{
			TCHAR str[256] = {};
			Button_GetText(GetDlgItem(hwndDlg, IDC_RADIO_WT), str, 256);
			ComboBox_SetText(GetDlgItem(hwndDlg, IDC_COMBO), str);
		}
			break;
		case  IDC_RADIO_DX:   
		{
			TCHAR str[256] = {};
			Button_GetText(GetDlgItem(hwndDlg, IDC_RADIO_DX), str, 256);
			ComboBox_SetText(GetDlgItem(hwndDlg, IDC_COMBO), str);
		}
			break;
		case IDC_EDIT_PASSWORD:
			switch (HIWORD(wParam))
			{
				case EN_UPDATE: //将要更新文本
				{
					//static bool first = true;
					//if (first)
					//{
					//	//隐藏编辑框光标
					//	HideCaret(GetDlgItem(hwndDlg, IDC_EDIT_PASSWORD));
					//	first = false;
					//}
				
					//static bool cond = false;
					//if (cond)
					//{
					//	cond = false;
					//	break;
					//}

					////将字符替换为***z字符串
					//static TCHAR password[256] = {};
					//static TCHAR password_star[256] = {};
					//GetWindowText((HWND)lParam, password, 256);
					//int char_num =_tcslen(password);
					//password_star[char_num] = 0;
					//for (int i = 0; i < char_num ; ++i)
					//	password_star[i] = TEXT('*');
					//cond = true;
					//SetWindowText((HWND)lParam, password_star);

				}
				;
			}

			break;
		case IDOK:
			{  
				TCHAR  buf[256] = {};
				TCHAR *  usre_name = TEXT("15pb") ;
				TCHAR *  password = TEXT("1234");
				GetWindowText(GetDlgItem(hwndDlg, IDC_EDIT_NAME),buf,256);
				if (0 == _tcscmp(usre_name, buf))
				{
					GetWindowText(GetDlgItem(hwndDlg, IDC_EDIT_PASSWORD), buf, 256);
					if (0 == _tcscmp(password, buf))
					{


						PostMessage(hwndDlg, WM_SHOWWINDOW, false, SW_PARENTCLOSING);
						DialogBox((HINSTANCE)GetWindowLong(hwndDlg, GWL_HINSTANCE),
							MAKEINTRESOURCE(IDD_DIALOG1),
							hwndDlg, InfoDialogProc);

						return 0;
					}
				}
				
				MessageBox(hwndDlg,TEXT("登陆失败!"),TEXT("用户名或密码错误!"),MB_OK);
				SendMessage(GetDlgItem(hwndDlg, IDC_EDIT_NAME), WM_SETTEXT, 0, (LPARAM)TEXT(""));
				SendMessage(GetDlgItem(hwndDlg, IDC_EDIT_PASSWORD), WM_SETTEXT,0, (LPARAM)TEXT(""));
			};
			break;

		case IDCANCEL:
			PostMessage(hwndDlg,WM_CLOSE,0,0);
			break;
		default:
			break;
		}
		break;
	case WM_INITDIALOG:
		ComboBox_AddString(GetDlgItem(hwndDlg, IDC_COMBO), TEXT("联通"));
		ComboBox_AddString(GetDlgItem(hwndDlg, IDC_COMBO), TEXT("电信"));
		ComboBox_AddString(GetDlgItem(hwndDlg, IDC_COMBO), TEXT("网通"));
		Button_SetCheck(GetDlgItem(hwndDlg, IDC_RADIO_DX), BST_CHECKED);

		SetWindowText(GetDlgItem(hwndDlg, IDC_EDIT_NAME),TEXT("15pb"));
		SetWindowText(GetDlgItem(hwndDlg, IDC_EDIT_PASSWORD), TEXT("1234"));
		break;
	case WM_CLOSE:
		EndDialog(hwndDlg, 0);
		break;
	default:
		break;
	}
	return 0;
}

int CALLBACK WinMain(
	_In_ HINSTANCE hInstance,
	_In_ HINSTANCE hPrevInstance,
	_In_ LPSTR     lpCmdLine,
	_In_ int       nCmdShow
	)
{
	DialogBox(hInstance,MAKEINTRESOURCE(IDD_DIALOG_MAIN),NULL,DialogProc);
	return 0;
}