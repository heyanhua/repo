#include <Windows.h>
#include "resource.h"


LRESULT CALLBACK OKButtonProc(
	_In_ HWND   hwndDlg,
	_In_ UINT   uMsg,
	_In_ WPARAM wParam,
	_In_ LPARAM lParam
	);

using  OLDWNDPROC = decltype(OKButtonProc);
OLDWNDPROC *  oldProc;


 LRESULT CALLBACK OKButtonProc(
	_In_ HWND   hwndDlg,
	_In_ UINT   uMsg,
	_In_ WPARAM wParam,
	_In_ LPARAM lParam
	)
{
	switch (uMsg)
	{
	case WM_LBUTTONDOWN:
		MessageBox(NULL,TEXT("15"),TEXT("PB"),MB_OK);
		return 0;
	}
	return oldProc(hwndDlg, uMsg, wParam, lParam);
};

 

INT_PTR CALLBACK DialogProc(
	_In_ HWND   hwndDlg,
	_In_ UINT   uMsg,
	_In_ WPARAM wParam,
	_In_ LPARAM lParam
	) 
{
	switch (uMsg)
	{
	case WM_INITDIALOG:
  oldProc =	(OLDWNDPROC *)SetWindowLong(GetDlgItem(hwndDlg, IDOK), GWL_WNDPROC, (LONG)OKButtonProc);
		break;
	case WM_CLOSE:
		PostQuitMessage(0);
		break;
	}
	return 0;
};





int CALLBACK WinMain(
	_In_ HINSTANCE hInstance,
	_In_ HINSTANCE hPrevInstance,
	_In_ LPSTR     lpCmdLine,
	_In_ int       nCmdShow
	)
{
	
	HWND hDlg =  CreateDialog(hInstance, MAKEINTRESOURCE(IDD_DIALOG1),NULL, DialogProc);
	UpdateWindow(hDlg);
	ShowWindow(hDlg,SW_SHOW);

	MSG msg = {};
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return 0;
}