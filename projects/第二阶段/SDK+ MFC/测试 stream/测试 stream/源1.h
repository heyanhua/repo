
#include <string>
#ifdef UNICODE
#define _tstring  std::wstring
#else
#define _tstring  std::string
#endif // !UNICODE


#include <sstream>
#ifdef UNICODE
#define _tstringstream   std::wstringstream
#define _tistringstream  std::wistringstream
#define _tostringstream  std::wostringstream
#else
#define _tstringstream   std::stringstream
#define _tistringstream  std::wstringstream
#define _tostringstream  std::wstringstream
#endif // !UNICODE




//可变参数模板, "..."是包扩展(Pack Expansion)  
template <typename FIRST, typename... TAIL>
_tstring & myto_string(_tstring & str, FIRST  first, TAIL... tail)
{
	_tstringstream   sstream;
	sstream << first;
	str += sstream.str();
	return myto_string(str, tail...);
}

template <typename FIRST, typename... TAIL>
_tstring & myto_string(_tstring & str,  TAIL... tail)
{
	_tstringstream   sstream;
	sstream << first;
	str += sstream.str();
	return myto_string(str, tail...);
}

//终止条件 --特化方式
_tstring & myto_string(_tstring& str)
{
	return str;
}
