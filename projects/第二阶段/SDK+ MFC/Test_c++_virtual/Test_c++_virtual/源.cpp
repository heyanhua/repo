



//1个字节,
//既然是个变量,
//就应该有地址
//要不然取地址时
//取什么?
class a
{
public:
	int f() { return sizeof(this); };
};


//4个字节(虚函数表指针),  也就是虚函数表指针的大小
class b
{
public:
	virtual int f() { return sizeof(this); };
};



//4个字节(虚函数表指针), 虚函数表内容和基类相同
class c :public b
{

};


//4个字节(虚函数表指针), 虚函数表和基类同样大小,但是内容不同
class d :public b
{
	virtual int f() {};
};


//4个字节, 虚函数表和基类 以及基类的基类同样大小,
class e :public c
{
public:
	int fff() {
		return sizeof(this);
	};
};


//虚函数表有三个内容, 分别指向虚函数表f的三个重载版本
//虚函数在类中没有确定的地址,  只有生成对象才有确定的地址.
class f1:public e
{
	virtual void f(int,int) {};
	virtual void f(int,int,int) {};
	virtual void f(int,int,int,int) {};
	int  fff(int ) {
		int x = 0;
		e::fff();
		e::f();
	};
};




//不管是哪种调用
//重载
//覆盖
//隐藏
//派生类都能显式的调用基类的函数
//函数,有访问权限的话
void main()
{
	auto size = sizeof(a);
		 size = sizeof(b);
		 size = sizeof(c);
		 size = sizeof(d);
		 size = sizeof(e);
		 size = sizeof(f1);


		 a a_obj;
		 e e_obj;
		 auto ret = a_obj.f();
		      ret = e_obj.fff();
			  ret = e_obj.f();
}