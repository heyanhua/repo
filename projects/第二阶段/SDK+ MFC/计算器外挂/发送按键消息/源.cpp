#include <windows.h>
#include <Tchar.h>
#include <sstream>


//------------------项目流产-------------
//------------------计划失败-------------
//----事实证明
//----Windows窗体注册类不能跨进程共享





#include <map>
using std::map;
using std::pair;
#include <string>
#include "resource.h"


//////////////////////////
////  类型相关
//////////////////////////
#ifdef UNICODE
#define _tstring  std::wstring
#else
#define _tstring  std::stirng
#endif // !UNICODE


#ifdef UNICODE
#define _tstringstream  std::wstringstream
#else
#define _tstringstream  std::stringstream
#endif // !UNICODE


//#ifdef UNICODE
//#define _itot  _itow
//#else
//#define _itot  _itoa
//#endif // !UNICODE



//全局变量 -------------
HINSTANCE g_hInstance = nullptr;






//全局变量 end------------









//思路; FindWindow找到计算器
//枚举所有子窗口,  并获取其位置,  样式,  句柄
//获取计算器窗口类样式,  创建计算器窗口.
//在计算器Create窗口中创建子窗口  -  在自己的子窗口  和  计算器句柄之间  做映射
//将主窗口中所有的WM_COMMAND消息发送到 计算器


struct myWINDOWINFO
{
	HWND        hWnd;
	HWND        hParentWnd;
	HWND		hOldWnd;
	HWND        hOldParentWnd;
	_tstring    strParentName;
	WINDOWINFO  msInfo;
};



BOOL CALLBACK EnumChildProc(
	_In_  HWND hwnd,
	_In_  LPARAM lParam
	)
{
	//获取自身----------------
	map<_tstring, myWINDOWINFO> * p = (decltype(p))lParam;
	map<_tstring, myWINDOWINFO> & ref = *p;
	_tstring name;
	name.resize(256,0);
	GetWindowText(hwnd, (LPWSTR)name.c_str(), name.max_size());
	if (0 == name[0])
	{
		//将hwnd转换为字符串
		_tstringstream tmp_stream;
		tmp_stream.unsetf(std::ios::skipws);
		tmp_stream << TEXT("hwnd:0x") << std::hex << (int) hwnd;
		tmp_stream >> name;
	}
	//获取并设置相应字段信息
	ref[name].hWnd = (HWND)0;
	ref[name].hParentWnd = (HWND)0;
	ref[name].hOldWnd = hwnd;
	ref[name].hOldParentWnd = GetParent(hwnd);
	GetWindowInfo(hwnd,& ref[name].msInfo );


	//枚举子窗口
	EnumChildWindows(hwnd, EnumChildProc, lParam);

	return true;  
};



//根据信息创建所有的窗口
//第一次创建所有的顶级窗口



void CreatAllWindow(map<_tstring, myWINDOWINFO> & ref)
{
	//多次迭代才能完全创建,
	//index 剩余多少个窗口没有创建.
	static auto index = ref.size();
	for (auto & item : ref)
	{
		if ( (item.second.hOldParentWnd == 0 || item.second.hParentWnd != 0 ) && item.second.hWnd == 0)
		{
			item.second.hWnd = CreateWindowEx(item.second.msInfo.dwExStyle,
							(LPCTSTR)MAKELONG(0, item.second.msInfo.atomWindowType),
							item.first.c_str(),
							item.second.msInfo.dwStyle,
							item.second.msInfo.rcWindow.left,
							item.second.msInfo.rcWindow.top,
							item.second.msInfo.rcWindow.right,
							item.second.msInfo.rcWindow.bottom,
							item.second.hParentWnd,
							NULL,  //暂置为0
							GetWindowLong(item.second.hOldWnd, GWL_HINSTANCE) ? g_hInstance : 0,
							NULL   //暂置为0
							);
			index--;

			if (NULL == item.second.hWnd)
			{
				OutputDebugString(TEXT("创建窗口失败!\t原窗口为:"));
				OutputDebugString(item.first.c_str());
				OutputDebugString(TEXT("\t错误代码:"));

				_tstringstream tmp_stream;
				TCHAR temstr[12] = {};
				tmp_stream << GetLastError();
				tmp_stream >> temstr;
				OutputDebugString(temstr);
				OutputDebugString(TEXT("\n"));
			}

			//所有窗口创建完毕,返回
			if (0 == index)
				return;

			//遍历所有子窗口并填充子窗口的 父窗口字段
			for (auto & enum_item : ref)
			{
				if (item.second.hOldWnd == enum_item.second.hOldParentWnd)
					enum_item.second.hParentWnd = item.second.hWnd;
			}
		}


	}



	CreatAllWindow(ref);
}




//INT_PTR CALLBACK DialogProc(
//	_In_ HWND   hwndDlg,
//	_In_ UINT   uMsg,
//	_In_ WPARAM wParam,
//	_In_ LPARAM lParam
//	)
//{
//	switch (uMsg)
//	{
//	case WM_INITDIALOG:
//	{
//
//
//		break;
//	}
//	case WM_CLOSE:  break;
//		PostQuitMessage(0);
//		break;
//	case WM_COMMAND:
//	{
//
//		switch (LOWORD(wParam))
//		{
//		case IDC_BUTTON0:
//		case IDC_BUTTON1:
//		case IDC_BUTTON2:
//		case IDC_BUTTON3:
//		case IDC_BUTTON4:
//		case IDC_BUTTON5:
//		case IDC_BUTTON6:
//		case IDC_BUTTON7:
//		case IDC_BUTTON8:
//		case IDC_BUTTON9:
//			wParam = 0x30 + LOWORD(wParam) - IDC_BUTTON0;
//			break;
//		case		IDC_BUTTON_ADD:
//			wParam = VK_ADD;
//			break;
//		case		IDC_BUTTON_SUB:
//			wParam = VK_SUBTRACT;
//			break;
//		case		IDC_BUTTON_MUL:
//			wParam = VK_MULTIPLY;
//			break;
//		case		IDC_BUTTON_DIV:
//			wParam = VK_DIVIDE;
//			break;
//		case       IDC_BUTTON_EQU:
//			wParam = 187;  //等号的虚拟键
//			break;
//		default:
//			break;
//		}
//	//	PostMessage(hWnd, WM_KEYUP, wParam, 0);
//	}
//	default:  break;
//		return 0;
//	}
//	return 0;
//}

int CALLBACK WinMain(
	_In_ HINSTANCE hInstance,
	_In_ HINSTANCE hPrevInstance,
	_In_ LPSTR     lpCmdLine,
	_In_ int       nCmdShow
	)
{
	g_hInstance = hInstance;
	TCHAR * main_window_name = TEXT("无标题-画图");
	map<_tstring, myWINDOWINFO> child_window;
	HWND hWnd = FindWindow(NULL, main_window_name);
	child_window[main_window_name].hOldWnd = hWnd;
	child_window[main_window_name].hWnd = (HWND)0;
	child_window[main_window_name].hOldParentWnd = (HWND)0;
	child_window[main_window_name].hParentWnd = (HWND)0;

	if (hWnd)
		EnumChildWindows(hWnd, EnumChildProc, (LPARAM)&child_window);
	else
		OutputDebugString(TEXT("查找主窗口失败!\n"));
	

	CreatAllWindow(child_window);




	//HWND hDlg = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), NULL, DialogProc);
	//UpdateWindow(hDlg);
	//ShowWindow(hDlg, SW_SHOW);

	//MSG  msg{};


	//while (GetMessage(&msg, 0, 0, 0))
	//{
	//	TranslateMessage(&msg);
	//	DispatchMessage(&msg);
	//}
}

