#ifndef WIN_HELPER_HPP
#define WIN_HELPER_HPP


#include "cpp_helper.hpp"
#include <windows.h>


//更好用的格式化字符串---------------------Begin

//可变参数模板, "..."是包扩展(Pack Expansion)  
template <typename FIRST, typename... TAIL>
_tstring & to_string(_tstring & str, const FIRST & first, const TAIL&... tail)
{
	_tstringstream   sstream;
	sstream << first; 
	str += sstream.str();
	return to_string(str, tail...);
}

//递归过程---展开参数包::
//不作为终止条件的话,只是在编译器帮忙展开参数包,并不生成真实代码
//可以同时作为参数包展开条件以及终止条件来使用
//单纯的展开条件不生成真实代码
template <typename... TAIL>
_tstring & to_string(_tstring& str,TAIL&... tall)
{
	return  str;
}

//终止条件
template<>
_tstring & to_string(_tstring& str)
{
	return str;
}

//更好用的格式化字符串---------------------End


#ifndef DLL_EXPORT
#define DECLDIR __declspec(dllimport)
#else
#define DECLDIR __declspec(dllexport)
#endif

template <typename FIRST, typename... TAIL>
VOID  WINAPI OutputDebugString(const FIRST & first, const TAIL&... tail)
{
	_tstring  str;
	to_string(str, first, tail...);
	OutputDebugString(str.c_str());
}




#endif 