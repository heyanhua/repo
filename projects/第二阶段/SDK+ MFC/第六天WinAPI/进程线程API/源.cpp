


#include <windows.h>
#include <tchar.h>
#include "D:\repo\sourcs_repo\win_helper.hpp"


DWORD  WINAPI  fun(LPVOID pParam)
{
	OutputDebugString(__FILE__);
	return TRUE;
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	//创建一个进程
	auto hProcess = [] {
		PROCESS_INFORMATION  pi = {};
		STARTUPINFO          si = {};
		//内核对象都有一个 LPSECURETT_ATTRUBITS --安全属性,  GDI对河和内核对象不同
		return CreateProcess(TEXT("D:\\ProtableSoft\\逆向分析常用工具\\调试工具\\OD\\Origin\\Origin-201\\ollydbg.exe"),
			NULL, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);	
	}();

	//创建一个线程
	int i = 0;
	int x = 0;

	//实际操作的函数 ----放在需要捕获变量的作用域中,
	auto real = [&](LPVOID pParam)->DWORD {
		//Do something...
		return 0;
	};
	using type_real_func = decltype(real);
	
	//这个匿名函数按照回调函数签名编写,
	auto fun = [](LPVOID pParam) ->DWORD {
					//lambda表达式的语法糖, 
		 return (* (  (type_real_func *)0)  )(pParam);
	};


	auto pThread = CreateThread( NULL,0, fun, NULL,NULL,NULL);

	WaitForSingleObject(pThread, -1);

}