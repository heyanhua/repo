// 第四天  DC.cpp : 定义应用程序的入口点。
//

#include "stdafx.h"
#include "第四天  DC.h"

#define MAX_LOADSTRING 100
#define GetDC_TEXTOUT  10001
#define GETWINDOWDC_TEXTOUT  10002
#define CREATEDC_TEXTOUT  10003




// 全局变量: 
HINSTANCE hInst;                                // 当前实例
WCHAR szTitle[MAX_LOADSTRING];                  // 标题栏文本
WCHAR szWindowClass[MAX_LOADSTRING];            // 主窗口类名

// 此代码模块中包含的函数的前向声明: 
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 在此放置代码。

    // 初始化全局字符串
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_DC, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // 执行应用程序初始化: 
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_DC));

    MSG msg;

    // 主消息循环: 
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_DC));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_DC);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释: 
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // 将实例句柄存储在全局变量中

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的:    处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case  WM_CREATE:

		//创建按钮用于GetDC()

		CreateWindow(

			TEXT("button"),

			TEXT("hehe"),

			WS_VISIBLE | WS_CHILD,

			50, 50, 60, 30, hWnd,

			(HMENU)GetDC_TEXTOUT,

			hInst,

			NULL);

		//创建按钮用于GetWindowDC()

		CreateWindow(

			TEXT("button"),

			TEXT("hehe"),

			WS_VISIBLE | WS_CHILD,

			120, 50, 60, 30, hWnd,

			(HMENU)GETWINDOWDC_TEXTOUT,

			hInst,

			NULL);

		CreateWindow(

			TEXT("button"),

			TEXT("hehe"),

			WS_VISIBLE | WS_CHILD,

			190, 50, 60, 30, hWnd,

			(HMENU)CREATEDC_TEXTOUT,

			hInst,

			NULL);


    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // 分析菜单选择: 
            switch (wmId)
            {
			case CREATEDC_TEXTOUT:
			{
				HDC hDc = CreateDC(TEXT("DISPLAY"), NULL, NULL, NULL);

				TextOut(hDc, 100, 100, TEXT("15PB"), 4);

				DeleteDC(hDc);



				HDC hClient = GetDC(hWnd);

				for (int i = 100; i < 120; i++)

				{

					SetPixel(hClient, i, 200, RGB(255, 0, 0));

				}

				ReleaseDC(hWnd, hClient);






				



			}
				break;
			case GetDC_TEXTOUT:
			{
				HDC hDc = GetDC(hWnd);

				TextOut(hDc, 0, 20, TEXT("15PB"), 4);

				ReleaseDC(hWnd, hDc);



				//画直线

				HPEN hPen = CreatePen(PS_DASHDOTDOT, 10, RGB(255, 0, 0));

				HDC hClient = GetDC(hWnd);

				HPEN hOldPen = (HPEN)SelectObject(hClient, hPen);

				POINT pt = {};

				MoveToEx(hClient, 100, 100, &pt);

				MoveToEx(hClient, 500, 100, &pt);

				LineTo(hClient, 100, 300);

				ReleaseDC(hWnd, hClient);

				//使用完画笔，不要忘了给销毁掉

				SelectObject(hClient, hOldPen);

				DeleteObject(hPen);

			}
				break;
			case GETWINDOWDC_TEXTOUT:
			{
				HDC hDc = GetWindowDC(hWnd);

				TextOut(hDc, 0, 0, TEXT("15PB"), 4);

				ReleaseDC(hWnd,hDc);


				// 			HPEN hPen = CreatePen(PS_SOLID, 10, RGB(255, 0, 0));

				HDC hClient = GetDC(hWnd);

				// 			HPEN hOldPen = (HPEN)SelectObject(hClient, hPen);

				// 

				// 			HBRUSH hBrush = CreateSolidBrush(RGB(0,0,255));

				// 			HBRUSH hOldBrush = (HBRUSH)SelectObject(hClient, hBrush);

				// 			//画矩形

				// 			Rectangle(hClient, 300, 10, 500, 300);

				//画椭圆

				//Ellipse(hClient, 300, 10, 500, 210);

				//画扇形

				//Pie(hClient, 300, 10, 500, 210, 100, 100, 500, 10);

				//画弦

				//Chord(hClient, 300, 10, 500, 210, 100, 100, 500, 10);

				//画圆角矩形

				//RoundRect(hClient, 300, 10, 500, 300, 200, 290);

				HDC hMem = CreateCompatibleDC(hClient);

				HBITMAP hBit = LoadBitmap(

					hInst, MAKEINTRESOURCE(IDB_BITMAP1));

				BITMAP  bitInfo = {};



				//可以获取GDI对象的信息。

				GetObject(hBit, sizeof(BITMAP), &bitInfo);



				SelectObject(hMem, hBit);

				//等比拷贝图片

				BitBlt(

					hClient,

					10, 200, bitInfo.bmWidth, bitInfo.bmHeight,

					hMem,

					0, 0,

					NOTSRCERASE);

			}
				break;
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: 在此处添加使用 hdc 的任何绘图代码...
			// TODO:  在此添加任意绘图代码...

			SetTextAlign(hdc, TA_CENTER);

			SetTextColor(hdc, RGB(0, 255, 255));

			SetBkColor(hdc, RGB(255, 255, 0));

			TextOut(hdc, 100, 100, TEXT("15PB"), 4);

			SetBkMode(hdc, TRANSPARENT);

			TextOut(hdc, 100, 140, TEXT("15PB132"), 7);

			SetBkMode(hdc, OPAQUE);



			TextOut(hdc, 100, 180, TEXT("15PB456789"), 10);

            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
