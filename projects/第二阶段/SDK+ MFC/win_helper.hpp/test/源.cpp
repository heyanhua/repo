template<typename fun, typename... Args>
auto lazy_fun(fun&f, Args&&... args)
{
	return[&f,&args...] { return f(args...); };
};


int funint(int)
{
	return 0;
}



void fun()
{
	;
}

void main()
{
	auto x = lazy_fun(funint,0);
	x();
}