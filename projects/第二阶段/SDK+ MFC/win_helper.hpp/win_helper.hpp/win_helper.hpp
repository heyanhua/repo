#ifndef WIN_HELPER_HPP
#define WIN_HELPER_HPP


#include "cpp_helper.hpp"
#include <windows.h>



#ifndef DLL_EXPORT
#define DECLDIR __declspec(dllimport)
#else
#define DECLDIR __declspec(dllexport)
#endif

template <typename FIRST, typename... TAIL>
VOID  WINAPI OutputDebugString(const FIRST & first, const TAIL&... tail)
{
	_tstring  str;
	to_string(str, first, tail...);
	OutputDebugString(str.c_str());
}

#endif 