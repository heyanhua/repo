

//通过...和初始化列表展开参数包

template<typename T>
void f(T t)
{
	//对每个参数执行的操作
	int x = 0;
}

template<typename... Args>
void f(Args... args)
{
	//编译器为了对 arr[]的初始化列表求值
	//而展开 args;
	//最终arr有 sizeof...(Args)个参数, 每个参数的值都为0
	//逗号表达式从左往右执行
	int arr[] = {(f(args),0)...};
	int x = sizeof(arr);
}

//
//void main()
//{
//	f(1,2,3,4,5);
//}