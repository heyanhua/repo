
// 1.2定义 (定义一个部分展开的可变模板参数模板类, 告诉编译器如何递归展开参数包)r; 
template<typename First, typename... Rest>
class _container <First first, Rest... rest>
{
	//使用First first
	first t;

};
// 1.3特化--- 终止条件:终止在最后两个参数
template<typename Last>
class  _container<Last last>
{
	//使用Last
	last t;

};

