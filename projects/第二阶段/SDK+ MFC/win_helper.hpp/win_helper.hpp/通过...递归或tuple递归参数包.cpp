#include "win_helper.hpp"
#include <windows.h>
#include <tuple>
using std::tuple;




template <typename... Args>
class any_container
{
public:
	size_t size() { 
		return sizeof...(Args);
	}
};


////////////////通过递归方式展开参数包   ----在递归的过程中进行操作.
//////////////template <typename T,typename... Args>
//////////////void f(T t,Args... args)
//////////////{
//////////////	//在此使用每一个参数
//////////////	//  ...
//////////////	//End
//////////////	   f(args...);
//////////////}
//////////////
////////////////递归终止条件 0个参数
//////////////void f()
//////////////{
//////////////	;
//////////////}


//通过tuple展开参数包--------  终止条件
template < std::size_t I = 0,typename Tuple>
typename std::enable_if <I == std::tuple_size <Tuple>::value> ::type 
	f(Tuple t)
{
}
//通过tuple展开参数包 --------- 处理函数
template < std::size_t I = 0, typename Tuple>
typename std::enable_if <I < std::tuple_size <Tuple>::value> ::type 
	f(Tuple t)
{
	//
	std::get<I>(t); //获取tuple的 第I个成员
		//...在此处处理 t    
	//
	f<I + 1>(t); //递归调用
}

//
template<typename... Args>
void f(Args... args)
{
	//调用方式:   将可变参数包转换为  tuple
	//然后通过 递增参数的索引来传递print函数.  选择
	//f(std::make_tuple(args...));
	int arr[] = {(args,1,2,3, 0)...};
}








//
//void main()
//{
//	any_container < int, int, char, double, long>  ac;
//	any_container<> ac2;
//	auto  i = ac.size();
//	 i = ac2.size();
//	 f(1,2,0,5,6,76);
//	return;
//}