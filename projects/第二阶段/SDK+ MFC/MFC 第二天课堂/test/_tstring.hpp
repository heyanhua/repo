#pragma once

#include <string>
#ifdef UNICODE
#define _tstring  wstring
#else
#define _tstring  string
#endif // !UNICODE


#include <sstream>
#ifdef UNICODE
#define _tstringstream   std::wstringstream
#define _tistringstream  std::wistringstream
#define _tostringstream  std::wostringstream
#else
#define _tstringstream   std::stringstream
#define _tistringstream  std::wstringstream
#define _tostringstream  std::wstringstream
#endif // !UNICODE


#include <fstream>
#ifdef UNICODE
#define _tfstream   std::wfstream
#define _tifstream  std::wifstream
#define _tofstream  std::wofstream
#else
#define _tfstream   std::fstream
#define _tifstream  std::ifstream
#define _tofstream  std::ofstream
#endif // !UNICODE



#include <iostream>
#ifdef UNICODE
#define _tiostream   std::wiostream
#define _tistream  std::wistream
#define _tostream  std::wostream
#else
#define _tiostream   std::iostream
#define _tistream  std::istream
#define _tostream  std::ostream
#endif // !UNICODE




class string: public std::string
{
public:
	
}