
// 主程序1Dlg.h : 头文件
//

#pragma once
#include "CmyShape.h"
#include <vector>

typedef struct _SHAPE
{
	CPoint Begin;
	CPoint End;
}SHAPE;

// C主程序1Dlg 对话框
class C主程序1Dlg : public CDialogEx
{
// 构造
public:
	C主程序1Dlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MY1_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;
	CPoint  m_Begin;
	BOOL   m_bIsPush;
	CPoint  m_End;

	std::vector<SHAPE> m_vecPoint;
	CmyShape* m_CurrentShape;
	std::vector<CmyShape *> m_Shape;
	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
};
