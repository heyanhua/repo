// InfoDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MFC第二天作业.h"
#include "InfoDlg.h"
#include "afxdialogex.h"


// InfoDlg 对话框

IMPLEMENT_DYNAMIC(InfoDlg, CDialogEx)

InfoDlg::InfoDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DIALOG1, pParent)
{

}

InfoDlg::~InfoDlg()
{
}

void InfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST2, info_list);
	DDX_Control(pDX, IDC_LIST2, info_list);
}


BEGIN_MESSAGE_MAP(InfoDlg, CDialogEx)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST2, &InfoDlg::OnLvnItemchangedList2)
END_MESSAGE_MAP()


// InfoDlg 消息处理程序


BOOL InfoDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	info_list.InsertColumn(0, TEXT("年份"),0,100);
	info_list.InsertColumn(0, TEXT("时间"), 0, 100);
	info_list.InsertColumn(0, TEXT("经历"), 0, 100);
	info_list.InsertColumn(0, TEXT("备注"), 0, 100);
	LVITEM li = {};
	info_list.InsertItem(&li);
	info_list.InsertItem(&li);
	info_list.SetItemText(0, 0,  TEXT("2009"));
	info_list.SetItemText(0, 1, TEXT("9月5日"));
	info_list.SetItemText(0, 2, TEXT("清华大学附中"));
	info_list.SetItemText(0, 3, TEXT("班长"));


	info_list.SetItemText(0, 0, TEXT("2011"));
	info_list.SetItemText(0, 1, TEXT("9月5日"));
	info_list.SetItemText(0, 2, TEXT("清华大学"));
	info_list.SetItemText(0, 3, TEXT("学生会主席"));





	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}


void InfoDlg::OnLvnItemchangedList2(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}
