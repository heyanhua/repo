
#include "CleanFile.h"
#include <windows.h>
#include <algorithm>

CleanFile::CleanFile():working_dir(256,0)
{
	filter.emplace_back(TEXT(".exe"));
	filter.emplace_back(TEXT(".pdb"));
	filter.emplace_back(TEXT(".pch"));
}


CleanFile::~CleanFile()
{
}

void CleanFile::start()
{
	//遍历文件
	working_dir += (TEXT("\\*.*"));
	clean_a_dir(working_dir.c_str());
}

void CleanFile::delete_from_filter(LPCTSTR file_type, size_t buffer_lenth)
{
	// sstream 把空格当作分割符号
	_tstringstream  strstm;
	_tstring        str_tmp;
	size_t index = 0;
	while (index <= buffer_lenth)
	{
		//取出过滤格式
		strstm << file_type[index];
		strstm >> str_tmp;

		
		//查找并删除原有格式
		for (auto Iter = filter.begin(); Iter != filter.end(); Iter++)
		{
			if (*Iter == str_tmp)
			{
				filter.erase(Iter);
				break;
			}
		}

		index += filter[filter.size() - 1].size();
	}

}

void CleanFile::add_to_filter(LPCTSTR file_type, size_t buffer_lenth)
{
	// sstream 把空格当作分割符号
	_tstringstream  strstm;
	_tstring        str_tmp;
	size_t index = 0;
	while (index  <= buffer_lenth)
	{ 
	    strstm <<file_type[index];
		strstm >> str_tmp;
		filter.emplace_back(str_tmp);
		index += filter[filter.size() - 1].size();
	}
}

void CleanFile::clean_a_dir(LPCTSTR dir)
{
	//遍历文件
	WIN32_FIND_DATA  wfd = {};
	_tstring  str; //找到的文件名
	unsigned int i = 0;

	//开始遍历
	auto  h = FindFirstFile(dir, &wfd);
	bool  found = false;//需要删除
	do {

		if (!(INVALID_HANDLE_VALUE == h || ERROR_FILE_NOT_FOUND == (long)h))
		{
			str = wfd.cFileName;
			//遍历
			for (auto item : filter)
			{
				i = str.find(item);
				//判断后缀名
				if (_tstring::npos != i && i + item.size() == str.size()) //查找到
				{
					found = true;
					break;
				}
			}

			//直接删除
			if (found) {
				DeleteFile(str.c_str());
				found = false;
			}
			//没有被删除且是一个目录
			if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				clean_a_dir(str.c_str());

			//递归调用后, 判断删除后是否为空,为空则删除
			if (ERROR_FILE_NOT_FOUND == (long)FindFirstFile(working_dir.c_str(), &wfd))
				DeleteFile(str.c_str());
		}
	} while ( FindNextFile(h, &wfd));//终止条件, 遍历完毕
}

void CleanFile::filter_to_buffer(_tstring buffer)
{
	// sstream 把空格当作分割符号
	buffer.clear();
	size_t index = 0;
	for (auto item : filter)
		buffer += item;
}


