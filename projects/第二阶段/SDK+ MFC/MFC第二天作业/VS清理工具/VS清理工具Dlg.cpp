
// VS清理工具Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "VS清理工具.h"
#include "VS清理工具Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CVS清理工具Dlg 对话框



CVS清理工具Dlg::CVS清理工具Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_VS_DIALOG, pParent)
	, edit_to_filter(_T(""))
	, status_buffer(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CVS清理工具Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT2, edit_to_filter);
	DDV_MaxChars(pDX, edit_to_filter, 256);
	DDX_Text(pDX, IDC_STATIC_Status, status_buffer);
}

BEGIN_MESSAGE_MAP(CVS清理工具Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_EN_CHANGE(IDC_EDIT2, &CVS清理工具Dlg::OnEnChangeEdit2)
	ON_BN_CLICKED(IDC_BUTTON1, &CVS清理工具Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON3, &CVS清理工具Dlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON2, &CVS清理工具Dlg::OnBnClickedButton2)
	ON_WM_DROPFILES()
END_MESSAGE_MAP()


// CVS清理工具Dlg 消息处理程序

BOOL CVS清理工具Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码


	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CVS清理工具Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CVS清理工具Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CVS清理工具Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CVS清理工具Dlg::OnEnChangeEdit2()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
}


void CVS清理工具Dlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(FALSE);

	clean_file.add_to_filter(
		edit_to_filter.GetBuffer(), 
		edit_to_filter.GetLength());
	
	// 更新状态栏
	_tstring str;
	clean_file.filter_to_buffer(str);
	status_buffer = str.c_str();
	UpdateData(TRUE);

}


void CVS清理工具Dlg::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码
	clean_file.start();
}


void CVS清理工具Dlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码	UpdateData(FALSE);
	UpdateData(FALSE);
	clean_file.delete_from_filter(
		edit_to_filter.GetBuffer(),
		edit_to_filter.GetLength());

	//更新状态栏
	_tstring str;
	clean_file.filter_to_buffer(str);
	status_buffer = str.c_str();
	UpdateData(TRUE);
}


void CVS清理工具Dlg::OnDropFiles(HDROP hDropInfo)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	_tstring  str(256,0);

	CDialogEx::OnDropFiles(hDropInfo);
	DragQueryFile(hDropInfo, 0, (LPWSTR)str.c_str(), 256);
	POINT pt;
	DragQueryPoint(hDropInfo, &pt);
	DragFinish(hDropInfo);

	//设置目录
	clean_file.working_dir = str;
}
