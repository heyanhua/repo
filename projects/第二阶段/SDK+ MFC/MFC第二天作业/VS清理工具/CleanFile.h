#pragma once

#include <vector>
#include <windows.h>
#include "D:\repo\sourcs_repo\cpp_helper.hpp"

class CleanFile
{
public:
	CleanFile();
	~CleanFile();
	void start(); //根据vector的信息开始工作
	_tstring  working_dir;//需要清理的目录
private:
	std::vector<_tstring>  filter;
//	std::vector<_tstring>  filter2;


public:
	//添加过滤信息
	void delete_from_filter(LPCTSTR file_type, size_t buffer_lenth);
	void add_to_filter(LPCTSTR file_type , size_t buffer_lenth);
	void clean_a_dir(LPCTSTR dir);
	void filter_to_buffer(_tstring buffer);
};

