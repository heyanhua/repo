
// MFC_第二题Dlg.h : 头文件
//

#pragma once
#include "afxwin.h"


// CMFC_第二题Dlg 对话框
class CMFC_第二题Dlg : public CDialogEx
{
// 构造
public:
	CMFC_第二题Dlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFC__DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnUpdateEdit1();
	afx_msg void OnChangeEdit2();
//	bool isRealChange;
//	CString edit1_str;
//	CString edit2_str;
	CEdit edit1;
	CEdit edit2;
	CString eidt_str;
	CString edit2str;
	afx_msg void OnUpdateEdit2();
	bool isRealChange;
};
