#pragma once


// Ctab_dlg1 对话框

class Ctab_dlg1 : public CDialogEx
{
	DECLARE_DYNAMIC(Ctab_dlg1)

public:
	Ctab_dlg1(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~Ctab_dlg1();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
};
