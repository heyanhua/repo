###控件的通知消息
当控件有事情发生时,它会向父窗口发送通知消息,
最常发生的事件就是鼠标单击消息, 此时控件会向父窗口
发送BN_CLICKED消息,  在wParam参数中包含有通知消息码
(鼠标单击时就是BN_CLICKED)和控件ID, lParam参数包含了
控件的句柄.

####控件通知消息的格式

			ON_通知消息码(nID,memberFun)
			nID是控件ID,memeberFun是消息处理函数名
			此消息宏通常添加到
			BEGIN_MESSAGE_MAP

			END_MESSAGE_MAP之间

消息处理函数声明的格式
afx_msg  void      memeberFun();


