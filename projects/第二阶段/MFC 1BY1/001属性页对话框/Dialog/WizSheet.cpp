// WizSheet.cpp : 实现文件
//

#include "stdafx.h"
#include "WizSheet.h"


// CWizSheet

IMPLEMENT_DYNAMIC(CWizSheet, CPropertySheet)

CWizSheet::CWizSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
	AddPage(&m_summandPage);
	AddPage(&m_addedPage);
	AddPage(&m_resultPage);
}

CWizSheet::CWizSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	AddPage(&m_summandPage);
	AddPage(&m_addedPage);
	AddPage(&m_resultPage);
}

CWizSheet::~CWizSheet()
{
}


BEGIN_MESSAGE_MAP(CWizSheet, CPropertySheet)
END_MESSAGE_MAP()


// CWizSheet 消息处理程序
