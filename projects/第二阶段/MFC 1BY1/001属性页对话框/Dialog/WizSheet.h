#pragma once
#include "AddedPage.h"
#include "Resultpage.h"
#include "SummandPage.h"


// CWizSheet

class CWizSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CWizSheet)

public:
	CWizSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CWizSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	virtual ~CWizSheet();

protected:
	DECLARE_MESSAGE_MAP()


	//���ӱ���:
	CSummandPage  m_summandPage;
	CAddedPage    m_addedPage;
	CResultPage   m_resultPage;
};


