// ResultPage.cpp : 实现文件
//

#include "stdafx.h"
#include "ResultPage.h"
#include "afxdialogex.h"
#include "resource.h"

// CResultPage 对话框

IMPLEMENT_DYNAMIC(CResultPage, CPropertyPage)

CResultPage::CResultPage()
	: CPropertyPage(IDD_DIALOG_RESULT_PAGE)
{

}

CResultPage::~CResultPage()
{
}

void CResultPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CResultPage, CPropertyPage)
END_MESSAGE_MAP()


// CResultPage 消息处理程序


BOOL CResultPage::OnSetActive()
{
	// TODO: 在此添加专用代码和/或调用基类

		//设置父窗口
		auto * psheet = (CPropertySheet *)GetParent();
		psheet->SetFinishText(TEXT("完成"));
	return CPropertyPage::OnSetActive();
}


BOOL CResultPage::OnWizardFinish()
{
	// TODO: 在此添加专用代码和/或调用基类

		MessageBox(TEXT("向导结束"));

	return CPropertyPage::OnWizardFinish();
}
