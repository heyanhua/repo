#pragma once


// CSummandPage 对话框

class CSummandPage : public CPropertyPage
{
	DECLARE_DYNAMIC(CSummandPage)

public:
	CSummandPage();
	virtual ~CSummandPage();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_SUMMAND_PROPERTYPAGE };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnSetActive();
};
