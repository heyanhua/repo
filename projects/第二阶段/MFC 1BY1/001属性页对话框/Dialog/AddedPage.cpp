// AddedPage.cpp : 实现文件
//

#include "stdafx.h"
#include "AddedPage.h"
#include "afxdialogex.h"
#include "resource.h"

// CAddedPage 对话框


IMPLEMENT_DYNAMIC(CAddedPage, CPropertyPage)

CAddedPage::CAddedPage()
	: CPropertyPage(IDD_DIALOG_Oprand2_PropertePage)
{

}

CAddedPage::~CAddedPage()
{
}

void CAddedPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CAddedPage, CPropertyPage)
END_MESSAGE_MAP()


// CAddedPage 消息处理程序
