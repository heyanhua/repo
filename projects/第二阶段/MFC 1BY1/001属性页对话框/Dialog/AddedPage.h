#pragma once


// CAddedPage 对话框

class CAddedPage : public CPropertyPage
{
	DECLARE_DYNAMIC(CAddedPage)

public:
	CAddedPage();   // 标准构造函数
	virtual ~CAddedPage();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_Oprand2_PropertePage };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
};
