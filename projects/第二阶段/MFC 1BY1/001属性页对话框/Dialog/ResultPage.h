#pragma once


// CResultPage 对话框

class CResultPage : public CPropertyPage
{
	DECLARE_DYNAMIC(CResultPage)

public:
	CResultPage();
	virtual ~CResultPage();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_RESULT_PAGE };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnSetActive();
	virtual BOOL OnWizardFinish();
};
