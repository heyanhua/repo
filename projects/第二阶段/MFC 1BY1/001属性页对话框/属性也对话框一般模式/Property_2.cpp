// Property_2.cpp : 实现文件
//

#include "stdafx.h"
#include "属性也对话框一般模式.h"
#include "Property_2.h"
#include "afxdialogex.h"


// CProperty_2 对话框

IMPLEMENT_DYNAMIC(CProperty_2, CPropertyPage)

CProperty_2::CProperty_2()
	: CPropertyPage(IDD_DIALOG_PROPERTY_2)
{

}

CProperty_2::~CProperty_2()
{
}

void CProperty_2::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CProperty_2, CPropertyPage)
END_MESSAGE_MAP()


// CProperty_2 消息处理程序
