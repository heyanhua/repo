#pragma once


// CProperty_2 对话框

class CProperty_2 : public CPropertyPage
{
	DECLARE_DYNAMIC(CProperty_2)

public:
	CProperty_2();
	virtual ~CProperty_2();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_PROPERTY_2 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
};
