
// 属性也对话框一般模式.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号


// C属性也对话框一般模式App: 
// 有关此类的实现，请参阅 属性也对话框一般模式.cpp
//

class C属性也对话框一般模式App : public CWinApp
{
public:
	C属性也对话框一般模式App();

// 重写
public:
	virtual BOOL InitInstance();

// 实现

	DECLARE_MESSAGE_MAP()
};

extern C属性也对话框一般模式App theApp;