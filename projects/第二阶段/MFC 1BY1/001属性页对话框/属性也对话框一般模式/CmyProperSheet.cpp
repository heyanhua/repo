// CmyProperSheet.cpp : 实现文件
//

#include "stdafx.h"
#include "属性也对话框一般模式.h"
#include "CmyProperSheet.h"


// CmyProperSheet

IMPLEMENT_DYNAMIC(CmyProperSheet, CPropertySheet)

CmyProperSheet::CmyProperSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{

	AddPage(&m_property1);
	AddPage(&m_property2);
	AddPage(&m_property3);

}

CmyProperSheet::CmyProperSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	AddPage(&m_property1);
	AddPage(&m_property2);
	AddPage(&m_property3);
}

CmyProperSheet::~CmyProperSheet()
{
}


BEGIN_MESSAGE_MAP(CmyProperSheet, CPropertySheet)
END_MESSAGE_MAP()


// CmyProperSheet 消息处理程序
