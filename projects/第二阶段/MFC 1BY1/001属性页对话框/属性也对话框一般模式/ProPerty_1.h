#pragma once


// CProPerty_1 对话框

class CProPerty_1 : public CPropertyPage
{
	DECLARE_DYNAMIC(CProPerty_1)

public:
	CProPerty_1();
	virtual ~CProPerty_1();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_PROPERTY_1 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
};
