#pragma once


// CProperty_3 对话框

class CProperty_3 : public CPropertyPage
{
	DECLARE_DYNAMIC(CProperty_3)

public:
	CProperty_3();
	virtual ~CProperty_3();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_PROPERTY_3 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
};
