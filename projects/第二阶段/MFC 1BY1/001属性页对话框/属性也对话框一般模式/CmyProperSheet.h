#pragma once
#include "ProPerty_1.h"
#include "Property_2.h"
#include "Property_3.h"

// CmyProperSheet

class CmyProperSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CmyProperSheet)

public:
	CmyProperSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CmyProperSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	virtual ~CmyProperSheet();

protected:
	DECLARE_MESSAGE_MAP()

	//���ӱ���
	CProPerty_1  m_property1;
	CProperty_2  m_property2;
	CProperty_3  m_property3;
};


