
// PE info MFCDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "PE info MFC.h"
#include "PE info MFCDlg.h"
#include "afxdialogex.h"
#include <functional>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif




DWORD easyGetFileMemBuffer(LPCSTR  szFileName, unsigned char * & pbuffer)
{
	OFSTRUCT  ofs = { sizeof(OFSTRUCT) };
	HANDLE hFile = (HANDLE)::OpenFile( szFileName, &ofs, OF_READ);
	if (hFile == (HANDLE)HFILE_ERROR)
		goto Error;
	DWORD FileSize = GetFileSize(hFile, &FileSize);
	if (FileSize == 0)
		goto Error_H;


	auto pFile = new unsigned char[FileSize];
	if (0 == ReadFile(hFile, pFile, FileSize, &FileSize, NULL))
		goto Error_H;

	pbuffer = pFile;
	return FileSize;
Error_H:
	CloseHandle(hFile);
Error:
	pbuffer = nullptr;
	return 0;
}



bool  analysisDosHandle(unsigned char * const szFileBuffer,
	std::function<void(IMAGE_DOS_HEADER const &)>const&  pDosHandleFun)
{
	if (IMAGE_DOS_SIGNATURE == *((int *)szFileBuffer))
		return false;
	if (IMAGE_NT_SIGNATURE == ((PIMAGE_DOS_HEADER)szFileBuffer)->e_lfanew)
		return false;
	if (pDosHandleFun != nullptr)
		pDosHandleFun(*((PIMAGE_DOS_HEADER)szFileBuffer));
	return true;
}




void  analysisNtHandle(unsigned char * const szFileBuffer,
	std::function<void(IMAGE_FILE_HEADER const &)>const& pFileHandleFun,
	std::function<void(IMAGE_OPTIONAL_HEADER32  const &)> const&  pOptHandleFun)
{
	if (pFileHandleFun != nullptr)
		pFileHandleFun(((PIMAGE_NT_HEADERS32)(szFileBuffer +
			((PIMAGE_DOS_HEADER)szFileBuffer)->e_lfanew))->FileHeader);

	if (pOptHandleFun != nullptr)
		pOptHandleFun(((PIMAGE_NT_HEADERS32)(szFileBuffer +
			((PIMAGE_DOS_HEADER)szFileBuffer)->e_lfanew))->OptionalHeader);

}



void DosHandleProc(IMAGE_DOS_HEADER const & dos)
{
	CString str;

	auto	hDosRoot = g_Tree->InsertItem(TEXT("DosHead"));
	//在根节点下插入子节点
	str.Format(TEXT("Dos头标识:%x"), dos.e_magic);
	
	g_Tree->InsertItem(str, 1, 1, hDosRoot, TVI_LAST);
	str.Format(TEXT("PE头偏移:%x"), dos.e_lfanew);
	g_Tree->InsertItem(str, 1, 1, hDosRoot, TVI_LAST);
}

void PEhandleProc(IMAGE_FILE_HEADER const & pe)
{
	CString str;
	auto	hPERoot = g_Tree->InsertItem(TEXT("PEHead"));
	str.Format(TEXT("文件运行的平台:%x"), pe.Machine);
	g_Tree->InsertItem(str, 1, 1, hPERoot, TVI_LAST);
	str.Format(TEXT("符号个数:%x"), pe.NumberOfSymbols);
	g_Tree->InsertItem(str, 1, 1, hPERoot, TVI_LAST);
	str.Format(TEXT("PE文件的属性值:%x"), pe.Characteristics);
	g_Tree->InsertItem(str, 1, 1, hPERoot, TVI_LAST);
	str.Format(TEXT("扩展头的大小:%x"), pe.SizeOfOptionalHeader);
	g_Tree->InsertItem(str, 1, 1, hPERoot, TVI_LAST);
	str.Format(TEXT("区段的数量:%x"), pe.NumberOfSections);
	g_Tree->InsertItem(str, 1, 1, hPERoot, TVI_LAST);

}

void OptionHandleProc(IMAGE_OPTIONAL_HEADER32 const & op)
{
	CString str;
	auto	hOptionRoot = g_Tree->InsertItem(TEXT("OptionHead"));

	str.Format(TEXT("程序入口点:%x"), op.AddressOfEntryPoint);
	g_Tree->InsertItem(str, 1, 1, hOptionRoot, TVI_LAST);

	str.Format(TEXT("默认加载地址:%x"), op.ImageBase);
	g_Tree->InsertItem(str, 1, 1, hOptionRoot, TVI_LAST);
	
	
	str.Format(TEXT("内存加载对齐粒度:%x"), op.SectionAlignment);
	g_Tree->InsertItem(str, 1, 1, hOptionRoot, TVI_LAST);

	str.Format(TEXT("文件镜像对齐粒度:%x"), op.FileAlignment);
	g_Tree->InsertItem(str, 1, 1, hOptionRoot, TVI_LAST);


	str.Format(TEXT("镜像大小:%x"), op.SizeOfImage);
	g_Tree->InsertItem(str, 1, 1, hOptionRoot, TVI_LAST);

	str.Format(TEXT("主文件偏移:%x"), op.SizeOfHeaders);
	g_Tree->InsertItem(str, 1, 1, hOptionRoot, TVI_LAST);

	str.Format(TEXT("程序入口点:%x"), op.AddressOfEntryPoint);
	g_Tree->InsertItem(str, 1, 1, hOptionRoot, TVI_LAST);

	str.Format(TEXT("栈初始值:%x"), op.SizeOfStackCommit);
	g_Tree->InsertItem(str, 1, 1, hOptionRoot, TVI_LAST);

	str.Format(TEXT("堆初始值:%x"), op.SizeOfHeapCommit);
	g_Tree->InsertItem(str, 1, 1, hOptionRoot, TVI_LAST);

	str.Format(TEXT("区块个数:%x"), op.NumberOfRvaAndSizes);
	g_Tree->InsertItem(str, 1, 1, hOptionRoot, TVI_LAST);


	str.Format(TEXT("节表:"));
	auto   hSelTable = g_Tree->InsertItem(str, 1, 1, hOptionRoot, TVI_LAST);

	TCHAR * buffer[] = { TEXT("导出表"),
		TEXT("导入表"), TEXT("资源"), TEXT("异常处理"), TEXT("安全结构"), TEXT("基址重定位"),
		TEXT("调试信息"), TEXT("版权"), TEXT("全局指针"), TEXT("TLS初始化"), TEXT("载入配置"),
		TEXT("绑定输入目录"), TEXT("导入地址表"), TEXT("延迟载入描述"), TEXT("COM信息"), TEXT("保留") };
	for (unsigned int i = 0; i < op.NumberOfRvaAndSizes; i++)
	{
		str.Format(TEXT("%s\t大小:%x\t虚拟地址%x"),buffer[i], op.DataDirectory[i].Size
			, op.DataDirectory[i].VirtualAddress);
		g_Tree->InsertItem(str, 1, 1, hSelTable, TVI_LAST);
	}



}















// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CPEinfoMFCDlg 对话框



CPEinfoMFCDlg::CPEinfoMFCDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_PEINFOMFC_DIALOG, pParent)
	, m_FileName('\0',256)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_buffer = nullptr;
}

void CPEinfoMFCDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TREE_MAIN, m_Tree);
	DDX_Control(pDX, IDC_EDIT1, m_edit);
}

BEGIN_MESSAGE_MAP(CPEinfoMFCDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DROPFILES()
	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE_MAIN, &CPEinfoMFCDlg::OnTvnSelchangedTreeMain)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CPEinfoMFCDlg 消息处理程序

BOOL CPEinfoMFCDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	g_Tree = &m_Tree;

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CPEinfoMFCDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CPEinfoMFCDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CPEinfoMFCDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



#define WCHAR_TO_CHAR(lpW_Char, lpChar)\
	WideCharToMultiByte(CP_ACP, NULL, lpW_Char, -1, \
	lpChar, _countof(lpChar), NULL, FALSE)


void CPEinfoMFCDlg::OnDropFiles(HDROP hDropInfo)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	//DragFinish、 DragQueryFile和 DragQueryPoint
	

	DragQueryFile(hDropInfo,0, m_FileName.GetBuffer(), 256);
	DragFinish(hDropInfo);

	// 处理拖拽
	if(m_buffer != nullptr)
	{
		delete[] m_buffer;
		m_buffer = nullptr;
	}
	char tmpFileName[256];
	WCHAR_TO_CHAR(m_FileName.GetBuffer(), tmpFileName);
	easyGetFileMemBuffer( tmpFileName, m_buffer);


	//分析并处理PE文件
	if (0 != analysisDosHandle(m_buffer, nullptr))
	{
		m_Tree.DeleteAllItems();
		analysisDosHandle(m_buffer, DosHandleProc);
		analysisNtHandle(m_buffer, PEhandleProc, OptionHandleProc);
	}
	else 
	{
		MessageBox(TEXT("不是PE文件!"));
	}
	CDialogEx::OnDropFiles(hDropInfo);
}


void CPEinfoMFCDlg::OnTvnSelchangedTreeMain(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}



extern  CTreeCtrl  * g_Tree = nullptr;

void CPEinfoMFCDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);
	CRect rect;
	GetClientRect(&rect);
	auto old_right = rect.right;
	rect.right = rect.right / 10 * 5 ;



	//父窗口不能为弹出窗口
	m_Tree.MoveWindow(&rect);
	rect.left = old_right / 10 * 5;
	rect.right = old_right;
	m_edit.MoveWindow(&rect);
	// TODO: 在此处添加消息处理程序代码
}
