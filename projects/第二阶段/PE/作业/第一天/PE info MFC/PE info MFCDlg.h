
// PE info MFCDlg.h : 头文件
//

#pragma once
#include "afxcmn.h"
#include <string.h>
#include "afxwin.h"

// CPEinfoMFCDlg 对话框
class CPEinfoMFCDlg : public CDialogEx
{
// 构造
public:
	CPEinfoMFCDlg(CWnd* pParent = NULL);	// 标准构造函数
	~CPEinfoMFCDlg(){
		if (m_buffer != nullptr)
		{
			delete[] m_buffer;
			m_buffer = nullptr;
	}
}
// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PEINFOMFC_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();


	DECLARE_MESSAGE_MAP()
public:
	afx_msg			void OnDropFiles(HDROP hDropInfo);
	CString			m_FileName;
	afx_msg			void OnTvnSelchangedTreeMain(NMHDR *pNMHDR, LRESULT *pResult);
	CTreeCtrl		m_Tree;
	unsigned char * m_buffer ;
	afx_msg void OnSize(UINT nType, int cx, int cy);
	CEdit m_edit;
};


extern  CTreeCtrl  * g_Tree;