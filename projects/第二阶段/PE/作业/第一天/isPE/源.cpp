

#include <windows.h>
#include <functional>




bool isPEFormat(unsigned char * const szFileBuffer)
{
	if (IMAGE_DOS_SIGNATURE == *((int *)szFileBuffer))
		return false;
	if (IMAGE_NT_SIGNATURE == ((PIMAGE_DOS_HEADER)szFileBuffer)->e_lfanew)
		return false;
	return true;
}


void  analysisNtHandle(unsigned char * const szFileBuffer,
	std::function<void(IMAGE_FILE_HEADER const &)>const& pFileHandleFun,
	std::function<void(IMAGE_OPTIONAL_HEADER32  const &)> const&  pOptHandleFun)
{
	if (pFileHandleFun != nullptr)
		pFileHandleFun(((PIMAGE_NT_HEADERS32)(szFileBuffer +
			((PIMAGE_DOS_HEADER)szFileBuffer)->e_lfanew))->FileHeader);

	if (pOptHandleFun != nullptr)
		pOptHandleFun(((PIMAGE_NT_HEADERS32)(szFileBuffer +
			((PIMAGE_DOS_HEADER)szFileBuffer)->e_lfanew))->OptionalHeader);

}



bool  analysisDosHandle(unsigned char * const szFileBuffer,
	std::function<void(IMAGE_DOS_HEADER const &)>const&  pDosHandleFun)
{
	if (IMAGE_DOS_SIGNATURE == *((int *)szFileBuffer))
		return false;
	if (IMAGE_NT_SIGNATURE == ((PIMAGE_DOS_HEADER)szFileBuffer)->e_lfanew)
		return false;
	if (pDosHandleFun != nullptr)
		pDosHandleFun(*((PIMAGE_DOS_HEADER)szFileBuffer));
	return true;
}


DWORD easyGetFileMemBuffer(PTCHAR  szFileName, unsigned char * & pbuffer)
{
	OFSTRUCT  ofs = { sizeof(OFSTRUCT) };
	HANDLE hFile = (HANDLE)OpenFile(szFileName, &ofs, OF_READ);
	if (hFile == (HANDLE)HFILE_ERROR)
		goto Error;
	DWORD FileSize = GetFileSize(hFile, &FileSize);
	if (FileSize == 0)
		goto Error_H;


	auto pFile = new unsigned char[FileSize];
	if (0 == ReadFile(hFile, pFile, FileSize, &FileSize, NULL))
		goto Error_H;

	pbuffer = pFile;
	return FileSize;
Error_H:
	CloseHandle(hFile);
Error:
	pbuffer = nullptr;
	return 0;
}

#include <iostream>
using namespace std;
#include <functional>
using std::function;



/**********************************************************************************************//**
 * @fn	void OutDosHandle(IMAGE_DOS_HEADER & const dos)
 *
 * @brief	Out dos handle.
 *
 * @author	Win 7
 * @date	2016/3/25
 *
 * @param [in,out]	dos	The dos.
 **************************************************************************************************/

void  OutDosHandle(IMAGE_DOS_HEADER const &  dos)
{
	if (IMAGE_DOS_SIGNATURE == dos.e_magic && IMAGE_NT_SIGNATURE == dos.e_lfanew)
		cout << "PE!!";
}

/**********************************************************************************************//**
 * @fn	void OutPEhandle(IMAGE_FILE_HEADER & const pe)
 *
 * @brief	Out p ehandle.
 *
 * @author	Win 7
 * @date	2016/3/25
 *
 * @param [in,out]	pe	The pe.
 **************************************************************************************************/

#include <atlstr.h>
#include <time.h>
void OutPEhandle(IMAGE_FILE_HEADER const&  pe)
{
	cout << "文件运行的平台:\t" << hex << pe.Machine << endl;
	cout << "区段的数量:\t" << hex << pe.NumberOfSymbols << endl;
	cout << "PE文件的属性值:\t" << hex << pe.Characteristics << endl;
	cout << "区段的数量:\t" << hex << pe.NumberOfSections << endl;
	cout << "PointerToSymbolTable:\t" << hex << pe.PointerToSymbolTable << endl;
	cout << "扩展头的大小:\t" << hex << pe.SizeOfOptionalHeader << endl;
	//tm * fileTime = gmtime((time_t *)pe.TimeDateStamp);
	//CString strTime;
	//strTime.Format(TEXT("%d年%d月%日"),fileTime->tm_year,  fileTime->tm_mon, 
	//	fileTime->tm_mday);
	//cout << "时间:\t" << strTime << endl;
}

/**********************************************************************************************//**
 * @fn	void OutOptionHandle(IMAGE_OPTIONAL_HEADER32 & const op)
 *
 * @brief	Out option handle.
 *
 * @author	Win 7
 * @date	2016/3/25
 *
 * @param [in,out]	op	The operation.
 **************************************************************************************************/

void OutOptionHandle(IMAGE_OPTIONAL_HEADER32 const&  op)
{
	cout <<"程序入口点"<<hex<< op.AddressOfEntryPoint <<endl;
	cout <<"默认加载地址" << hex << op.ImageBase << endl;
	cout << "内存加载对齐粒度" << hex << op.SectionAlignment << endl;
	cout << "文件镜像对齐粒度" << hex << op.FileAlignment << endl;
	cout << "镜像大小" << hex << op.SizeOfImage << endl;
	cout << "主文件偏移" << hex << op.SizeOfHeaders << endl;
	cout << "程序入口点" << hex << op.AddressOfEntryPoint << endl;
	cout << "栈初始值" << hex << op.SizeOfStackCommit << endl;
	cout << "堆初始值" << hex << op.SizeOfHeapCommit << endl;
	cout << "区块个数" << hex << op.NumberOfRvaAndSizes << endl;

	for (unsigned int i = 0; i < op.NumberOfRvaAndSizes; i++)
	{
		cout << "第" << i << "个区块大小:" << op.DataDirectory[i].Size \
			<< "虚拟地址" << op.DataDirectory[i].VirtualAddress << endl;
	}
}


/**********************************************************************************************//**
 * @fn	int main()
 *
 * @brief	Main entry-point for this application.
 *
 * @author	Win 7
 * @date	2016/3/25
 *
 * @return	Exit-code for the process - 0 for success, else an error code.
 **************************************************************************************************/

/**********************************************************************************************//**
 * @fn	int main()
 *
 * @brief	Main entry-point for this application.
 *
 * @author	Win 7
 * @date	2016/3/25
 *
 * @return	Exit-code for the process - 0 for success, else an error code.
 **************************************************************************************************/

int main()
{

	unsigned char * filebuffer = nullptr; 
	//读取文件到buffer
	auto FileSize =  easyGetFileMemBuffer( TEXT("C:\\Users\\win7\\Desktop\\逆向练习0.exe"), filebuffer);

	//解析并执行操作
	if (0 != analysisDosHandle(filebuffer, nullptr)) //只能使用nullptr 而不能使用0或者NULL
	{
		analysisDosHandle(filebuffer, OutDosHandle);
		analysisNtHandle(filebuffer, OutPEhandle, OutOptionHandle);
	}

	delete []filebuffer;
	system("pause");
	return 0;

}