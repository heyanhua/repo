1. PE头
	1.1	DOS标识	  	MZ
	1.2	NT头	    	[offset:3C]
	1.3	NT头标识    	DWORD: [offset:3c]
	1.4	是否64位	
		64位：		WORD: [NT头 + 0x14] == F0
	
	1.5	区段个数：	DWORD:[NT头 + 0x10]
		基地址：	
		64位:		QWORD:[NT头 + 0x30]
		32位：		DWORD:[NT头 + 0x34]	


	1.6	DataDrictory	size:0x80BYTE
		64位:		NT头 + 88
		32位：		NT头 + 78

	1.7	DataDrictory
	//0. Export Directory
	//1. Import Directory
	//2. Resource Directory
	//3. Exception Directory
	//4. Security Directory
	//5. Base Relocation Table
	//6. Debug Directory
	//7. Architecture Specific Data
	//8. RVA of GP
	//9. TLS Directory
	//a. Load Configuration Directory
	//b. Bound Import Directory in headers
	//c. Import Address Table
	//d. Delay Load Import Descriptors
	//e. COM Runtime descriptor
	//f. 0结尾
2. 区段头	
	2.1	64位		NT头 + 0x108	
		32位		NT头 + 0xF8
        2.2     每个区段大小	0x28

	typedef struct _IMAGE_SECTION_HEADER {
    BYTE    Name[IMAGE_SIZEOF_SHORT_NAME];  //8BYTE
    union {
            DWORD   PhysicalAddress;
            DWORD   VirtualSize;
    } Misc;
    DWORD   VirtualAddress;
    DWORD   SizeOfRawData;
    DWORD   PointerToRawData;
    DWORD   PointerToRelocations;
    DWORD   PointerToLinenumbers;
    WORD    NumberOfRelocations;
    WORD    NumberOfLinenumbers;
    DWORD   Characteristics;
} IMAGE_SECTION_HEADER, *PIMAGE_SECTION_HEADER;


3. 分析各区段信息
	
序号   	VirtualAddress;	  SizeOfRawData;  PointerToRawData;	Rva2Offset
   	   1000h	   80600h		400h		  -c00			   82000h	   10000h		80A00h		  -1600  
	   92000h	   1A00h		90A00h		  -1600	  
	   94000h	   9600h		92400h		  -1C00
	   9E000h	   5a400h		9ba00h		  -2600
	   f9000h	   400h			f5e00h		  -3200


4.  解析导入表
    
    
	   		