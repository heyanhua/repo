

#include <windows.h>
#include <iostream>
using std::cout;
int  mainnn()
{

	auto hFile = CreateFile(TEXT("C:\\Users\\win7\\Desktop\\逆向练习5.exe"),
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		NULL, NULL);

	auto size = GetFileSize(hFile, NULL);
	unsigned char * buffer = new unsigned char[size];

	ReadFile(hFile, buffer, size, &size, NULL);
	CloseHandle(hFile);

	//1.判断并输出是否为PE文件
	PIMAGE_DOS_HEADER  pDos_Header = (PIMAGE_DOS_HEADER)buffer;
	PIMAGE_NT_HEADERS  pNT_Header = (PIMAGE_NT_HEADERS)buffer + pDos_Header->e_lfanew;


	if (pDos_Header->e_magic != IMAGE_DOS_SIGNATURE
		|| pNT_Header->Signature != IMAGE_NT_SIGNATURE)
		cout << "不是PE文件";



	//2.输出NT头-File头各字段
	cout << pNT_Header->FileHeader.Machine; //机器硬件
	cout << pNT_Header->FileHeader.Characteristics; //属性

	cout << pNT_Header->FileHeader.NumberOfSections; //Section总数

	cout << pNT_Header->FileHeader.NumberOfSymbols; // Obj使用

	cout << pNT_Header->FileHeader.PointerToSymbolTable; // Obj使用

	cout << pNT_Header->FileHeader.SizeOfOptionalHeader; // OptionalHeader 大小  可以用来判断是否64位

	cout << pNT_Header->FileHeader.TimeDateStamp; // 时间戳

	auto isNT64 = [&]()->bool
	{
		pNT_Header->FileHeader.SizeOfOptionalHeader == sizeof(IMAGE_OPTIONAL_HEADER64);
	};

	if (isNT64())
		cout << "32位程序";
	else
		cout << "64位程序";

	//3. 输出Option头字段

	cout << pNT_Header->OptionalHeader.ImageBase; //基地址
	cout << pNT_Header->OptionalHeader.BaseOfCode;//code段 Rva
	cout << pNT_Header->OptionalHeader.BaseOfData; //data段 Rva
	cout << pNT_Header->OptionalHeader.DataDirectory; //目录数组
	cout << pNT_Header->OptionalHeader.AddressOfEntryPoint; // OEP  Rva
	cout << pNT_Header->OptionalHeader.DllCharacteristics; //Dll 属性
	cout << pNT_Header->OptionalHeader.LoaderFlags; //装载标志
	cout << pNT_Header->OptionalHeader.Magic; //标识  PE32  或 pe 32 +
	cout << pNT_Header->OptionalHeader.MajorImageVersion; //文件主版本
	cout << pNT_Header->OptionalHeader.MinorImageVersion; //文件次版本
	cout << pNT_Header->OptionalHeader.MajorLinkerVersion; //链接主版本
	cout << pNT_Header->OptionalHeader.MinorLinkerVersion; //链接次版本
	cout << pNT_Header->OptionalHeader.Win32VersionValue; //Win32版本值
	cout << pNT_Header->OptionalHeader.Subsystem; //子系统
	cout << pNT_Header->OptionalHeader.SizeOfUninitializedData; //未初始化数据大小  
	cout << pNT_Header->OptionalHeader.SizeOfCode; //代码大小
	cout << pNT_Header->OptionalHeader.SizeOfHeaders; //头大小包括PE NT
	cout << pNT_Header->OptionalHeader.FileAlignment; //文件对齐粒度
	cout << pNT_Header->OptionalHeader.SectionAlignment;//节内存对齐粒度
	cout << pNT_Header->OptionalHeader.NumberOfRvaAndSizes; //数据目录个数
	cout << pNT_Header->OptionalHeader.ImageBase; //基地址

	//4. 数据节表目录
	for (int i = 0; i < pNT_Header->OptionalHeader.NumberOfRvaAndSizes; i++)
	{
		cout << pNT_Header->OptionalHeader.DataDirectory[i].Size;
		cout << pNT_Header->OptionalHeader.DataDirectory[i].VirtualAddress;
	}

	//5.转换
	auto Rva2Offset = [&](DWORD Rva) ->DWORD
	{
		DWORD va = 0;
		PIMAGE_SECTION_HEADER    pSection_Header = nullptr;
		for (int i = 0; i < pNT_Header->FileHeader.NumberOfSections; i++)
		{
			if (pSection_Header->VirtualAddress < Rva
				&& Rva <= pSection_Header->Misc.VirtualSize
				+ pSection_Header->VirtualAddress)
				return Rva - pSection_Header->VirtualAddress + pSection_Header->PointerToRawData;
		}

		return 0;
	};


	//5. 输出每个节表头信息
	auto pSection_Header = PIMAGE_SECTION_HEADER(pNT_Header + isNT64() ?
		sizeof(IMAGE_NT_HEADERS32) : sizeof(IMAGE_NT_HEADERS32));


	for (int i = 0;i< pNT_Header->FileHeader.NumberOfSections ; i++)
	{
	
		cout << pSection_Header->Name;
		cout << pSection_Header->Characteristics;
		cout << pSection_Header->SizeOfRawData;
		cout << pSection_Header->PointerToRawData;
		cout << pSection_Header->VirtualAddress;
		cout << pSection_Header->Misc.VirtualSize;

		pSection_Header ++;
	}


	//6.  输出导出表信息
	
	//6.1 导出表的头
	      auto  pExport_Directory=  (PIMAGE_EXPORT_DIRECTORY)Rva2Offset(pNT_Header->OptionalHeader.DataDirectory \
			   [IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);
		  
		  pExport_Directory->Name;  //文件名  Rva
		  pExport_Directory->Base;  //Ordinals基数
		  pExport_Directory->AddressOfFunctions;    //导出地址数组 入口
		  pExport_Directory->AddressOfNames;	    //导出名字数组 入口
		  pExport_Directory->AddressOfNameOrdinals; //导出序号数组 入口
		 



		  //索引过的设置为true
		  auto indexedAddress =   new bool[pExport_Directory->AddressOfFunctions];


		  //遍历函数名字以及 序号
		  for (int i = 0; i < pExport_Directory->NumberOfNames; i++)
		  {
	
			//输出函数名字	
			((unsigned char *)Rva2Offset(pExport_Directory->AddressOfNames))[i];
			//输出函数序号
			 auto  Ordinal = Rva2Offset(pExport_Directory->AddressOfNameOrdinals)[i];
			//标记函数地址
			((unsigned char *)Rva2Offset(pExport_Directory->AddressOfNameOrdinals))[i];
		  }

		  //遍历函数地址
		  for(int i = 0; i < pExport_Directory->NumberOfFunctions; i++)
		  {
			  //只输出没名字序号的
			  if(!indexedAddress[i])
			  ((unsigned char *) Rva2Offset(pExport_Directory->AddressOfFunctions))[i];
		  }

	//6.  输出导入表信息
	
	//6.1 导入表的头
	      auto  pImport_Directory=  (PIMAGE_IMPORT_DESCRIPTOR)Rva2Offset(pNT_Header->OptionalHeader.DataDirectory \
			   [IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
		  size_t  pImport_Directory_Size = pNT_Header->OptionalHeader.DataDirectory \
			   [IMAGE_DIRECTORY_ENTRY_IMPORT].Size / sizeof(IMAGE_IMPORT_DESCRIPTOR)-1;

		  
		  //遍历每个导入描述表
		  for (size_t i = 0; i < pImport_Directory_Size; i++)
		  {
			  //xxx.dll
			  pExport_Directory->Name;

			  //遍历每个thunk
			  for (int j = 0; j < 0;j++)
			  {
				  //判断是否为序号
				  if (
					  (unsigned int)
					  ((
						  (PIMAGE_THUNK_DATA32)pImport_Directory->OriginalFirstThunk)
						  ->u1.AddressOfData)
					  <= 0x80000000
					  )
				  {
					  //处理名字
					  PIMAGE_IMPORT_BY_NAME ipn = (PIMAGE_IMPORT_BY_NAME)(PIMAGE_THUNK_DATA32)pImport_Directory->OriginalFirstThunk)
					  ->u1.AddressOfData);

					  //处理名字
					  ipn->Name;
				  }
				  else
				  {
					  //处理序号
				  }


			  }
			  pImport_Directory++;
		  }
	
		  // 7.资源信息
		  //6.1 导出表的头
		  auto  pResource_Diectory = (PIMAGE_RESOURCE_DIRECTORY)Rva2Offset(pNT_Header->OptionalHeader.DataDirectory \
			  [IMAGE_DIRECTORY_ENTRY_RESOURCE].VirtualAddress);
		  size_t  pResource_Diectory_Size = pNT_Header->OptionalHeader.DataDirectory \
			  [IMAGE_DIRECTORY_ENTRY_RESOURCE].Size / sizeof(IMAGE_RESOURCE_DATA_ENTRY) - 1;
		
		  //资源的种类
		  auto kindCount = pResource_Diectory->NumberOfNamedEntries;
		  // auto pFirstEntry = pResource_Diectory->

		  //资源种类for循环
		  for (int i = 0; i < pResource_Diectory_Size; i++)
		  {
			  ;
		  }

		  //本种类资源for循环
		  



		  
	delete[]buffer;
}



void main()
{
	//手工查询PE表
	//1. 计算Dos.e_lfanew的编译
	IMAGE_DOS_HEADER     Dos_Header; //0x3c
	auto Roffset_e_lfanew =  (int)((unsigned char *) \
		&Dos_Header.e_lfanew - (unsigned char *)&Dos_Header);     //0x0000 003c  size: DWORD

	//2. 判断是否为64位置PE  
	//2.1 PE_HEADER中有一个OptionalHeaderSize如果大小为
	auto OptionalHeader32Size = sizeof(IMAGE_OPTIONAL_HEADER32);  //0xe0
	auto OptionalHeader64Size = sizeof(IMAGE_OPTIONAL_HEADER64);  //0xf0
	
	IMAGE_NT_HEADERS64     Nt_Headers;

	auto Roffset_OptionalHeaderSize = (int)((unsigned char *) \
		&Nt_Headers.FileHeader.SizeOfOptionalHeader -     //  32位 64位通用
		(unsigned char *)&Nt_Headers) ;                   //  0x14  + e_lfanew  size :WORD;


	auto Roffset_BaseImage = (int)((unsigned char *) \
		&Nt_Headers.OptionalHeader.ImageBase -           //  0x30  64位           int_64    
		(unsigned char *)&Nt_Headers);                   //  0x34  + e_lfanew   size :DWORD;


	auto Roffset_DataDirectory = (int)((unsigned char *) \
		&Nt_Headers.OptionalHeader.DataDirectory -       //  0x88  64位           int_64    
		(unsigned char *)&Nt_Headers);                   //  0x78  + e_lfanew   size :DWORD;


	auto Roffset_DataDirectory_Expory = (int)((unsigned char *) \
		&Nt_Headers.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT] -      
		(unsigned char *)&Nt_Headers);                   //  0x88  64位                  
														 //  0x78  + e_lfanew   size :DWORD(RVA)  +  DWORD(SIZE)
														 
	auto Roffset_DataDirectory_Import = (int)((unsigned char *) \
		&Nt_Headers.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT] -
		(unsigned char *)&Nt_Headers);                   //  0x90  64位           int_64    
														 //  0x80  + e_lfanew   size :DWOR(RVA)  +  DWORD(SIZE);

	auto Roffset_SectionHandle   =  (int)((unsigned char *) \
		&Nt_Headers.OptionalHeader.DataDirectory[16] -   
		(unsigned char *)&Nt_Headers);                   //  0x108  64位           int_64    
														 //  0xf8  + e_lfanew   size :结构体

	auto size = sizeof(IMAGE_IMPORT_DESCRIPTOR);
	

	int  aaa = 1000;
	return;
}
