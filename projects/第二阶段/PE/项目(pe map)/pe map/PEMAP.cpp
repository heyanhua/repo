#include "PEMAP.h"



PEMAP::PEMAP()
{
}


PEMAP::~PEMAP()
{
	if (pDos_Header)
		delete[]pDos_Header;
}

bool PEMAP::DropFile(TCHAR * pFileName)
{
	Clean();
	auto hFile = CreateFile(pFileName,
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		NULL, NULL);

	auto size = GetFileSize(hFile, NULL);
	m_buffer_size = size;
	m_buffer = new unsigned char[size];

	ReadFile(hFile, m_buffer, size, &size, NULL);
	CloseHandle(hFile);


	PIMAGE_NT_HEADERS  _pNT_Header = (PIMAGE_NT_HEADERS)(m_buffer + pDos_Header->e_lfanew);

	//判断是否为PE文件
	if (pDos_Header->e_magic != IMAGE_DOS_SIGNATURE
		|| _pNT_Header->Signature != IMAGE_NT_SIGNATURE)
	{
		delete[] m_buffer;
		m_buffer = nullptr;
		pDos_Header = nullptr;
		return false;
	}


	//初始化数据
	Init();
	return true;	
}

bool PEMAP::Init()
{

		pNt_Headers32 = (PIMAGE_NT_HEADERS32)(m_buffer + pDos_Header->e_lfanew);
		pNt_Headers64 = (PIMAGE_NT_HEADERS64)(m_buffer + pDos_Header->e_lfanew);
		if(is64())
		pSection_Header = (PIMAGE_SECTION_HEADER)( (unsigned char *)m_buffer + sizeof(IMAGE_NT_HEADERS32));
		else
		pSection_Header = (PIMAGE_SECTION_HEADER)((unsigned char *)m_buffer + sizeof(IMAGE_NT_HEADERS64));




		//填充固定格式头信息
		auto  last_p = (unsigned char *)pDos_Header;
		for (auto &item : Dos_HeaderContents)
		{
			item.Roffset = (size_t)last_p;
			last_p += item.size;
		}

		last_p = (unsigned char *)pNt_Headers32;
		for (auto &item : Nt_Headers32Contents)
		{
			item.Roffset = (size_t)last_p;
			last_p += item.size;
		}

		last_p = (unsigned char *)pNt_Headers64;
		for (auto &item : Nt_Headers64Contents)
		{
			item.Roffset = (size_t)last_p;
			last_p += item.size;
		}
		
		last_p = (unsigned char *)(&pNt_Headers32->FileHeader);
		for (auto &item : Nt_Headers_File_HeaderContents)
		{
			item.Roffset = (size_t)last_p;
			last_p += item.size;
		}

		last_p = (unsigned char *)(&pNt_Headers32->OptionalHeader);
		for (auto &item : Nt_Headers_Optional_Header32Contents)
		{
			item.Roffset = (size_t)last_p;
			last_p += item.size;
		}

		last_p = (unsigned char *)(&pNt_Headers64->OptionalHeader);
		for (auto &item : Nt_Headers_Optional_Header64Contents)
		{
			item.Roffset = (size_t)last_p;
			last_p += item.size;
		}





	return true;
}

bool PEMAP::is64()
{
	PIMAGE_NT_HEADERS  _pNT_Header = (PIMAGE_NT_HEADERS)(m_buffer + pDos_Header->e_lfanew);
	return _pNT_Header->FileHeader.SizeOfOptionalHeader == sizeof(IMAGE_OPTIONAL_HEADER64);
}

bool PEMAP::Clean()
{
	if(m_buffer!=nullptr)
	delete[]m_buffer;
	m_buffer = nullptr;
	m_buffer_size = 0;
	pDos_Header = nullptr;
	pNt_Headers32 = nullptr;
	pNt_Headers64 = nullptr;
	pSection_Header = nullptr;

	for (auto &item : Dos_HeaderContents)
		item.Roffset = 0;


	for (auto &item : Nt_Headers32Contents)
		item.Roffset = 0;


	for (auto &item : Nt_Headers64Contents)
		item.Roffset = 0;

	for (auto &item : Nt_Headers_File_HeaderContents)
		item.Roffset = 0;

	for (auto &item : Nt_Headers_Optional_Header32Contents)
		item.Roffset = 0;

	for (auto &item : Nt_Headers_Optional_Header64Contents)
		item.Roffset = 0;


	return false;
}

size_t PEMAP::Rva2Offset(size_t Rva)
{
	
	auto    _pSection_Header = pSection_Header;
	for (int i = 0; i < pNt_Headers32->FileHeader.NumberOfSections; i++)
	{
		_pSection_Header++;
		if (pSection_Header->VirtualAddress < Rva
			&& Rva <= pSection_Header->Misc.VirtualSize
			+ pSection_Header->VirtualAddress)
			return Rva - pSection_Header->VirtualAddress + pSection_Header->PointerToRawData;
		_pSection_Header++;
	}

	return 0;
}
