#pragma once

#include <windows.h>
#include <vector>
using std::vector;
#include <map>
using std::pair;



typedef struct _FieldInfo
{
	wchar_t *const	Name; 
	size_t	 const	size;
	size_t			Roffset; //数据地址
	bool			isStr;

} FieldInfo, *pFieldInfo;


class PEMAP
{
public:
	PEMAP();
	~PEMAP();
public:
	bool	DropFile(TCHAR* pFileName);
	bool	Init(); //初始化数据
	bool	is64(); 
	bool    Clean(); //清理数据
	size_t	Rva2Offset(size_t Rva);
	//四个pe头信息
	unsigned char *			m_buffer = nullptr;
	size_t					m_buffer_size = 0;
	PIMAGE_DOS_HEADER		pDos_Header = nullptr;
	PIMAGE_NT_HEADERS32		pNt_Headers32 = nullptr;
	PIMAGE_NT_HEADERS64		pNt_Headers64 = nullptr;
	PIMAGE_SECTION_HEADER	pSection_Header = nullptr;

	vector< FieldInfo>		Dos_HeaderContents
	{
		{L"e_magic:" , sizeof(	WORD),0},
		{L"e_cblp:" , sizeof(	WORD),0},
		{L"e_cp:" , sizeof(		WORD),0},
		{L"e_crlc:" , sizeof(	WORD),0},
		{L"e_cparhdr:" , sizeof(WORD),0},
		{L"e_minalloc:" , sizeof(WORD),0},
		{L"e_maxalloc:" , sizeof(WORD),0},
		{L"e_ss:" , sizeof(		WORD),0},
		{L"e_sp:" , sizeof(		WORD),0},
		{L"e_csum:" , sizeof(	WORD),0},
		{L"e_ip:" , sizeof(		WORD),0},
		{L"e_cs:" , sizeof(		WORD),0},
		{L"e_lfarlc:" , sizeof(	WORD),0},
		{L"e_ovno:" , sizeof(	WORD),0},
		{L"e_res[4]:" , sizeof(	WORD),0},
		{L"e_oemid:" , sizeof(	WORD),0},
		{L"e_oeminfo:" , sizeof(WORD),0},
		{L"e_res2[10]:" , sizeof(WORD),0},
		{L"e_lfanew:" , sizeof(	LONG),0}
	};
	vector< FieldInfo>		Nt_Headers32Contents
	{
		{L"Signature:", sizeof(DWORD),0},
		{L"FileHeader:", sizeof(IMAGE_FILE_HEADER),0},
		{L"OptionalHeader:",sizeof(IMAGE_OPTIONAL_HEADER32),0}
	};	

	vector< FieldInfo>		Nt_Headers64Contents
	{
		{ L"Signature:", sizeof(DWORD),0 },
		{ L"FileHeader:", sizeof(IMAGE_FILE_HEADER),0 },
		{ L"OptionalHeader:",sizeof(IMAGE_OPTIONAL_HEADER64),0 }
	};

	vector< FieldInfo>		Nt_Headers_File_HeaderContents
	{	
		{L"Machine:" , sizeof(WORD),0},
		{ L"NumberOfSections:" , sizeof(WORD),0},
		{ L"TimeDateStamp:" , sizeof(DWORD),0},
		{ L"PointerToSymbolTable:" , sizeof(DWORD),0},
		{ L"NumberOfSymbols:" , sizeof(DWORD),0},
		{ L"SizeOfOptionalHeader:" , sizeof(WORD),0},
		{ L"Characteristics:" , sizeof(WORD),0}
	};


	vector< FieldInfo>		Nt_Headers_Optional_Header32Contents
	{
	   {L"Magic:", sizeof(WORD),0},
	   {L"MajorLinkerVersion:", sizeof(BYTE),0 },
	   {L"MinorLinkerVersion:", sizeof(BYTE),0 },
	   {L"SizeOfCode:", sizeof(DWORD),0 },
	   {L"SizeOfInitializedData:", sizeof(DWORD),0 },
	   {L"SizeOfUninitializedData:", sizeof(DWORD),0 },
	   {L"AddressOfEntryPoint:", sizeof(DWORD),0 },
	   {L"BaseOfCode:", sizeof(DWORD),0 },
	   {L"BaseOfData:", sizeof(DWORD),0 },
	   {L"ImageBase:", sizeof(DWORD),0 },
	   {L"SectionAlignment:", sizeof(DWORD),0 },
	   {L"FileAlignment:", sizeof(DWORD),0 },
	   {L"MajorOperatingSystemVersion:", sizeof(WORD),0 },
	   {L"MinorOperatingSystemVersion:", sizeof(WORD),0 },
	   {L"MajorImageVersion:", sizeof(WORD),0 },
	   {L"MinorImageVersion:", sizeof(WORD),0 },
	   {L"MajorSubsystemVersion:", sizeof(WORD),0 },
	   {L"MinorSubsystemVersion:", sizeof(WORD),0 },
	   {L"Win32VersionValue:", sizeof(DWORD),0 },
	   {L"SizeOfImage:", sizeof(DWORD),0 },
	   {L"SizeOfHeaders:", sizeof(DWORD),0 },
	   {L"CheckSum:", sizeof(DWORD),0 },
	   {L"Subsystem:", sizeof(WORD),0 },
	   {L"DllCharacteristics:", sizeof(WORD),0 },
	   {L"SizeOfStackReserve:", sizeof(DWORD),0 },
	   {L"SizeOfStackCommit:", sizeof(DWORD),0 },
	   {L"SizeOfHeapReserve:", sizeof(DWORD),0 },
	   {L"SizeOfHeapCommit:", sizeof(DWORD),0 },
	   {L"LoaderFlags:", sizeof(DWORD),0 },
	   {L"NumberOfRvaAndSizes:", sizeof(DWORD),0 },
	   {L"DataDirectory[0x16]:", sizeof(IMAGE_DATA_DIRECTORY)*0x16,0 }
	};

	vector< FieldInfo>		Nt_Headers_Optional_Header64Contents
	{
		{ L"Magic:", sizeof(WORD),0 },
		{ L"MajorLinkerVersion:", sizeof(BYTE),0 },
		{ L"MinorLinkerVersion:", sizeof(BYTE),0 },
		{ L"SizeOfCode:", sizeof(DWORD),0 },
		{ L"SizeOfInitializedData:", sizeof(DWORD),0 },
		{ L"SizeOfUninitializedData:", sizeof(DWORD),0 },
		{ L"AddressOfEntryPoint:", sizeof(DWORD),0 },
		{ L"BaseOfCode:", sizeof(DWORD),0 },
		{ L"ImageBase:", sizeof(ULONGLONG),0 },
		{ L"SectionAlignment:", sizeof(DWORD),0 },
		{ L"FileAlignment:", sizeof(DWORD),0 },
		{ L"MajorOperatingSystemVersion:", sizeof(WORD),0 },
		{ L"MinorOperatingSystemVersion:", sizeof(WORD),0 },
		{ L"MajorImageVersion:", sizeof(WORD),0 },
		{ L"MinorImageVersion:", sizeof(WORD),0 },
		{ L"MajorSubsystemVersion:", sizeof(WORD),0 },
		{ L"MinorSubsystemVersion:", sizeof(WORD),0 },
		{ L"Win32VersionValue:", sizeof(DWORD),0 },
		{ L"SizeOfImage:", sizeof(DWORD),0 },
		{ L"SizeOfHeaders:", sizeof(DWORD),0 },
		{ L"CheckSum:", sizeof(DWORD),0 },
		{ L"Subsystem:", sizeof(WORD),0 },
		{ L"DllCharacteristics:", sizeof(WORD),0 },
		{ L"SizeOfStackReserve:", sizeof(ULONGLONG),0 },
		{ L"SizeOfStackCommit:", sizeof(ULONGLONG),0 },
		{ L"SizeOfHeapReserve:", sizeof(ULONGLONG),0 },
		{ L"SizeOfHeapCommit:", sizeof(ULONGLONG),0 },
		{ L"LoaderFlags:", sizeof(DWORD),0 },
		{ L"NumberOfRvaAndSizes:", sizeof(DWORD),0 },
		{ L"DataDirectory[0x16]:", sizeof(IMAGE_DATA_DIRECTORY) * 0x16,0 }

	};


	vector< FieldInfo>		Section_HeaderContents
	{
		{L"Name[0x8]:", sizeof(BYTE) * 0x8,0,true},
		{L"VirtualSize|| PhysicalAddress:", sizeof(DWORD),0 },
		{L"VirtualAddress:", sizeof(DWORD),0 },
		{L"SizeOfRawData:", sizeof(DWORD),0 },
		{L"PointerToRawData:", sizeof(DWORD),0 },
		{L"PointerToRelocations:", sizeof(DWORD),0 },
		{L"PointerToLinenumbers:", sizeof(DWORD),0 },
		{L"NumberOfRelocations:", sizeof(WORD),0 },
		{L"NumberOfLinenumbers:", sizeof(WORD),0 },
		{L"Characteristics:", sizeof(DWORD),0 }
	};
};


