#pragma once
#include "afxcmn.h"


// CProcessDialogThread 对话框

class CProcessDialogThread : public CDialogEx
{
	DECLARE_DYNAMIC(CProcessDialogThread)

public:
	CProcessDialogThread(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CProcessDialogThread();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PROCESS_DIALOG_THREAD };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListCtrl m_list;
	int       m_SeletePID;
};
