#pragma once

#include <windows.h>
#include <vector>
#include <map>
using std::map;
using std::vector;
#include <string>
using std::wstring;
using std::vector;
#include <TlHelp32.h>  //need   CreateToolhelp32Snapshot等函数
#include <functional>
using std::function;




class CProcessInfo
{
public:

	struct Thread
	{

	};
	struct Handle
	{

	};
	struct Window
	{

	};
	struct Memory
	{

	};

	struct _ProcessInfo
	{
		_ProcessInfo() :szExeFile(MAX_PATH, 0),
			ExeFileSign(MAX_PATH, 0),
			szExePath(MAX_PATH,0),
			th32ProcessID(0),
			th32ParentProcessID(0),
			th32ModuleID(0),
			EPROCESS(0),
			Ring3Access(false)
		{

		}
		wstring			szExeFile;		//文件名  ---Snap获得
		wstring			szExePath;		//文件路径 
		DWORD			th32ProcessID;  //进程ID  ---Snap获得
		DWORD			th32ParentProcessID; //父进程ID  ---Snap获得
		DWORD			th32ModuleID;	//模块ID   ---Snap获得
		DWORD			cntThreads;		//线程总数 ---Snap获得
		LONG			pcPriClassBase; //访问权限 ---Snap获得

		wstring			ExeFileSign;	//文件厂商
		long long		EPROCESS;		//基址
		bool			Ring3Access;	//3环能否访问
		
		vector<DWORD,MODULEENTRY32>	Moudles;
		vector<DWORD,Thread>			Threads;
		vector<DWORD,Memory>			Memorys;
		vector<DWORD,Handle>			Handles;
		vector<DWORD,Window>			Window;
	};
	

public:
	map<DWORD,_ProcessInfo>  m_Data;
	size_t				  m_HideProcessCount =0;
	size_t				  m_Ruing3NotAccessCount =0;

public:
	void	EnumProcessBYSnap(function<void(PROCESSENTRY32 const&)> const& pfun = nullptr);
	void	EnumMoudleBYSnap(DWORD dwPId,  function<void(MODULEENTRY32 const&)> const& pfun = nullptr);
	void	EnumThreadBYSnap();
	
};






//全局变量的前置声明
extern CProcessInfo   g_ProcessInfo;

