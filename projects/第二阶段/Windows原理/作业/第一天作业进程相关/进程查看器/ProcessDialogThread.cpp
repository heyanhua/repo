// ProcessDialogThread.cpp : 实现文件
//

#include "stdafx.h"
#include "进程查看器.h"
#include "ProcessDialogThread.h"
#include "afxdialogex.h"


// CProcessDialogThread 对话框

IMPLEMENT_DYNAMIC(CProcessDialogThread, CDialogEx)

CProcessDialogThread::CProcessDialogThread(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_PROCESS_DIALOG_THREAD, pParent)
{

}

CProcessDialogThread::~CProcessDialogThread()
{
}

void CProcessDialogThread::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_MOUDLE, m_list);
}


BEGIN_MESSAGE_MAP(CProcessDialogThread, CDialogEx)
END_MESSAGE_MAP()


// CProcessDialogThread 消息处理程序


BOOL CProcessDialogThread::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	CRect rect;
	GetClientRect(&rect);

	//初始化线程窗口控件
	m_list.InsertColumn(0, TEXT("线程Id"), 0, rect.Width() / 10);
	m_list.InsertColumn(1, TEXT("ETHREAD"), 0, rect.Width() / 6);
	m_list.InsertColumn(2, TEXT("Teb"), 0, rect.Width() / 6);
	m_list.InsertColumn(3, TEXT("优先级"), 0, rect.Width() / 10);
	m_list.InsertColumn(4, TEXT("线程入口"), 0, rect.Width() / 6);
	m_list.InsertColumn(5, TEXT("模块"), 0, rect.Width() / 7);
	m_list.InsertColumn(6, TEXT("切换次数"), 0, rect.Width() / 10);
	m_list.InsertColumn(7, TEXT("线程状态"), 0, rect.Width() / 8);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}
