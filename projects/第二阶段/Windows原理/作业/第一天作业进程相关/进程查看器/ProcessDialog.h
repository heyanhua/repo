#pragma once
#include "ProcessInfo.h"
#include "ProcessDialogHandle.h"
#include "ProcessDialogMoudle.h"
#include "ProcessDialogThread.h"
#include "afxcmn.h"
#include "afxwin.h"

// CProcessDialog 对话框

class CProcessDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CProcessDialog)

public:
	CProcessDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CProcessDialog();


	
// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PROCESS_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CString& FormatStatusStr(CString& str);
public:
	virtual BOOL			OnInitDialog();
	CListCtrl				m_ProcessListCtrl;
	CStatic					m_StatusBar;
	int						m_SelectedPID; //通过右键菜单选中的PID
	afx_msg void OnRclickPage1ProcessList(NMHDR *pNMHDR, LRESULT *pResult);


	//弹出线程窗口
	afx_msg void On32771();
	afx_msg void On32772();
	afx_msg void On32773();
	afx_msg void On32777();
	afx_msg void On32775();
	afx_msg void On32776();
	afx_msg void On32778();
	afx_msg void On32779();
};
