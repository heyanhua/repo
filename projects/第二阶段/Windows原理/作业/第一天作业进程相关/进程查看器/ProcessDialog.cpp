// ProcessDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "进程查看器.h"
#include "ProcessDialog.h"
#include "afxdialogex.h"


// CProcessDialog 对话框

IMPLEMENT_DYNAMIC(CProcessDialog, CDialogEx)

CProcessDialog::CProcessDialog(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_PROCESS_DIALOG, pParent)
{

}

CProcessDialog::~CProcessDialog()
{
}

void CProcessDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PAGE1_PROCESS_LIST, m_ProcessListCtrl);
	DDX_Control(pDX, IDC_STATIC_PROCESS_STATUS_BAR, m_StatusBar);
}


BEGIN_MESSAGE_MAP(CProcessDialog, CDialogEx)
	ON_NOTIFY(NM_RCLICK, IDC_PAGE1_PROCESS_LIST, &CProcessDialog::OnRclickPage1ProcessList)
	ON_COMMAND(ID_32771, &CProcessDialog::On32771)
	ON_COMMAND(ID_32772, &CProcessDialog::On32772)
	ON_COMMAND(ID_32773, &CProcessDialog::On32773)
	ON_COMMAND(ID_32777, &CProcessDialog::On32777)
	ON_COMMAND(ID_32775, &CProcessDialog::On32775)
	ON_COMMAND(ID_32776, &CProcessDialog::On32776)
	ON_COMMAND(ID_32778, &CProcessDialog::On32778)
	ON_COMMAND(ID_32779, &CProcessDialog::On32779)
END_MESSAGE_MAP()


// CProcessDialog 消息处理程序


BOOL CProcessDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	//获取数据
	g_ProcessInfo.EnumProcessBYSnap();
	for (auto & item : g_ProcessInfo.m_Data)
	{
		g_ProcessInfo.EnumMoudleBYSnap(item.second.th32ProcessID);
	}


	//ui
	// TODO: 在此添加额外的初始化代码

	CRect rect;
	m_ProcessListCtrl.GetClientRect(&rect);
	DWORD dwOldStyle = m_ProcessListCtrl.GetExtendedStyle();

	//2.设置当前控件的扩展风格
	m_ProcessListCtrl.SetExtendedStyle(dwOldStyle | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);


	//添加列:
	m_ProcessListCtrl.InsertColumn(0, TEXT("映像名称"), 0, rect.Width() / 6);
	m_ProcessListCtrl.InsertColumn(1, TEXT("进程ID"), 0, rect.Width() / 14);
	m_ProcessListCtrl.InsertColumn(2, TEXT("父进程ID"), 0, rect.Width() / 10);
	m_ProcessListCtrl.InsertColumn(3, TEXT("映像路径"), 0, rect.Width() / 5);
	m_ProcessListCtrl.InsertColumn(4, TEXT("EPROCESS"), 0, rect.Width() / 6);
	m_ProcessListCtrl.InsertColumn(5, TEXT("应用层访问"), 0, rect.Width() / 8);
	m_ProcessListCtrl.InsertColumn(6, TEXT("文件厂商"), 0, rect.Width() / 6);
	//添加信息
	for (auto &item : g_ProcessInfo.m_Data)
	{
		m_ProcessListCtrl.InsertItem (0,	item.second.szExeFile.c_str());
		m_ProcessListCtrl.SetItemText(0, 1, std::to_wstring(item.second.th32ParentProcessID).c_str());
		m_ProcessListCtrl.SetItemText(0, 2, std::to_wstring(item.second.th32ProcessID).c_str());
		m_ProcessListCtrl.SetItemText(0, 3, item.second.Moudles.empty()?TEXT("") : item.second.szExePath.c_str());
		
	}
	

	//设置主窗口状态条信息
	CString status_str;
	m_StatusBar.SetWindowTextW(FormatStatusStr(status_str));



	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}



CString& CProcessDialog::FormatStatusStr(CString &str)
{
	 str.Format(TEXT("%s%d%s%d%s%d"), TEXT("进程:  "), g_ProcessInfo.m_Data.size(),
		 TEXT(",隐藏进程:  "), g_ProcessInfo.m_HideProcessCount,
		 TEXT(",应用层不可访问:  "), g_ProcessInfo.m_HideProcessCount);
	 return str;
}

void CProcessDialog::OnRclickPage1ProcessList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码

	m_SelectedPID = _ttoi(m_ProcessListCtrl.GetItemText
		(pNMItemActivate->iItem, 1));


	//弹出菜单
	CPoint point;
	::GetCursorPos(&point);
	CMenu menu;
	VERIFY(menu.LoadMenu(IDR_MENU1));      //IDR_MENU_POPUP是新建菜单ID
	CMenu* popup = menu.GetSubMenu(0);
	ASSERT(popup != NULL);
	popup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
	*pResult = 0;
}



//模块
void CProcessDialog::On32771()
{
	// TODO: 在此添加命令处理程序代码


	CProcessDialogMoudle   dialog;
	dialog.m_SeletePID = m_SelectedPID;
	dialog.DoModal();
}

//线程
void CProcessDialog::On32772()
{
	// TODO: 在此添加命令处理程序代码
	CProcessDialogThread   dialog;
	dialog.m_SeletePID = m_SelectedPID;
	dialog.DoModal();

}

//句柄
void CProcessDialog::On32773()
{
	// TODO: 在此添加命令处理程序代码
	CProcessDialogHandle   dialog;
	dialog.m_SeletePID = m_SelectedPID;
	dialog.DoModal();
}


//在进程中查找模块
void CProcessDialog::On32777()
{
	// TODO: 在此添加命令处理程序代码

}


//进程内存
void CProcessDialog::On32775()
{
	// TODO: 在此添加命令处理程序代码

}

//查看进程窗口
void CProcessDialog::On32776()
{
	// TODO: 在此添加命令处理程序代码

}


//查看占用文件
void CProcessDialog::On32778()
{
	// TODO: 在此添加命令处理程序代码

}

//刷新
void CProcessDialog::On32779()
{
	// TODO: 在此添加命令处理程序代码
}
