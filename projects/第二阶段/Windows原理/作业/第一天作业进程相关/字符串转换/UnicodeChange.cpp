#include <stdafx.h>
#include"UnicodeChange.h"


//1.多字节转换为Unicode 转换为等宽
bool multiToUnicode(const std::string& multiText, std::wstring& unicodeText)
{

	//获取字符个数
	int size = ::MultiByteToWideChar(CP_ACP,
		0,
		multiText.c_str(),
		-1,
		NULL,
		0);
	if (0 == size)
	{
		return false;
	}

	wchar_t* wszBuffer = new wchar_t[size + 1];
	::ZeroMemory(wszBuffer, (size + 1) * sizeof(wchar_t));

	//转换字符
	if (0 == ::MultiByteToWideChar(CP_ACP,
		0,
		multiText.c_str(),
		-1,
		wszBuffer,
		size + 1))
	{
		delete[] wszBuffer;
		return false;
	}

	unicodeText = wszBuffer;
	delete[] wszBuffer;
	return true;

}


//2.Unicode转换为多字节
bool UnicodeTomulti(const std::wstring& unicodeText, std::string& multiText)
{

	// Unicode->UTF8的转换
	size_t size = ::WideCharToMultiByte(CP_UTF8,
		0,
		unicodeText.c_str(),
		-1,
		NULL,
		0,
		NULL,
		NULL);

	char* szBuffer = new char[size + 1];
	::ZeroMemory(szBuffer, (size + 1) * sizeof(char));

	if (0 == ::WideCharToMultiByte(CP_UTF8,
		0,
		unicodeText.c_str(),
		-1,
		szBuffer,
		0,
		NULL,
		NULL))
	{
		delete[] szBuffer;
		return false;
	}

	multiText = szBuffer;
	delete[] szBuffer;
	return true;
}



//3.多字节转换为 UTF - 8
//分两步走：
//第一步：将多字节转换为 Unicode；
//第二步，将Unicode转换为 UTF - 8

bool multiToUTF8(const std::string& multiText, std::string& utf8Text)
{

	// 把输入转换为Unicode
	int size = ::MultiByteToWideChar(CP_ACP,
		0,
		multiText.c_str(),
		-1,
		NULL,
		0);
	if (0 == size)
	{
		return false;
	}

	wchar_t* wszBuffer = new wchar_t[size + 1];
	::ZeroMemory(wszBuffer, (size + 1) * sizeof(wchar_t));

	if (0 == ::MultiByteToWideChar(CP_ACP,
		0,
		multiText.c_str(),
		-1,
		wszBuffer,
		size + 1))
	{
		delete[] wszBuffer;
		return false;
	}

	// Unicode->UTF8的转换
	size = ::WideCharToMultiByte(CP_UTF8,
		0,
		wszBuffer,
		-1,
		NULL,
		0,
		NULL,
		NULL);
	if (0 == size)
	{
		delete[] wszBuffer;
		return false;
	}

	char* szBuffer = new char[size + 1];
	::ZeroMemory(szBuffer, (size + 1) * sizeof(char));

	if (0 == ::WideCharToMultiByte(CP_UTF8,
		0,
		wszBuffer,
		-1,
		szBuffer,
		size + 1,
		NULL,
		NULL))
	{
		delete[] wszBuffer;
		delete[] szBuffer;
		return false;
	}

	utf8Text = szBuffer;
	delete[] wszBuffer;
	delete[] szBuffer;
	return true;
}