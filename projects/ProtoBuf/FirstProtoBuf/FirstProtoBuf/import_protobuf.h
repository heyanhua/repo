#pragma once


#ifdef _DEBUG

#pragma comment (lib,"libprotobufd.lib")
#pragma comment (lib,"libprotobuf-lited.lib")
#pragma comment (lib,"libprotocd.lib")

#else



#pragma comment (lib,"libprotobuf.lib")
#pragma comment (lib,"libprotobuf-lite.lib")
#pragma comment (lib,"libprotoc.lib")

#endif
