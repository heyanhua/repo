


#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#include "import_protobuf.h"
#include "person.pb.h"
void main()
{
	GOOGLE_PROTOBUF_VERIFY_VERSION;  //验证protobuf 版本

	tutorial::Person  person{};


	//写
	person.set_age(100);
	person.set_name("name");
	person.set_email("email@email.com");


	//读
	auto age = person.age();
	auto name = person.name();
	auto email =  person.email();
	

	
	void * parray = new char[person.ByteSize()];
	//序列化到buffer
	person.SerializeToArray(parray,256);  
	//从buffer反序列化
    person.ParseFromArray(parray,256);  


	unsigned char data[20]{1,2,3};
	person.SerializeToArray( parray,20); //序列化到数组  why error?


	person.SerializePartialToArray(data, 20);


	//序列化到字符串
	string str;
	person.SerializeToString(&str);    //系列化到字符串
	str[str.find('n',0)] = 'N';

	//从字符串反序列化
	person.ParseFromString(str);       //从一个字符串解析  --反序列化
	

	//序列化到文件
	fstream fs("1234", ios::out | ios::trunc | ios::binary);
	person.SerializeToOstream(&fs);
	fs.flush();
	fs.close();
	fs.clear();

	//从文件反序列化
	fstream fsr("1234", ios::in | ios::binary);
	person.ParseFromIstream(&fsr);
	fs.close();
	fs.clear();



	//从文件反序列化							  
	 age = person.age();     //读


	 name = person.name();
	 email = person.email();
	 person.ParseFromArray(data, 20);

	 age = person.age();     //读
	 name = person.name();
	 email = person.email();

	 int size = sizeof(person);
	 //--------------------------API 学习

	 person.name();  //
	 person.clear_name(); //清理
	 size = person.ByteSize();  //序列化


	


	size =  person.ByteSize();  



	 system("pause");
	
}