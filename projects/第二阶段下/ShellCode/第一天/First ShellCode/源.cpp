#include <windows.h>





char    byteShellCode[] = "";





//ShellCode的调试    02

__declspec(naked) void  asmShellCode()
{

//main
	__asm {
		pushad

		sub   esp,0x60                      //减去ShellCode的大小
		jmp   tag_ShellCodeEntryEip


			//由此往上添加更多的数据



			//tag ShellCodeEntryEip  - 0x4D  -0x23
			//tag_str_GetProcAddress :
			_asm _emit(0x47)_asm _emit(0x65)_asm _emit(0x74)_asm _emit(0x50)
			_asm _emit(0x72)_asm _emit(0x6F)_asm _emit(0x63)_asm _emit(0x41)
			_asm _emit(0x64)_asm _emit(0x64)_asm _emit(0x72)_asm _emit(0x65)
			_asm _emit(0x73)_asm _emit(0x73)_asm _emit(0x00)
			
			
			//tag ShellCodeEntryEip  - 0x3E  -0x23
			//tag_str_LoadLibraryExA :
			_asm _emit(0x4C)_asm _emit(0x6F)_asm _emit(0x61)_asm _emit(0x64)
			_asm _emit(0x4C)_asm _emit(0x69)_asm _emit(0x62)_asm _emit(0x72)
			_asm _emit(0x61)_asm _emit(0x72)_asm _emit(0x79)_asm _emit(0x45)
			_asm _emit(0x78)_asm _emit(0x41)_asm _emit(0x00)
			
			
			//tag  ShellCodeEntryEip  - 0x2F  -0x23
			//tag_str_user32.dll:    
			_asm _emit(0x55)_asm _emit(0x73)_asm _emit(0x65)_asm _emit(0x72)
			_asm _emit(0x33)_asm _emit(0x32)_asm _emit(0x2E)
			_asm _emit(0x64)_asm _emit(0x6C)_asm _emit(0x6C)_asm _emit(0x00)
			

			//tag  ShellCodeEntryEip  - 0x24   -0x23
			//tag_str_MessageBoxA:
			_asm _emit(0x4D)_asm _emit(0x65)_asm _emit(0x73)_asm _emit(0x73)
			_asm _emit(0x61)_asm _emit(0x67)_asm _emit(0x65)_asm _emit(0x42)
			_asm _emit(0x6F)_asm _emit(0x78)_asm _emit(0x41)_asm _emit(0x00)
			


			//tag  ShellCodeEntryEip  - 0x18  -0x23
			//tag_str_ExitProcess:
			_asm _emit(0x45)_asm _emit(0x78)_asm _emit(0x69)_asm _emit(0x74)
			_asm _emit(0x50)_asm _emit(0x72)_asm _emit(0x6F)_asm _emit(0x63)
			_asm _emit(0x65)_asm _emit(0x73)_asm _emit(0x73)_asm _emit(0x00)

			//tag  ShellCodeEntryEip  - 0x0c  -0x23
			//tag_str_Hello World:
			_asm _emit(0x48)_asm _emit(0x65)_asm _emit(0x6C)_asm _emit(0x6C)
			_asm _emit(0x6F)_asm _emit(0x20)_asm _emit(0x57)_asm _emit(0x6F)
			_asm _emit(0x72)_asm _emit(0x6C)_asm _emit(0x64)_asm _emit(0x00)


		//ShellCodeEntryEip[ GetPC后会被填充为正确的地址]

		//返回值  edx  ==  shellcode base address    == ShellCodeEntryEip  
tag_ShellCodeEntryEip:
		call   subFunGetPC

		//返回值  ebx  Kernel32  base address
		call   subFunGetModuleForGetProcAddress
		
		//返回值  eax  subfunGetProcAddress
		
		call   subfunGetProcAddress
		
		push  eax                           // 临时保存--- GetProcAddress
		push  edx                           // 保存    ---

		//获取LoadLibraryExA
		lea    ecx, [edx-0x3e]  //"LoadLibraryExA\0"
		push   ecx
		push   ebx                     //Kernel32.dll
		call   eax                     

			//pay_load函数************************
			//入口 参数1, ShellCode BaseAddress ,   参数2   Kernel32Base
			//     参数3, GetProcAddress            参数4   LoadLibraryExw
		pop    edx
		pop    esi                    // 取出  临时保存的--- GetProcAddress
		//调用PayLoad
		push   eax                    // 参数4   LoadLibraryEx
		push   esi                    // 参数3,  GetProcAddress 
		push   ebx                    // 参数2   Kernel32Base
		push   edx                    // 参数1, ShellCode BaseAddress
		call   Core_PayLoad

		popad


	}





/**********************************************************/
/*   寄存器占用  无,意味着除了出参数之外不破坏任何的寄存器
/**********************************************************/

	//ShellCode找到自己的位置  GetPC
	//入口			无
	//出口			edx
subFunGetPC:
	//寄存器占用	无
	__asm
	{
		//call  Next-1
	 // Next:
		//retn
		//dec   ebx
		//pop   edx

		//当前被调用后栈顶是eip + 5
		//同一个值压入两次,
		//retn用掉一个
		//剩余的一个被带回调用者
		pop    edx
		push   edx
		sub    edx ,0x5       
		retn   
		
	}



	//获取GetProcAddress所在模块的地址
	//入口        无
	//出口        ebx
	//寄存器占用  无
subFunGetModuleForGetProcAddress:
	__asm
	{
		push  esi
		mov  esi,dword ptr fs:[0x30]     //esi  =  PEB地址
		mov  esi,[esi+0x0c]              //esi  =  PEB_LDR_DATA结构的指针
		mov  esi,[esi+0x1c]              //esi  =  InInitializationOrderModuleList
		mov  esi,[esi]				     //esi  =  访问链表中的第二个条目
		mov  ebx,[esi+0x08]				 //ebx  =  获取Kernel32.dll||KernelBase.dll基地址
		pop  esi

		retn
	}


	//获取关键函数地址
	//入口			ebx  dll基地址,    edx   opcode  baseAddress
	//出口			eax  GetProcAddress的地址
	//寄存器占用	无
subfunGetProcAddress:
	__asm
	{
				
		push  ebp
		mov   ebp, esp
		sub   esp, 0x0c	

		//开辟三个局部变量   
		//Local1:EAT VA   
		//Local2:ENT VA   
		//Local3:EOT VA



			//保存关键寄存器  ebx   edx
			push  ebx
			push  edx
	

					//获取EAT,ENT与EOT的地址      //ebx  dll基地址
					mov   esi, [ebx + 0x3c]       //= IMAGE_DOS_HEADER.e_lfanew
					lea   esi, [ebx + esi]        //= PE文件头VA
					mov   esi, [esi + 0x78]		  //= IMAGE_DIRCTORY_EXPORT.virtualAddress   32位78   64位 70
					lea   esi, [ebx + esi]		  //= 导出表 VA

					//获取AddressOfFuntions
					mov   edi, [esi + 0x1c]       //= IMAGE_EXPORT_DIRECTORY.AddressOfFuntions
					lea   edi, [ebx + edi]		  //= EAT  VA
					mov	  [ebp - 0x04], edi       //Local1 = EAT VA

					//获取AddressOfName
					mov   edi, [esi + 0x20]       //= IMAGE_EXPORT_DIRECTORY.AddressOfName
					lea   edi, [ebx + edi]        // = ENT VA
					mov   [ebp - 0x08], edi       //Local2 = ENT VA 

					//获取AddressOfNameOrdinals
					mov   edi, [esi + 0x24]       //= IMAGE_EXPORT_DIRECTORY.AddressOfNameOrdinals
					lea   edi, [ebx + edi]        // = ENT VA
					mov	  [ebp - 0x0c], edi       //Local3 = EOT VA 


			//查找GetProcAddress函数的地址
			//双层循环比较字符串
			xor   eax,eax
			jmp   tag_FirstCmp

			tag_CmpFunNameLoop:
			    inc    eax

			tag_FirstCmp:
				mov    esi, [ebp - 0x08]				 //ENT数组头地址
				mov    esi, [esi + 4*eax]			     //根据计数器偏移来访问每一个函数地址
				lea    esi, [ebx + esi]                  //ENT  va


				lea    edi, [edx - 0x4d]			    //GetProcAddress字符串地址
				mov    ecx, 0x0e
				
				cld
				repe   cmpsb                            //大循环
                 
				jne     tag_CmpFunNameLoop
				//找到地址索引, 地址在eax中
				//使用地址索引, 找到对应的地址序号
				mov    esi,[ebp - 0x0c]                  //序号数组地址
				xor    edi,edi
				mov    di,[esi + eax*2]                 //找到地址数组的索引
				//使用add索引,找到函数名对应的函数地址
				mov    eax,[ebp - 0x04]                
				mov    esi,[eax + edi*4]               //esi =  函数偏移
				//返回获取到的关键函数地址
				
				lea    eax,[ebx + esi]                   //找到  序号对应

				
		
		pop  edx
		pop  ebx
		
		add   esp,0x0c
		pop   ebp
		retn  
	}



	//pay_load函数************************
	//入口 参数1, ShellCode BaseAddress ,+4   参数2   Kernel32Base  +8
	//     参数3, GetProcAddress     +c       参数4   LoadLibraryExw +10
	//出口 eax
	//参数占用  all
Core_PayLoad:
	__asm
	{

		push  ebp
		mov   ebp,esp
		sub   esp, 0x8   //局部变量
		pushad
		//保存寄存器


		//获取MessageBoxA的地址
		xor   eax,eax
		mov   ebx,[ebp + 0x04]      //参数1
		lea   ecx, [ebx - 0x2f  - 0x23]  //"User32.dll\0"
		push  0
		push  0
		push  ecx
		call  [ebp + 0x10]       //LoadLibraryExA  --装载user32.dll
		
		//获取MessageBox地址
		lea   ecx, [ebx  -0x24 - 0x23]  //"MessageBoxA\0"
		push  ecx              //目标函数地址
		push  eax              //模块基地址
		call  [ebp + 0x0c]     //GetProcAddress
		mov   [ebp - 0x04],eax     //Local 1   MessageBoxA  Address

		//获取ExitProc的函数地址
		lea   ecx, [ebx - 0x18 - 0x23]  //"ExitProcess\0"
		push  ecx
		push  [ebp + 0x8]         //Kernel32Base
		call  [ebp + 0x0c]        //GetProcAddress地址
		mov   [ebp - 0x08],eax        //Local 2   ExitProcess Address
		

		//调用Messagebox!
		lea    ecx, [ebx - 0x0c - 0x23]  //"Hello 15pb\0"
		push   0                        //4
		push   ecx                      //3  lpTitle 
		push   ecx                      //2  lpText
		push   0                        //0 
		call   [ebp - 0x04]
		push   0
		call   [ebp - 0x08]				//退出
		//恢复所有寄存器
		popad
		add   esp,0x08
		pop   ebp
		retn
		
	}


}







#include <windows.h>
void main()
{


	//ShellCode的装载  01
	//__asm {
	//	push   ebp
	//	mov    ebp,esp
	//	lea    eax, asmShellCode
	//		ret
	//	push   eax
	//	pop    ebp
	//	ret
	//}

	asmShellCode();

	MessageBoxA(0,0,0,0);
}


