// TCPServer.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "TcpServer.h"

// link with Ws2_32.lib
#pragma comment(lib,"Ws2_32.lib")

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <stdlib.h>   // Needed for _wtoi
#include <thread>

#include "SocketNode.h"

CTcpServer::CTcpServer()
{
	m_isWoring = false;
	m_sockService = INVALID_SOCKET;
	m_sockListen  = INVALID_SOCKET;
	m_hAcceptThread = NULL;
}


CTcpServer::~CTcpServer()
{
}

bool CTcpServer::Init()
{

	//1.初始化套接字资源

	WSADATA  wsd{};
	if (0 != WSAStartup(MAKEWORD(2, 2), &wsd))
	{
		OutputDebugString(TEXT("WSAStartup() Error!"));
		return 0;
	}

	if (2 != LOBYTE(wsd.wVersion) || 2 != HIBYTE(wsd.wVersion))
	{
		OutputDebugString(TEXT("Could not find a usable version of Winsock.dll\n"));
		WSACleanup();
		return 0;
	}





	return true;
}

bool CTcpServer::Start(char *strIP, unsigned short port,int backlog)
{

	//2.创建服务套接字

	socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (INVALID_SOCKET == m_sockService)
	{
		OutputDebugString(TEXT("sListening  socket  create Fail\n"));
		WSACleanup();
		return 0;
	}

	//3.绑定端口到套接字
	SOCKADDR_IN  sockaddr = {};
	ULONG  _addr = 0;
	inet_pton(AF_INET, strIP, (PVOID)&_addr);
	sockaddr.sin_addr.s_addr = htonl(_addr);
	sockaddr.sin_port = htons((short)port);


	// Bind the socket.
	auto iResult = bind(m_sockService, (SOCKADDR *)&sockaddr, sizeof(m_sockService));
	if (iResult == SOCKET_ERROR) {
		OutputDebugString(L"bind failed with error n");
		closesocket(m_sockService);
		WSACleanup();
		return 0;
	}

	//4.开始监听

	if (listen(m_sockListen, backlog) == backlog)
		OutputDebugString(L"listen function fail \n");
	OutputDebugString(L"Listening on socket...\n");


	//5. 开始监听
	std::thread   thd(this->AcceptThread);
	thd.detach();
	m_hAcceptThread =  thd.native_handle();

	m_isWoring = true;
	return true;
}

bool CTcpServer::Exit()
{
	return false;
}

bool CTcpServer::AcceptThread()
{

	while (m_isWoring)
	{
		//6.等待客户端请求
		sockaddr_in  addrClient{};
		int	adddrClientLen = sizeof(sockaddr_in);
		auto  _lenth = sizeof(addrClient);
		SOCKET  sToClientSock = accept( m_sockListen,(sockaddr *)&addrClient, (int *)&_lenth);

		if (sToClientSock == INVALID_SOCKET) {
			OutputDebugString(L"accept failed with\n");
			closesocket(sToClientSock);
			WSACleanup();
			return 1;
		}
		else
			OutputDebugString(L"Client connected.\n");
		


		//构建一个客户端类, 把这个类对象放到vector内保存
		CSocketNode  *  client = new CSocketNode;
		client->m_socket = sToClientSock;
		client->m_addr = addrClient;
		client->m_pointerToContiner = &m_vClientSocket;

		//7. 进行一次收发信息
		client->sendMessageTo(MESSAGETYPE::sendText, "已经连接到服务器:",strlen("已经连接到服务器:"));


		client->recvMessageFrom();


	}
	return false;
}



int main()
{
    return 0;
}
