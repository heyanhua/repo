#pragma once

// link with Ws2_32.lib
#pragma comment(lib,"Ws2_32.lib")

#include <winsock2.h>
#include <ws2tcpip.h>
#include <atlstr.h>
#include <vector>
#include <thread>

	enum MESSAGETYPE {
		sendName = 1,
		sendText ,
		sendCtrl 
	};
typedef   struct _MYMESSAGE {
	short messageType;
	short messageSize;
}MYMESSAGE;




class CSocketNode
{
public:
	CSocketNode();
	~CSocketNode();

	bool  sendMessageTo(MESSAGETYPE type, char* argContext, int ContextSize);

	bool  recvMessageFrom();
	bool  recvThread();


	bool  close();


public:
	SOCKET						m_socket{};
	CString						m_name;
	sockaddr_in					m_addr{};
	bool						m_isClose=true;

	std::vector<CString>		m_SendedMsg;
	std::vector<CSocketNode*>*	m_pointerToContiner = nullptr;



	HANDLE						m_recvThreadHandle;
	


};

