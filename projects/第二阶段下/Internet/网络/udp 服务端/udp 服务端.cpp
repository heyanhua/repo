// udp 服务端.cpp : 定义控制台应用程序的入口点。
//



#include "stdafx.h"


// link with Ws2_32.lib
#pragma comment(lib,"Ws2_32.lib")

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>



int main()
{

	//1.初始化套接字资源

	WSADATA  wsd{};
	if (0 != WSAStartup(MAKEWORD(2, 2), &wsd))
	{
		OutputDebugString(TEXT("WSAStartup() Error!"));
		return 1;
	}

	if (2 != LOBYTE(wsd.wVersion) || 2 != HIBYTE(wsd.wVersion))
	{
		OutputDebugString(TEXT("Could not find a usable version of Winsock.dll\n"));
		WSACleanup();
		return 1;
	}

	//2.创建服务套接字
	SOCKET  service = \
		socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (INVALID_SOCKET == service)
	{
		OutputDebugString(TEXT("sListening  socket  create Fail\n"));
		WSACleanup();
		return 1;
	}

	//3.绑定端口到套接字
	SOCKADDR_IN  sockaddr = {};
	ULONG  _addr = 0;
	inet_pton(AF_INET, "127.0.0.1", (PVOID)&_addr);
	sockaddr.sin_addr.s_addr = htonl(_addr);
	sockaddr.sin_port = htons((short)1234);


	// Bind the socket.
	auto iResult = bind(service, (SOCKADDR *)&sockaddr, sizeof(service));
	if (iResult == SOCKET_ERROR) {
		OutputDebugString(L"bind failed with error n");
		closesocket(service);
		WSACleanup();
		return 1;
	}


	//4. 接收数据
	ZeroMemory(&sockaddr, sizeof(sockaddr));
	int nSize = sizeof(sockaddr);
	char buffer[256] = {};
	auto ret = recvfrom(service, buffer,256,0,(SOCKADDR*)&sockaddr, &nSize);
	if (SOCKET_ERROR == ret)
	{
		
	}


	//5. 发送消息
	char  buf[] = "我是服务器";
	auto ret = sendto(service, buf, _countof(buf), 0, (SOCKADDR*)&sockaddr, nSize);


    return 0;
}

