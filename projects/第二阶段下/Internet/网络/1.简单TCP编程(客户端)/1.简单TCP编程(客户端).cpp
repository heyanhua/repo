// 1.简单TCP编程(客户端).cpp : 定义控制台应用程序的入口点。
//
#ifndef UNICODE
#define UNICODE 1
#endif

#include "stdafx.h"



#include "stdafx.h"


// link with Ws2_32.lib
#pragma comment(lib,"Ws2_32.lib")

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <stdlib.h>   // Needed for _wtoi


int main()
{
	//1.初始化套接字资源
		//a.判断返回值
	WSADATA  wsd{};
	if (0 != WSAStartup(MAKEWORD(2, 2), &wsd))
	{
		OutputDebugString(TEXT("WSAStartup() Error!"));
		return 1;
	}

		//b.判断版本
	if (2 != LOBYTE(wsd.wVersion) || 2 != HIBYTE(wsd.wVersion))
	{
		OutputDebugString(TEXT("Could not find a usable version of Winsock.dll\n"));
		WSACleanup();
		return 1;
	}

	

	//2.创建套接字(connect to service)
	SOCKET  client = \
		socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (INVALID_SOCKET == client)
	{
		OutputDebugString(TEXT("client  socket  create Fail\n"));
		WSACleanup();
		return 1;
	}


	//3.绑定端口到套接字
	SOCKADDR_IN  sockaddr = {};
	sockaddr.sin_family = AF_INET;
	ULONG  _addr = 0;
	inet_pton(AF_INET, "127.0.0.1", (PVOID)&_addr);
	sockaddr.sin_addr.s_addr = htonl(_addr);
	sockaddr.sin_port = htons((short)1324);


	// Bind the socket.
	auto iResult = bind(client, (SOCKADDR *)&sockaddr, sizeof(client));
	if (iResult == SOCKET_ERROR) {
		OutputDebugString(L"bind client socket fail ");
		closesocket(client);
		WSACleanup();
		return 1;
	}


	//4.连接服务器
	sockaddr_in clientService;
	clientService.sin_family = AF_INET;
	inet_pton(AF_INET, "127.0.0.1", (PVOID)&_addr);
	sockaddr.sin_addr.s_addr = htonl(_addr);
	clientService.sin_port = htons(1234);

	auto retVal = connect(client, (SOCKADDR *)&clientService,sizeof(clientService));


	//5.向服务器发送信息
#define BUF_SIZE  256
	char buffer[BUF_SIZE] = {};
	char sendbuffer[BUF_SIZE] = {};
	// Receive until the peer closes the connection
	do {

		ZeroMemory(buffer, BUF_SIZE);
		send(client, buffer, BUF_SIZE, 0);

		//输入信息

		//
		printf("请输入信息:");
		scanf_s("%s",buffer);
		send(client, buffer, BUF_SIZE, 0); //发送


		iResult = send(client, buffer, BUF_SIZE, 0);
		if (iResult > 0)
		{
			ZeroMemory(buffer, BUF_SIZE);
			OutputDebugStringA("Bytes received\n");
		}
		else if (iResult == 0)
			OutputDebugStringA("Connection closed\n");
		else
			OutputDebugStringA("recv failed\n");

	} while (iResult > 0);



	//7.关闭套接字句柄
	closesocket(client);

	//8.释放套接字资源
	WSACleanup();

    return 0;
}


