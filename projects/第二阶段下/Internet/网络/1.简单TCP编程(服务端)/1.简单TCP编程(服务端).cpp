// 1.简单TCP编程(服务端).cpp : 定义控制台应用程序的入口点。
//


#ifndef UNICODE
#define UNICODE 1
#endif

#include "stdafx.h"


// link with Ws2_32.lib
#pragma comment(lib,"Ws2_32.lib")

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <stdlib.h>   // Needed for _wtoi





//Const
#define   MAXLISTEN    12

int main()
{

	//1.初始化套接字资源
	
	WSADATA  wsd{};
	if (0 != WSAStartup(MAKEWORD(2, 2), &wsd))
	{
		OutputDebugString(TEXT("WSAStartup() Error!"));
		return 1;
	}

	if(2 != LOBYTE(wsd.wVersion)  || 2 !=HIBYTE(wsd.wVersion))
	{
		OutputDebugString(TEXT("Could not find a usable version of Winsock.dll\n"));
		WSACleanup();
		return 1;
	}

	//2.创建服务套接字
	SOCKET  service = \
	socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (INVALID_SOCKET == service)
	{
		OutputDebugString(TEXT("sListening  socket  create Fail\n"));
		WSACleanup();
		return 1;
	}

	//3.绑定端口到套接字
	SOCKADDR_IN  sockaddr = {};
	ULONG  _addr = 0;
	inet_pton(AF_INET, "127.0.0.1", (PVOID)&_addr);
	sockaddr.sin_addr.s_addr = htonl(_addr);
	sockaddr.sin_port = htons((short)1234);
	

	// Bind the socket.
	auto iResult = bind(service, (SOCKADDR *)&sockaddr, sizeof(service));
	if (iResult == SOCKET_ERROR) {
		OutputDebugString(L"bind failed with error n");
		closesocket(service);
		WSACleanup();
		return 1;
	}

	//4.创建监听套接字,并监听
	SOCKET ListenSocket = INVALID_SOCKET;

	//----------------------
	// Listen for incoming connection requests 
	// on the created socket
	if (listen(ListenSocket, SOMAXCONN) == SOCKET_ERROR)
		OutputDebugString(L"listen function fail \n");
	OutputDebugString(L"Listening on socket...\n");


	//5.等待连接请求
	// Create a SOCKET for accepting incoming requests.
	OutputDebugString(L"Waiting for client to connect...\n");

	//----------------------
	// Accept the connection.
	auto AcceptSocket = accept(ListenSocket, NULL, NULL);
	if (AcceptSocket == INVALID_SOCKET) {
		OutputDebugString(L"accept failed with\n");
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}
	else
		OutputDebugString(L"Client connected.\n");


	//6.接收数据  并反馈
#define BUF_SIZE  256
	char buffer[BUF_SIZE] = {};
	char sendbuffer[BUF_SIZE] = {};
	// Receive until the peer closes the connection
	do {

		iResult = recv(service, buffer, BUF_SIZE, 0);
		if (iResult > 0)
		{
			ZeroMemory(buffer, BUF_SIZE);
			OutputDebugStringA("Bytes received\n");
			sprintf_s(buffer,"服务端已经收到%s ",buffer);
			send(service, sendbuffer, BUF_SIZE, 0);

			if (strcmp(buffer, "kill"))
				break;
		}
		else if (iResult == 0)
			OutputDebugStringA("Connection closed\n");
		else
			OutputDebugStringA("recv failed\n");

	} while (iResult > 0);

	//7.关闭套接字句柄
	closesocket(ListenSocket);
	closesocket(service);

	//8.释放套接字资源
	WSACleanup();

    return 0;
}

