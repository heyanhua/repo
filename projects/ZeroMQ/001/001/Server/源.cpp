

#include "zmq.h"
#pragma comment (lib,"libzmq.lib")
#include <iostream>

void main()
{

	auto context = zmq_ctx_new();  //创建上下文
	auto socket = zmq_socket(context, ZMQ_REP);  //创建zmq套接字

	zmq_bind(&socket, "tcp://*:12435");

	while(true)
	{
		zmq_msg_t request;
		zmq_msg_init(&request);  //创建并初始化消息
		zmq_msg_recv(&request,&socket,0); //接收消息



		printf("接收到请求:  \n");  //打印消息
		zmq_msg_close(&request);

		//do somgthing

		//应答
		zmq_msg_t reply;
		zmq_msg_init(&reply);
		zmq_msg_init_size(&reply,6);
		memcpy(zmq_msg_data(&reply), "Wrold!",6);

		zmq_msg_send(&reply, socket, 0);
		zmq_msg_close(&reply);

	}

	zmq_close(&socket);
	zmq_ctx_destroy(&context);


}