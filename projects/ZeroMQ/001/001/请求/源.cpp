


#include "zmq.h"
#pragma comment (lib,"libzmq.lib")
#include <iostream>

void main()
{
	auto context = zmq_ctx_new();  //创建上下文
	auto socket = zmq_socket(context, ZMQ_REQ);  //创建reply套接字
	zmq_connect(&socket, "tcp://localhost:12435");
	while (true)
	{

		zmq_msg_t request;
		zmq_msg_init(&request);
		zmq_msg_init_size(&request, 6);
		memcpy(zmq_msg_data(&request), "Wrold!", 6);

		zmq_msg_send(&request, socket, 0);
		zmq_msg_close(&request);



		zmq_msg_t reply;
		zmq_msg_init(&reply);

		zmq_msg_recv(&reply, socket, 0);

		printf("%收到消息%s",zmq_msg_data(&reply));
		zmq_msg_close(&reply);

	}

	zmq_close(&socket);
	zmq_ctx_destroy(&context);


}