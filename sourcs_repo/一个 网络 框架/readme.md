类说明:

BaseObj:          基本操作, 比如方便的错误处理.

BaseSocket:        socket服务的封装, 底层服务.

BaseNode:       端的封装, 主要操作

        选择模型   指向 <span class="hljs-constant">Base</span>NetMode

        config   接收配置信息,如本地<span class="hljs-literal">ip</span>,远程<span class="hljs-literal">ip</span>, 选择的模型,
                 <span class="hljs-constant">Handle</span>类
        <span class="hljs-literal">start</span>    调用模型的<span class="hljs-literal">start</span>
    `</pre>

    BaseNetMode:    网络模型的基类, 主要操作

    <pre>`    <span class="hljs-built_in">Start</span>       开启具体服务
        选择Handle   
    `</pre>

    目前完成的:

    <pre>`        <span class="hljs-attribute">BaseTcpServer</span>  server;
            <span class="hljs-attribute">BaseNetMode</span>    mode;
            <span class="hljs-attribute">myBaseHandle</span>   handle;
    <span class="hljs-attribute">server</span>.<span class="hljs-function">Config</span>(<span class="hljs-function">TEXT</span>(<span class="hljs-string">"127.0.0.1"</span>),<span class="hljs-number">8865</span>,&mode,&handle);

server.Start();