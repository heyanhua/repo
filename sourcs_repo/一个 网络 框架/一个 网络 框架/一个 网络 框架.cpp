// 一个 网络 框架.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "BaseTcpServer.h"
#include "WinMsgNetMode.h"
#include <vector>
#include <atlstr.h>


struct  myConnectData
{
	BaseSocket				Socket;
	std::vector<CString>	vStr;  //[0]存储姓名
};

class myBaseHandle 
	:public BaseHandle
{
public:

	std::vector<myConnectData>  m_vSockets;

	bool Accept(SOCKET & s) {

		//重写回调接口  简单处理接收所有连接请求
		myConnectData  data{ BaseSocket(s), std::vector<CString>() };
		std::move(data);


		//通知用户服务器连接成功, 并提示绑定用户名
		char * sendbuf = "服务器连接成功,请输入用户名:";
		data.Socket.Send(sendbuf, strnlen_s(sendbuf, 256));
		m_vSockets.push_back(data);
		return true;
	};

	//重写接收数据函数 --保存  转发
	virtual  bool Recv(SOCKET & s) {

		char buf[256]{};
		recv(s, buf, 256, MSG_PEEK);
		for (auto &item : m_vSockets)
		{

			//分发 [不向发送者发送,  不向输入用户名的用户发送(用户名被存在第一条)]
			if (item.Socket.m_socket != s && item.vStr.empty())
			{
				item.Socket.Send(buf, strnlen_s(buf, 256));
			}
			else //保存聊天记录
				item.vStr.emplace_back(buf);
		}


		return true;
	};

};





void testTcpServer()
{

	HWND hwnd = nullptr;
	BaseTcpServer   server;
	WinMsgNetMode   mode(hwnd);
	myBaseHandle   handle;
	server.Config(TEXT("127.0.0.1"),8865,&mode,&handle);
	server.Start();
}

void testTcpClient()
{

}

void testUdpServer()
{

}

void testUdpClient()
{

}

int  main()
{
	testTcpServer();
    return 0;
}

