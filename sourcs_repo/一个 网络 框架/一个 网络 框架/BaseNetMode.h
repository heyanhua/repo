#pragma once
#include "BaseObj.h"
#include "BaseHandle.h"

//基本的网络模型
//  网络模型用于沟通基本的端框架 和 业务流程
//  
class BaseNetMode :
	public BaseObj
{
public:
	BaseNetMode();
	virtual ~BaseNetMode();
	virtual  bool Start();
	bool     SetBaseHandle(BaseHandle* pObj);
	BaseHandle*	m_pBaseHandle = nullptr;

public:
	//重要操作 -- 将Socket 进入模型统一管理
	virtual  bool	RegistSocket(SOCKET & s); 
};

