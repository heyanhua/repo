#include "stdafx.h"
#include "WinMsgNetMode.h"



#define USER_WM_SOCKET	WM_USER + 100


WinMsgNetMode::WinMsgNetMode(HWND hwnd)
{
	m_hwnd = hwnd;
}

WinMsgNetMode::~WinMsgNetMode()
{
}

#include <functional>
bool WinMsgNetMode::Start()
{
	auto Functional = std::bind(&WinMsgNetMode::newWndProc, *this, std::placeholders::_1,
		std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
	auto ret =  SetWindowLong(m_hwnd, GWL_WNDPROC, (LONG)&Functional);

	if( 0 == ret)
	{
		OutputDbgError(TEXT(" WinMsgNetMode::Start() -----> SetWindowLong() Error"));
		return false;
	}
	else
	{
		pOriginWndProc = ret;
	}


	return true;
}



bool  WinMsgNetMode::RegistSocket(SOCKET & s)
{
	//ע��Accept  Read  Write 
	if (SOCKET_ERROR == WSAAsyncSelect(s, m_hwnd, USER_WM_SOCKET,
		FD_ACCEPT | FD_WRITE | FD_READ | FD_CONNECT | FD_CLOSE))
	{
		OutputDbgError(TEXT("WinMsgNetMode::RegistSocket -----> WSAAsyncSelect Error"));
		return false;
	}

	return true;
}

typedef  LRESULT(* PWNDPROC)(HWND, UINT, WPARAM, LPARAM);
LRESULT WinMsgNetMode::newWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	LRESULT ret = TRUE;
	if (hWnd == m_hwnd && message == USER_WM_SOCKET)
	{
		int lError = WSAGETSELECTERROR(lParam);
		int lEvent = WSAGETSELECTEVENT(lParam);
		SOCKET  msgSocket = (SOCKET)wParam;
		switch (lEvent)
		{
		case FD_ACCEPT:
			ret = m_pBaseHandle->Accept(msgSocket);
			break;
		case FD_WRITE:
			ret = m_pBaseHandle->Write(msgSocket);
			break;
		case FD_READ:
			ret = m_pBaseHandle->Read(msgSocket);
			break;
		case FD_CONNECT:
			ret = m_pBaseHandle->Connect(msgSocket);
			break;
		case FD_CLOSE:
			ret = m_pBaseHandle->Close(msgSocket);
			break;
		default:
			break;
		}
	}
	else
	{
	((PWNDPROC)pOriginWndProc)(hWnd, message, wParam, lParam);
	}
	return ret;
}
