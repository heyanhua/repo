#include "stdafx.h"
#include "BaseSocket.h"



#include <windows.h>
#include <atlstr.h>
#include <Inaddr.h>





bool BaseSocket::Close()
{
	bool ret = true;
	shutdown(m_socket, SD_BOTH);
	if (SOCKET_ERROR == closesocket(m_socket))
	{
		OutputDbgError(TEXT("\n**********closesocket SOCKET_ERROR **********\t"));
		ret = false;
	}

	ZeroMemory((void *)&m_addr_self, sizeof(m_addr_self));
	ZeroMemory((void *)&m_addr_self, sizeof(m_addr_remote));
	m_socket = INVALID_SOCKET;
	return ret;
}

bool BaseSocket::isValid()
{
	return INVALID_SOCKET != m_socket;
}

bool BaseSocket::Bind(LPCWCHAR  ipStr, short port)
{

	IN_ADDR  IPv4addr{};
	if (1  > InetPton(AF_INET, ipStr, &IPv4addr))
	{
		OutputDbgError(TEXT("\n********** inet_pton error **********\t"));
		return false;
	}
	m_addr_self.sin_family = AF_INET;
	m_addr_self.sin_port = port;
	m_addr_self.sin_addr.S_un.S_addr =/* htonl(IPv4addr.S_un.S_addr)*/ IPv4addr.S_un.S_addr;
	if (SOCKET_ERROR == ::bind(m_socket, (sockaddr *)&m_addr_self, sizeof(m_addr_self)))
	{
		OutputDbgError(TEXT("\n********** bind error **********\t"));
		return false;
	}

	GetAddress();
	return true;
}

bool BaseSocket::Send(char buf[], size_t size, int flag)
{
	if (SOCKET_ERROR == send(m_socket, buf, size, flag))
	{
		OutputDbgError(TEXT("\n********** BaseSocket::HandleSend -> send **********\t"));
		return false;
	}
	return true;
}

bool BaseSocket::SendTo(char buf[], size_t size, LPCWCHAR  ipStr, USHORT port)
{

	//转换地址
	sockaddr_in toaddr{};
	IN_ADDR  IPv4addr{};
	if (1  > InetPton(AF_INET, ipStr, &IPv4addr))
	{
		OutputDbgError(TEXT("\n********** inet_pton error **********\t"));
		return false;
	}

	toaddr.sin_family = AF_INET;
	toaddr.sin_port = port;
	toaddr.sin_addr = IPv4addr;

	//处理发送
	if (SOCKET_ERROR == sendto(m_socket, buf, size, 0, (sockaddr *)&toaddr, sizeof(sockaddr)))
	{
		OutputDbgError(TEXT("\n********** BaseSocket::HandleSendTo -> sendto **********\t"));
		return false;
	}
	return false;
}

bool BaseSocket::Recv(char buf[], size_t size, int flag)
{
	if (SOCKET_ERROR == recv(m_socket , buf, size, flag))
	{
		OutputDbgError(TEXT("\n********** BaseSocket::Recv -> recv **********\t"));
		return false;
	}
	return true;
}

bool BaseSocket::RecvFrom(char buf[], size_t size, LPCWCHAR  ipStr, USHORT port)
{

	//转换地址
	sockaddr_in fromaddr{};
	IN_ADDR  IPv4addr{};
	if (1  > InetPton(AF_INET, ipStr, &IPv4addr))
	{
		OutputDbgError(TEXT("\n********** inet_pton error **********\t"));
		return false;
	}

	fromaddr.sin_family = AF_INET;
	fromaddr.sin_port = port;
	fromaddr.sin_addr = IPv4addr;
	int addrsize = sizeof(fromaddr);
		if (SOCKET_ERROR == ::recvfrom(m_socket, buf, size, 0, (SOCKADDR *)& fromaddr, &addrsize))
		{
			OutputDbgError(TEXT("\n********** RecvFrom  recvfrom **********\t"));
			return false;
		}
	return false;
}

bool BaseSocket::Listen()
{
	if (SOCKET_ERROR == listen(m_socket, SOMAXCONN))
	{
		OutputDbgError(TEXT("\n********** BaseSocket::Listen() error **********\t"));
		return false;
	}
	return true;
}

bool BaseSocket::Connect(LPCWCHAR  RemoteIpStr, short RemotePort)
{
	IN_ADDR  IPv4addr{};
	if (1  > InetPton(AF_INET, RemoteIpStr, &IPv4addr))
	{
		OutputDbgError(TEXT("\n********** inet_pton change error **********\t"));
		return false;
	}
	sockaddr_in  addr_remote{};
	addr_remote.sin_family = AF_INET;
	addr_remote.sin_port = RemotePort;
	addr_remote.sin_addr.S_un.S_addr = IPv4addr.S_un.S_addr;

	//处理连接
	if (SOCKET_ERROR == connect(m_socket, (sockaddr *)&addr_remote, sizeof(addr_remote)))
	{
		OutputDbgError(TEXT("\n**********BaseSocket::Connect  connect **********\t"));
		return false;
	}
	else
		m_addr_remote = addr_remote; //保存远端地址
	return true;
}

SOCKET BaseSocket::Accept()
{
	sockaddr connect_addr{}; int size = sizeof(connect_addr);
	SOCKET  client = accept(m_socket, &connect_addr, &size);
	if (INVALID_SOCKET == client)
	{
		OutputDbgError(TEXT("*************TcpServer::Accept() -> accept  error**************"));
		return INVALID_SOCKET;
	}
	return client;
}

BaseSocket::~BaseSocket()
{
	if (INVALID_SOCKET != m_socket)
		Close();
}

BaseSocket::BaseSocket(int af, int type, int protocol)
{
	if (!st_isReady)
	{
		WSADATA wsaData{};
		if (0 != WSAStartup(MAKEWORD(2, 2), &wsaData)) {
			/* Tell the user that we could not find a usable */
			/* Winsock DLL.                                  */
			printf("WSAStartup failed with error: \n");

		}
		else
			st_isReady = true;


	}

	m_socket = socket(af, type, protocol);
	if (INVALID_SOCKET == m_socket)
		OutputDbgError(TEXT("\n**********socket INVALID_SOCKET **********\t"));
}

BaseSocket::BaseSocket(SOCKET const & s)
{
	m_socket = s;
	GetAddress();
}

bool BaseSocket::GetAddress()
{

	bool  ret = true;
	ZeroMemory(&m_addr_self, sizeof(m_addr_self));
	ZeroMemory(&m_addr_remote, sizeof(m_addr_remote));
	int nAddrLen = sizeof(SOCKADDR_IN);

	//根据远程套接字获取地址信息
	if (SOCKET_ERROR == ::getpeername(m_socket, (SOCKADDR*)&m_addr_remote, &nAddrLen))
	{
		OutputDbgError(TEXT("\n**********Get  remote IP address by socket failed!**********\t"));
		ret = false;
	}

	//根据本地套接字获取地址信息
	if (SOCKET_ERROR == ::getsockname(m_socket, (SOCKADDR*)&m_addr_self, &nAddrLen))
	{
		OutputDbgError(TEXT("\n**********Get  local IP address by socket failed!**********\t"));
		ret = false;
	}
	return ret;
}

bool			BaseSocket::st_isReady = false; //是否加载了dll