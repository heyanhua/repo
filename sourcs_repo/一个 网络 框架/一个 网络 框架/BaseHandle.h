#pragma once
#include "BaseObj.h"
#include <winsock2.h>



//用户实现类,
//挑选需要的函数
//重写

class BaseHandle :
	public BaseObj
{
public:
	BaseHandle();
	virtual ~BaseHandle();
	virtual  bool    Accept(SOCKET &s) { return false; };//接收到的socket
	virtual  bool	 Read(SOCKET &s) { return false; };  //需要Recv的socket
	virtual  bool    Close(SOCKET &s) { return  false; };
	virtual  bool    Write(SOCKET &s) { return  false; };
	virtual  bool    Connect(SOCKET &s) { return  false; };
	//...
};

