#include "stdafx.h"
#include "BaseTcpServer.h"



BaseTcpServer::BaseTcpServer()
	:m_ListenSocket(AF_INET, SOCK_STREAM, IPPROTO_TCP)
{
	;
}

BaseTcpServer::~BaseTcpServer()
{
}


//配置TcpServer   地址, 端口,模型 ,回掉
bool BaseTcpServer::Config(LPCWCHAR ipStr, short port, \
	BaseNetMode  * pNetMode,BaseHandle* pHandle,int MAXLISTEN)
{
	bool ret = true;
	ret =  m_ListenSocket.Bind(ipStr,port);
	ret =  m_ListenSocket.Listen();
	m_pNetMode = pNetMode;
	m_pNetMode->m_pBaseHandle = pHandle;
	return ret;
}

bool BaseTcpServer::Config(LPVOID pData)
{
	struct {
		LPCWCHAR ipStr; 
		short port; 
		BaseNetMode  * pNetMode; 
		BaseHandle* pHandle; 
		int MAXLISTEN;
	}* p = (decltype(p))pData;

	return Config( p->ipStr, p->port,p->pNetMode,p->pHandle, p->MAXLISTEN);
}

