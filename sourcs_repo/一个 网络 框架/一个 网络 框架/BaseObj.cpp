#include "stdafx.h"
#include "BaseObj.h"
#include <windows.h>
#include <atlstr.h>

BaseObj::BaseObj()
{
}


BaseObj::~BaseObj()
{
}


//use as :OutputDbgError(TEXT("\n**********MessageBox Error **********\t"));
void  BaseObj::OutputDbgError(TCHAR * PrefixStr)
{
	LPVOID lpMsgBuf = nullptr;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		GetLastError(),
		0, // Default language
		(LPTSTR)&lpMsgBuf,
		0,
		NULL
		);
	// Display the string.
	OutputDebugString(PrefixStr);
	OutputDebugString((LPTSTR)lpMsgBuf);
	// Free the buffer.
	LocalFree(lpMsgBuf);
}
