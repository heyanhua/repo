#pragma once
#include "BaseObj.h"
#include <winsock2.h>
#include <WS2tcpip.h>    //need  inet_ntop
#pragma comment (lib,"Ws2_32.lib")

class BaseSocket :
	public BaseObj
{
public:
	BaseSocket(int af, int type, int protocol);
	BaseSocket(SOCKET const &s);  //已经存在的Socket, 比如tcp中连接产生的socket
	virtual ~BaseSocket();
public:

	bool	Close();
	bool	isValid();							 //返回当前套接字是否有效
	bool	Bind(LPCWCHAR  ipStr, short port);   //绑定套接字到端口

	bool	Send(char buf[], size_t  size, int flag = MSG_OOB);					 //tcp 发送数据
	bool	SendTo(char buf[], size_t  size, LPCWCHAR  ipStr, USHORT  port);		 //udp 发送数据

	bool	Recv(char buf[], size_t  size, int flag = MSG_OOB);
	bool	RecvFrom(char buf[], size_t size, LPCWCHAR  ipStr, USHORT port);

	bool    Listen();

	bool    Connect(LPCWCHAR  RemoteIpStr, short RemotePort);
	SOCKET  Accept();


public:
	SOCKET			m_socket = INVALID_SOCKET;	   //socket 句柄
	sockaddr_in		m_addr_self = {};   //本地地址
	sockaddr_in		m_addr_remote = {}; //远程地址

private:
	bool			GetAddress();
	static bool     st_isReady; //是否加载了dll
};

