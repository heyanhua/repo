#pragma once
#include "BaseNode.h"
#include "BaseSocket.h"
class BaseTcpServer :
	public BaseNode
{
public:
	BaseTcpServer();
	virtual ~BaseTcpServer();

	bool	Config(LPCWCHAR  ipStr, short port,\
		BaseNetMode  * pNetMode, BaseHandle* pHandle
		, int MAXLISTEN = 5);
	bool	Config(LPVOID pData);
public:
	BaseSocket					m_ListenSocket;
};

