#pragma once
#include "BaseNetMode.h"
#include <windows.h>
#include <functional>
//利用窗口的小消息模型

class WinMsgNetMode :
	public BaseNetMode
{
public:
	WinMsgNetMode(HWND hwnd);
	virtual ~WinMsgNetMode();

	virtual	 bool Start();
	bool	 WinMsgNetMode::RegistSocket(SOCKET & s);
	HWND	 m_hwnd = nullptr;
	LONG     pOriginWndProc;

public:
	LRESULT  newWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
};
