#pragma once
#include "BaseObj.h"
#include "BaseNetMode.h"
class BaseNode :
	public BaseObj
{
public:
	BaseNode();
	virtual ~BaseNode();
	virtual	 bool Start();
	virtual  bool Config(void *  data);          //会被重写, 且应该被重写
			 bool SetNetMode(BaseNetMode * pObj);//选择处理模型

	BaseNetMode * m_pNetMode = nullptr;			 //网络模型[具体的处理流程在网络模型里]
};

