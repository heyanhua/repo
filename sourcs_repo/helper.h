
//平台无关代码

#ifndef HELPER_H
#define HELPER_H



#include <string>
#ifdef UNICODE
#define _tstring  std::wstring
#else
#define _tstring  std::string
#endif // !UNICODE


#include <sstream>
#ifdef UNICODE
#define _tstringstream   std::wstringstream
#define _tistringstream  std::wistringstream
#define _tostringstream  std::wostringstream
#else
#define _tstringstream   std::stringstream
#define _tistringstream  std::wstringstream
#define _tostringstream  std::wstringstream
#endif // !UNICODE


#include <fstream>
#ifdef UNICODE
#define _tfstream   std::wfstream
#define _tifstream  std::wifstream
#define _tofstream  std::wofstream
#else
#define _tfstream   std::fstream
#define _tifstream  std::ifstream
#define _tofstream  std::ofstream
#endif // !UNICODE



#include <iostream>
#ifdef UNICODE
#define _tiostream   std::wiostream
#define _tistream  std::wistream
#define _tostream  std::wostream
#else
#define _tiostream   std::iostream
#define _tistream  std::istream
#define _tostream  std::ostream
#endif // !UNICODE





//延迟调用.------------------------------Begin
template<typename fun, typename... Args>
auto lazy_fun(fun&f, Args&&... args)
{
	return[&f, &args...]{ return f(args...); };
};


////////延迟调用 exmple
//----auto x = lazy_fun(funint, 0);
//----x();
////////////////////////////////////////  end




//更好用的格式化字符串.---------------------Begin

//可变参数模板, "..."是包扩展(Pack Expansion)  
template <typename FIRST, typename... TAIL>
_tstring & to_string(_tstring & str, const FIRST & first, const TAIL&... tail)
{
	_tstringstream   sstream;
	sstream << str;
	sstream << first;
	sstream >> str;
	return to_string(str, tail...);
}
//终止条件 --特化方式
_tstring & to_string(_tstring& str)
{
	return str;
}


////////to_string exmple
//    _tstirng  str;
//----to_string(str ,TEXT("1,2,4,5"),"1",'1',1,1.8997878);
///////////////  end


////////////////////////////////////////  end









#endif HELPER_H