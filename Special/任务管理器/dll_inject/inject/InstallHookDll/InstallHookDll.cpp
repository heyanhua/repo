// InstallHookDll.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"

#include <stdio.h>
#include <stdlib.h>
#include <atlstr.h>
#include <vector>
using std::vector;
#include <map>
using std::pair;
#include "InstallHookDll.h"
#include "CmyProcessInfo.h"




typedef struct _TestResult
{
	bool	isThread; //是否支持线程HOOK
	bool	is_global;
	DWORD   ThreadId; //线程ID
}TestResult;

typedef struct  _HookType
{
	int  idHook;
	bool originType; //支持的类型   false == only Global
}HookType;

HookType    originHook[] = {
	{ WH_CALLWNDPROC, true },
	{ WH_CALLWNDPROCRET, true },
	{ WH_CBT, true },
	{ WH_DEBUG, true },
	{ WH_FOREGROUNDIDLE, true },
	{ WH_GETMESSAGE, true },
	{ WH_JOURNALPLAYBACK ,false },
	{ WH_JOURNALRECORD ,false },
	{ WH_KEYBOARD ,true },
	{ WH_KEYBOARD_LL ,false },
	{ WH_MOUSE ,true },
	{ WH_MOUSE_LL ,false },
	{ WH_MSGFILTER ,true },
	{ WH_SHELL ,true },
	{ WH_SYSMSGFILTER,false }
};

typedef struct _UseHookType
{
	HookType    cnstType;  
	bool		useType;   //采用的类型   false == Global
	DWORD		dwThreadId;//目标线程ID   if（originType == true ||  useType == false ）此值无效
}UseHookType;



HHOOK				g_Hook = nullptr;
DWORD*				g_PID=nullptr;
bool				g_IsGlobal = false;
TCHAR*				g_pszTargetDllName;
extern HMODULE      g_hModule;
extern HANDLE	    g_hEvent;

BOOL   Scope = 0; 
LRESULT CALLBACK NothingProc(
	int code,           
	WPARAM wParam,      
	LPARAM lParam) {     
						

	return CallNextHookEx(g_Hook, code, wParam, lParam);
}


//return value ： false  failt  snap32thread 失败
extern "C" _declspec(dllexport)  BOOL    TestInstallThreadHook \
(DWORD Pid, vector<pair<int, vector<int>>>& result)
{
	HHOOK  hHook = nullptr;
	
	//获取进程的线程列表
	vector<int>  TidList;
	[&]()->DWORD {
		CmyProcessInfo v;
		v.FreshThreadsInfo();
		for (auto &item : v.m_Threads)
		{
			if (item.first == Pid)
			{
				for (auto &it : item.second)
					TidList.push_back(it.th32ThreadID);

				break;
			}
		}
		return true;
	}();

	if (TidList.empty())
		return FALSE;
	[&]() {
		vector<pair<int, vector<int>>> v;
		result.swap(v);
	}();

	//Start Testing...
	auto TidIndex = 0;
	for (auto &it : TidList)  //遍历所有线程
	{

		for (auto &item : originHook) //为每个线程遍历消息表
		{
			if (true == item.originType) //支持线程安装的消息类型
			{
				hHook =
					SetWindowsHookEx(item.idHook,
						NothingProc,
						g_hModule,
						0);
				if (NULL != hHook) //支持a ThreadHook
				{
					bool condfound = false; //找到了
					for (auto &i : result) //将每个线程所支持的线程Hook表存储到出参中
					{
						if (it == i.first) //找到pid所在的vector
						{
							i.second.push_back(item.idHook); 
							condfound = true;
						}
		
					}
					if (condfound == true) //没有找到tid项, 创建tid,并放入结果容器
					{
						vector<int> v;
						v.push_back(item.idHook);
						result.emplace_back(it,v);
					}

					//卸载hook           -----------  每次一成功的安装都会立即卸载
					UnhookWindowsHookEx(hHook);
				}


			}
		}
	}

	return TRUE;
}




//成功则返回消息类型 失败返回false
extern "C" _declspec(dllexport)  BOOL    InjectDllByInstallGlobalHook \
(TCHAR * pszTargetDllName, DWORD Pid)
{
	if (g_Hook == 0)
	{
		for (auto &item : originHook)
		{
				g_Hook =
					SetWindowsHookEx(item.idHook,
						NothingProc,
						g_hModule,
						0);  //全局钩子

				if (NULL != g_Hook) //安装成功
				{

					CString str(pszTargetDllName);
					memcpy_s(g_pszTargetDllName, 256, pszTargetDllName, str.GetLength());
					g_IsGlobal = true;
					*g_PID = Pid;
					return item.idHook;
				}
				//安装钩子失败，尝试安装其它钩子
		}
	}
	g_IsGlobal = false;
	return FALSE;
}


//成功则返回消息类型 失败返回false
extern "C" _declspec(dllexport)  BOOL    InjectDllByInstallThreadHook \
(TCHAR * pszTargetDllName,  DWORD Pid, vector<pair<int, vector<int>>>& resulted)
{
	if (g_Hook == 0)
	{

		for (auto &tid : resulted)
		{
			for (auto &item : originHook)
			{
				if (false == item.originType)
				{
					g_Hook =
						SetWindowsHookEx(item.idHook,
							NothingProc,
							g_hModule,
							tid.first);  //目标线程
				}
				if (NULL != (DWORD)g_Hook)
				{
					g_IsGlobal = true;
					*g_PID = Pid;
					CString str(pszTargetDllName);
					memcpy_s(g_pszTargetDllName, 256, pszTargetDllName, str.GetLength());
					return item.idHook;  //成功安装一种钩子,返回钩子类型
				}
				//安装钩子失败，尝试安装其它钩子
			}
		}
	}
	g_IsGlobal = false;
	return FALSE; //尝试所有钩子都安装失败
}

extern "C" _declspec(dllexport)  BOOL    UnstallTheHook(HHOOK h)
{
	if (h != 0)
	{
		UnhookWindowsHookEx(h);
		g_Hook = 0;
		return TRUE;
	}

	if (g_Hook != 0)
	{
		UnhookWindowsHookEx(g_Hook);
		g_Hook = 0;
		return TRUE;
	}
	return FALSE;
}


// 返回全局hook的支持列表
extern "C" _declspec(dllexport)  BOOL    TestInstallGlobalHook \
(vector<int>& result)
{

		for (auto &item : originHook)  //遍历消息表
		{
			HHOOK  hHook = nullptr;

				hHook =
					SetWindowsHookEx(item.idHook,
						NothingProc,
						g_hModule,
						0);
				if (NULL != hHook) //安装成功
				{				
					result.push_back(item.idHook);
					//卸载hook
					UnhookWindowsHookEx(hHook);
				}


			}
		

	return TRUE;
}

extern "C" _declspec(dllexport) BOOL	 WaitInjectResult()
{
	return	WaitForSingleObject(g_hEvent,-1);
}
