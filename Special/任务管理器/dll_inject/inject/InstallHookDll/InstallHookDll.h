#pragma once


#include <windows.h>

//成功则返回消息类型 失败返回false
extern "C" _declspec(dllexport)  BOOL    InjectDllByInstallGlobalHook \
(TCHAR * pszTargetDllName, DWORD Pid);

//成功则返回消息类型 失败返回false
extern "C" _declspec(dllexport)  BOOL    InjectDllByInstallThreadHook \
(TCHAR * pszTargetDllName, DWORD Pid, vector<pair<int, vector<int>>>& resulted);

extern "C" _declspec(dllexport)  BOOL    UnstallTheHook(HHOOK h=0);

extern "C" _declspec(dllexport)  BOOL    TestInstallThreadHook \
(DWORD Pid, vector<pair<int, vector<int>>>& result);

extern "C" _declspec(dllexport)  BOOL    TestInstallGlobalHook \
(vector<int>& result);

extern "C" _declspec(dllexport)  BOOL    WaitInjectResult();



///// can used  as ...
//auto hDll = LoadLibrary(TEXT("..\\Debug\\InstallHookDll.dll"));
//
//function<BOOL(TCHAR *, DWORD)>
//							InjectDllByInstallGlobalHook = nullptr;
//
//function<BOOL(TCHAR *, DWORD, vector<pair<int, vector<int>>>&)>
//							InjectDllByInstallThreadHook = nullptr;
//
//function<BOOL(HHOOK)>		UnstallTheHook = nullptr;
//
//function<BOOL(DWORD, vector<pair<int, vector<int>>>)>
//							TestInstallThreadHook = nullptr;
//function<BOOL(vector<int>&)>
//							TestInstallGlobalHook = nullptr;
//function<BOOL()>			WaitInjectResult = nullptr;
// 	[&]()
//	{
//		auto hAddress = GetProcAddress(hDll, TEXT("InjectDllByInstallGlobalHook"));
//		if (NULL != (DWORD)hAddress)
//		{
//			InjectDllByInstallGlobalHook = hAddress;
//		}
//		hAddress = GetProcAddress(hDll, TEXT("InjectDllByInstallThreadHook"));
//		if (NULL != (DWORD)hAddress)
//		{
//			InjectDllByInstallThreadHook = hAddress;
//		}
//
//		hAddress = GetProcAddress(hDll, TEXT("UnstallTheHook"));
//		if (NULL != (DWORD)hAddress)
//		{
//			InjectDllByInstallGlobalHook = hAddress;
//		}
//		hAddress = GetProcAddress(hDll, TEXT("TestInstallGlobalHook"));
//		if (NULL != (DWORD)hAddress)
//		{
//			InjectDllByInstallThreadHook = hAddress;
//		}
//		hAddress = GetProcAddress(hDll, TEXT("InjectDllByInstallGlobalHook"));
//		if (NULL != (DWORD)hAddress)
//		{
//			InjectDllByInstallGlobalHook = hAddress;
//		}
//		hAddress = GetProcAddress(hDll, TEXT("WaitInjectResult"));
//		if (NULL != (DWORD)hAddress)
//		{
//			WaitInjectResult = hAddress;
//		}
// }();
