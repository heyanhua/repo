// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "stdafx.h"
#include <Windows.h>
#include <atlstr.h>


#define  FILE_MAPPING_NAME			TEXT("{D0E18829-3159-44AE-A8D3-D6D76}")
#define  SUCCEED_EVENT_NAME			TEXT("{2303360C-1161-4BED-9265-420D81D8643B}")

struct ErrLog         //错误日志 --- 用于目标进程向主线程通告
{
	enum ERR_CODE {
		CREATEFILEMAPPING_NULL = 0,
		
	};
	DWORD		   err_PID;
	DWORD		   err_TID;
	DWORD          err_CODE;//错误号码
};

struct G_VALUE        //用于进程间通信
{
	DWORD		   g_PID;				//想要Hook的PID  ------- 主进程通知(每一个被Hook的dll应该判断此值)
	TCHAR          g_pszTargetDllName[256]; //想要加载的DLl  ------- 主进程通知
	size_t		   g_ErrArrCount;			//错误日志结构体数组的元素数量
	ErrLog		   g_FirstErrLog[1];		//第一个错误信息
};



	   
extern HHOOK	   g_Hook ;
extern DWORD*      g_PID;
extern bool        g_IsGlobal ;
extern TCHAR*      g_pszTargetDllName; //MAX_SIZE 256
	   HMODULE     g_hModule;
	   HANDLE	   g_hEvent;
	   


//直接将全局变量映射到FileMaping中
//所有的进程的dll使用的都为同一个
// 结构体.


BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{

	g_hEvent = CreateEvent(
		NULL,                // default security attributes
		FALSE,               // non-manual-reset event
		FALSE,               // initial state is nonsignaled
		SUCCEED_EVENT_NAME   // object name
		);

	//创建一个进程间通信的文件映射对象
	static  HANDLE  hFileMap = CreateFileMapping(INVALID_HANDLE_VALUE,
		NULL, PAGE_READWRITE,
		0, 4096,
		FILE_MAPPING_NAME);
	static  auto    erFileMapError = GetLastError();



	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	{

		OutputDebugString(TEXT("11111111111111111111111111111"));
		//本地执行
		if (NULL != hFileMap && ERROR_ALREADY_EXISTS != erFileMapError)
		{

			//对象已经存在
			auto pgValue = static_cast<G_VALUE *>(MapViewOfFile(hFileMap, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(G_VALUE)));
			if (NULL == pgValue)
				return FALSE;
			//将全局对象指向共享内存中
			OutputDebugString(TEXT("22222222222222222222222222"));
			g_PID = &(pgValue->g_PID);
			g_pszTargetDllName = (pgValue->g_pszTargetDllName);

		}


		//远程执行
		if (NULL != hFileMap && ERROR_ALREADY_EXISTS == erFileMapError)
		{

			//对象已经存在
			auto pgValue = static_cast<G_VALUE *>(MapViewOfFile(hFileMap, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(G_VALUE)));
			if (NULL == pgValue)
				return FALSE;
			static  auto    erSucceedEventError = GetLastError();
			//将全局对象指向共享内存中
			g_PID = &(pgValue->g_PID);
			g_pszTargetDllName = (pgValue->g_pszTargetDllName);

			//判断当前进程和PID是否匹配
			if (pgValue->g_PID == GetCurrentProcessId())
			{
				//找到目标进程
				//注入
				auto errLb = LoadLibrary(g_pszTargetDllName);


				//报告注入成功
				if (ERROR_ALREADY_EXISTS == erSucceedEventError
					&& NULL != errLb)
					SetEvent(g_hEvent);
				else
					return FALSE;
			}
		}

		OutputDebugString(TEXT("33333333333333333333333"));
	}
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		if (NULL != hFileMap)
			CloseHandle(hFileMap);
		if (NULL != hFileMap)
			CloseHandle(g_hEvent);
		break;
	}
	return TRUE;
}

