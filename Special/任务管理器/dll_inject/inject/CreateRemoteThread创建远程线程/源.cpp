

#include <windows.h>

//远程线程注入
//Point: 利用LoadLibrary函数签名与 CreateRemoteThread函数
// 的回调函数参数签名相同,
// 创建一个远程执行LoadLibrary("自己的dll")的远程线程
// LoadLibrary能够加载成功就能够获得在目标进程中执行自己代码的权限
// 关键点：Loadlibrary函数的参数如何传进目标进程的地址空间
// 返回值 INVALID_HANDLE_VALUE, OpenProcess fail
//        NULL					VirtualAllocEx  fail
//        NULL - 2              WriteProcessMemory fail
//        NULL - 3              CreateRemoteThread fail
//        NULL - 4              VirtualFreeEx      fail
BOOL injectBYCreateRemoteThread(DWORD Pid,PTCHAR szDllName)
{
	//1.打开进程
	auto hProcess = OpenProcess(PROCESS_ALL_ACCESS,FALSE,Pid);
	if (INVALID_HANDLE_VALUE == hProcess)
		return (BOOL)INVALID_HANDLE_VALUE;

	//2.在远程进程中申请空间(用于将自己的dll名字写入目标进程)
	LPVOID  pszRemoteDllName = VirtualAllocEx( hProcess,NULL,4096,MEM_COMMIT,PAGE_READWRITE);
	if (pszRemoteDllName == NULL)
		return NULL;

	//3.写入dll名字
	if (!WriteProcessMemory(hProcess, pszRemoteDllName, szDllName, MAX_PATH, NULL))
		return NULL - 2;

	//4.在远程进程中创建远程线程
	DWORD tidRemoteThread = 0;
	auto hRemoteThread = CreateRemoteThread(
		hProcess,
		NULL,
		0,
		(LPTHREAD_START_ROUTINE)LoadLibrary,
		pszRemoteDllName,
		NULL,
		&tidRemoteThread);
	if (NULL == hRemoteThread)
		return NULL - 3;
	
	//5.等待远程线程结束返回
	DWORD dw = WaitForSingleObject(hRemoteThread, -1);

	//6.获取线程退出码,即LoadLibrary返回
	DWORD dwExitCode;
	GetExitCodeThread(hRemoteThread,&dwExitCode);

	//7.释放空间
	if (!VirtualFreeEx(hProcess, pszRemoteDllName, 4096, MEM_DECOMMIT))
		return NULL - 4;
	CloseHandle(hProcess);

	return TRUE;
}




void main()
{

	auto ret =	injectBYCreateRemoteThread(5664,\
		TEXT("D:\\repo\\special\\dll_inject\\inject\\Debug\\myDll.dll"));


}