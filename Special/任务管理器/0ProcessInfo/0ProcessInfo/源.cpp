

#include "CmyProcessInfo.h"
#include <iostream>
using std::cout;
using std::endl;
void main()
{

	CmyProcessInfo  pi;
	pi.FreshProcessInfo();
	pi.FreshThreadsInfo();

	auto  index  = 0;
	for (auto& item : pi.m_Processes)
	{
		pi.FreshHeapsInfo(item.th32ProcessID);
		pi.FreshModuesInfo(item.th32ProcessID);
		index++;
	}

	return;

}

