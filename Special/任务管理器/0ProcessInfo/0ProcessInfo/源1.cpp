#include <windows.h>
#include <tlhelp32.h>
#include <stdio.h>

int _main(void)
{
	HEAPLIST32 hl;

	HANDLE hHeapSnap = CreateToolhelp32Snapshot(TH32CS_SNAPHEAPLIST, GetCurrentProcessId());

	hl.dwSize = sizeof(HEAPLIST32);

	if (hHeapSnap == INVALID_HANDLE_VALUE)
	{
		printf("CreateToolhelp32Snapshot failed (%d)\n", GetLastError());
		return 1;
	}

	if (Heap32ListFirst(hHeapSnap, &hl))
	{
		do
		{
			HEAPENTRY32 he;
			ZeroMemory(&he, sizeof(HEAPENTRY32));
			he.dwSize = sizeof(HEAPENTRY32);

			if (Heap32First(&he, GetCurrentProcessId(), hl.th32HeapID))
			{
				printf("\nHeap ID: %d\n", hl.th32HeapID);
				do
				{
					printf("Block size: %d\n", he.dwBlockSize);

					he.dwSize = sizeof(HEAPENTRY32);
				} while (Heap32Next(&he));
			}
			hl.dwSize = sizeof(HEAPLIST32);
		} while (Heap32ListNext(hHeapSnap, &hl));
	}
	else printf("Cannot list first heap (%d)\n", GetLastError());

	CloseHandle(hHeapSnap);

	return 0;
}

