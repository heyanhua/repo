// ProcessDialogMoudle.cpp : 实现文件
//

#include "stdafx.h"
#include "进程查看器.h"
#include "ProcessDialogMoudle.h"
#include "ProcessInfo.h"
#include "afxdialogex.h"


// CProcessDialogMoudle 对话框

IMPLEMENT_DYNAMIC(CProcessDialogMoudle, CDialogEx)

CProcessDialogMoudle::CProcessDialogMoudle(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_PROCESS_DIALOG_MOUDLE, pParent)
{

}

CProcessDialogMoudle::~CProcessDialogMoudle()
{
}

void CProcessDialogMoudle::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_MOUDLE, m_list);
}


BEGIN_MESSAGE_MAP(CProcessDialogMoudle, CDialogEx)
END_MESSAGE_MAP()


// CProcessDialogMoudle 消息处理程序


BOOL CProcessDialogMoudle::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	CRect rect;
	GetClientRect(&rect);
	//初始化句柄窗口控件
	m_list.InsertColumn(0, TEXT("文件路径"), 0, rect.Width() / 3);
	m_list.InsertColumn(1, TEXT("基址"), 0, rect.Width() / 6);
	m_list.InsertColumn(2, TEXT("大小"), 0, rect.Width() / 6);
	m_list.InsertColumn(3, TEXT("文件厂商"), 0, rect.Width() / 3);

	//添加信息
	DWORD				   nCount = 0; //模块总数
	CString				   szProcessName; //模块名字
	g_ProcessInfo.EnumMoudleBYSnap(m_SeletePID);
	//	//添加信息



	for (auto &it : g_ProcessInfo.m_Data)
	{
		if (it.second.th32ProcessID == m_SeletePID)
		{
			//获取模块信息

			nCount = it.second.Moudles.size();
			szProcessName = it.second.szExeFile.c_str();

			//遍历并插入每个模块的信息
			for (auto &item : it.second.Moudles)
			{
				m_list.InsertItem(0, item.second.szExePath);
				m_list.SetItemText(0, 1, std::to_wstring((DWORD)item.second.modBaseAddr).c_str());
				m_list.SetItemText(0, 2, std::to_wstring(item.second.modBaseSize).c_str());
				m_list.SetItemText(0, 3, item.second.szModule);
			}
			if (it.second.th32ProcessID == m_SeletePID)
				break;
		}
	}


	CString str;
	str.Format(TEXT("[%s]进程模块(%d)"), szProcessName, nCount);
	SetWindowText(str);



	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}
