#pragma once
#include "afxcmn.h"


// CProcessDialogHandle 对话框

class CProcessDialogHandle : public CDialogEx
{
	DECLARE_DYNAMIC(CProcessDialogHandle)

public:
	CProcessDialogHandle(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CProcessDialogHandle();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PROCESS_DIALOG_HANDLE };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListCtrl m_list;
	int       m_SeletePID;
};
