
// 0ProcessInfoDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "0ProcessInfo.h"
#include "0ProcessInfoDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// C0ProcessInfoDlg 对话框



C0ProcessInfoDlg::C0ProcessInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_MY0PROCESSINFO_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void C0ProcessInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB1, m_tab);
}

BEGIN_MESSAGE_MAP(C0ProcessInfoDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, &C0ProcessInfoDlg::OnTcnSelchangeTab1)
	ON_WM_SIZE()
END_MESSAGE_MAP()


// C0ProcessInfoDlg 消息处理程序

BOOL C0ProcessInfoDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	//CRect tabRect;
	//m_Tab.GetClientRect(&tabRect);
	m_tab.InsertItem(0, TEXT("进程")); //程序的绝大部分功能
	m_tab.InsertItem(1, TEXT("程序")); //窗口程序信息
	m_tab.InsertItem(2, TEXT("PE"));   //pe map
	m_tab.InsertItem(3, TEXT("文件")); //explor  + 文件信息
	m_tab.InsertItem(4, TEXT("服务")); //
	m_tab.InsertItem(5, TEXT("内存")); //内存图
	//m_ProcessDlg.Create(IDD_PROCESS_DIALOG, &m_TabCtrl);

	//tabRect.top += 21;
	//m_ProcessDlg.MoveWindow(&tabRect);
	//m_ProcessDlg.ShowWindow(SW_SHOW);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void C0ProcessInfoDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR C0ProcessInfoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void C0ProcessInfoDlg::OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}


void C0ProcessInfoDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);
	CRect rect{0,0,cx,cy};

	m_tab.MoveWindow(&rect);
	// TODO: 在此处添加消息处理程序代码
}
