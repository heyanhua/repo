
// 0ProcessInfoDlg.h : 头文件
//

#pragma once
#include "afxcmn.h"


// C0ProcessInfoDlg 对话框
class C0ProcessInfoDlg : public CDialogEx
{
// 构造
public:
	C0ProcessInfoDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MY0PROCESSINFO_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult);
	// tab控件
	CTabCtrl m_tab;
	afx_msg void OnSize(UINT nType, int cx, int cy);
};
