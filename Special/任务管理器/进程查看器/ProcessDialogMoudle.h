#pragma once
#include "afxcmn.h"


// CProcessDialogMoudle 对话框

class CProcessDialogMoudle : public CDialogEx
{
	DECLARE_DYNAMIC(CProcessDialogMoudle)

public:
	CProcessDialogMoudle(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CProcessDialogMoudle();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PROCESS_DIALOG_MOUDLE };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	int       m_SeletePID;
	CListCtrl m_list;
};
