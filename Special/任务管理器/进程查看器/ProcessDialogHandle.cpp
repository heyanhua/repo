// ProcessDialogHandle.cpp : 实现文件
//

#include "stdafx.h"
#include "进程查看器.h"
#include "ProcessDialogHandle.h"
#include "afxdialogex.h"


// CProcessDialogHandle 对话框

IMPLEMENT_DYNAMIC(CProcessDialogHandle, CDialogEx)

CProcessDialogHandle::CProcessDialogHandle(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_PROCESS_DIALOG_HANDLE, pParent)
{

}

CProcessDialogHandle::~CProcessDialogHandle()
{
}

void CProcessDialogHandle::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_HANDLE, m_list);
}


BEGIN_MESSAGE_MAP(CProcessDialogHandle, CDialogEx)
END_MESSAGE_MAP()


// CProcessDialogHandle 消息处理程序


BOOL CProcessDialogHandle::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	CRect rect;
	GetClientRect(&rect);
	////初始化句柄窗口控件
	m_list.InsertColumn(0, TEXT("句柄类型"), 0, rect.Width() / 6);
	m_list.InsertColumn(1, TEXT("句柄名"), 0, rect.Width() / 3);
	m_list.InsertColumn(2, TEXT("句柄"), 0, rect.Width() / 6);
	m_list.InsertColumn(3, TEXT("句柄对象"), 0, rect.Width() / 6);
	m_list.InsertColumn(4, TEXT("句柄名类型代号"), 0, rect.Width() / 12);
	m_list.InsertColumn(5, TEXT("引用计数"), 0, rect.Width() / 12);


	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}
