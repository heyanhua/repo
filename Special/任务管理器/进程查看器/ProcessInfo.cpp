
#include "stdafx.h"
#include "ProcessInfo.h"


void test()
{
	CProcessInfo  pi;





}


void CProcessInfo::EnumProcessBYSnap(function<void(PROCESSENTRY32 const&)> const& pfun)
{

	PROCESSENTRY32 item = { sizeof(PROCESSENTRY32) };
	//1 创建一个快照
	HANDLE hSnap =  CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	//2 找到第一个进程
	if (Process32First(hSnap, &item) == 0)
	{
		CloseHandle(hSnap);
		return ;
	}
	//3 循环遍历进程
	do
	{
		//执行操作
		if(pfun !=nullptr)
			pfun(item);
		
		//写数据到容器
		auto index = m_Data.size();
		m_Data[index].th32ProcessID = item.th32ProcessID;
		m_Data[index].th32ParentProcessID = item.th32ParentProcessID;
		m_Data[index].szExeFile = item.szExeFile;
		m_Data[index].cntThreads = item.cntThreads;
		m_Data[index].pcPriClassBase = item.pcPriClassBase;
		m_Data[index].th32ModuleID = item.th32ModuleID;
	} while (Process32Next(hSnap, &item));
	CloseHandle(hSnap);
}

void CProcessInfo::EnumMoudleBYSnap(DWORD dwPId, function<void(MODULEENTRY32 const&)> const & pfun)
{
	MODULEENTRY32 item = { sizeof(MODULEENTRY32) };
	// 1. 创建一个模块相关的快照句柄
	auto hModuleSnap = CreateToolhelp32Snapshot(
		TH32CS_SNAPMODULE,  // 指定快照的类型
		dwPId);            // 指定进程
	if (hModuleSnap == INVALID_HANDLE_VALUE) {
		CString str;
		str.Format(TEXT("创建进程模块Snap进程ID:%d"), dwPId);
		OutputDebugString(str);
	return;
	}
	// 2. 通过模块快照句柄获取第一个模块信息
	if (!Module32First(hModuleSnap, &item)) {
		CString str;
		str.Format(TEXT("获取进程模块失败,进程ID:%d"),dwPId);
		OutputDebugString(str);
		CloseHandle(hModuleSnap);
		return ;
	}
	// 3. 循环获取模块信息
	// 3.1找到当前进程PID块

	

	//找到写入位置

	do {
		//执行操作
		if (pfun != nullptr)
			pfun(item);

		//save  data
		g_ProcessInfo.m_Data[dwPId].Moudles[item.th32ModuleID]= (MODULEENTRY32W)item;
		//...
	} while (Module32Next(hModuleSnap, &item));



	// 4. 关闭句柄并退出函数
	CloseHandle(hModuleSnap);
	return ;
}


void CProcessInfo::EnumThreadBYSnap()
{

}


CProcessInfo   g_ProcessInfo;