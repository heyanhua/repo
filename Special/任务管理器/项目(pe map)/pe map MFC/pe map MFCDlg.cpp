
// pe map MFCDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "pe map MFC.h"
#include "pe map MFCDlg.h"
#include "afxdialogex.h"
#include "DialogDaleyTable.h"

#include <functional>
using std::function;



#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();
	//CAboutDlg(PEMAP & obj);
// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}



void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CpemapMFCDlg 对话框



CpemapMFCDlg::CpemapMFCDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_PEMAPMFC_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CpemapMFCDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_REPORT, m_list);
	DDX_Control(pDX, IDC_TREE_PREVIEW, m_tree);
}

BEGIN_MESSAGE_MAP(CpemapMFCDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_SIZE()
	ON_WM_DROPFILES()
	ON_WM_GETMINMAXINFO()
	ON_NOTIFY(TVN_SELCHANGED, IDC_TREE_PREVIEW, &CpemapMFCDlg::OnSelchangedTreePreview)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_REPORT, &CpemapMFCDlg::OnLvnItemchangedListReport)
END_MESSAGE_MAP()


// CpemapMFCDlg 消息处理程序

BOOL CpemapMFCDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标




	SetWindowText(TEXT("PE map：拖拽文件以进行解析!"));

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CpemapMFCDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CpemapMFCDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CpemapMFCDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CpemapMFCDlg::Clean()
{
	m_hDosRoot = nullptr;
	m_hNtHeaderRoot = nullptr;
	m_hNtFileHeaderRoot = nullptr;
	m_hNtOptionalHeaderRoot = nullptr;
	decltype(m_hSectionHeaderRoot)  temp_v;
	m_list.DeleteAllItems();
	m_tree.DeleteAllItems();
	m_hSectionHeaderRoot.swap(temp_v);
}

void CpemapMFCDlg::HeandleExportTable()
{

	m_list.DeleteAllItems();

	CString		str;
	LVCOLUMN  lv;
	lv.pszText = TEXT("Ordinal");
	m_list.SetColumn(0,&lv);
	lv.pszText = TEXT("Rva");
	m_list.SetColumn(1, &lv);
	lv.pszText = TEXT("Offset");
	m_list.SetColumn(2, &lv);
	lv.pszText = TEXT("inOrdinal");
	m_list.SetColumn(3, &lv);
	lv.pszText = TEXT("Name");
	m_list.SetColumn(4, &lv);
	lv.pszText = TEXT("0");
	m_list.SetColumn(5, &lv);



	//找到导出表
	auto ExportTable = m_pe.is64() ? m_pe.pNt_Headers64->
		OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT]
		: m_pe.pNt_Headers32->
		OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];

	ExportTable.VirtualAddress = m_pe.Rva2Offset(ExportTable.VirtualAddress);

	//导出表的地址指向导出表的结构体, 
	//结构体中(6个字段)直接指向数组.
	auto   pTable = (PIMAGE_EXPORT_DIRECTORY) (m_pe.m_buffer + ExportTable.VirtualAddress  );
	
	//遍历方法:
	//1.遍历地址
	//
	auto address = m_pe.m_buffer + m_pe.Rva2Offset(pTable->AddressOfFunctions);
	int * p = (int *)address;
	for (size_t i = 0; i < pTable->NumberOfFunctions ; i++)
	{
		//导出序号
		str.Format(TEXT("%x"), i + pTable->Base);
		m_list.InsertItem(i,str);
		//函数地址
		str.Format(TEXT("%x"), p[i]);
		m_list.SetItemText(i,1, str);

		//文件偏移
		str.Format(TEXT("%x"), m_pe.Rva2Offset(p[i]));
		m_list.SetItemText(i,2, str);
	}
	
	//2. 遍历函数名以及序号

	 auto OrdinalsAddress = (short *)( m_pe.m_buffer + m_pe.Rva2Offset(pTable->AddressOfNameOrdinals));
	 auto NamesAddress    = (int *)(m_pe.m_buffer + m_pe.Rva2Offset(pTable->AddressOfNames));
	for (size_t i = 0; i < pTable->NumberOfNames; i++)
	{
		//dll内序号
		str.Format(TEXT("%x"), OrdinalsAddress[i]);
		m_list.SetItemText(OrdinalsAddress[i],3, str);
		TCHAR  WideStr[256]{};
		CHAR * p = (CHAR*)(m_pe.m_buffer + m_pe.Rva2Offset(NamesAddress[i]));
		MultiByteToWideChar(CP_ACP
			, NULL,
			 p,
			strnlen_s(p, 256),
			WideStr,
			256);
		//函数名
		str.Format(TEXT("%s"), WideStr);
		m_list.SetItemText(OrdinalsAddress[i],4, str);
	}

	

	
}

void CpemapMFCDlg::HeandleImportTable()
{
	CString		str;
	m_list.DeleteAllItems();
	LVCOLUMN  lv;
	lv.pszText = TEXT("DllName");
	m_list.SetColumn(0, &lv);
	lv.pszText = TEXT("ThunkOffset");
	m_list.SetColumn(1, &lv);
	lv.pszText = TEXT("ThunkValue");
	m_list.SetColumn(2, &lv);
	lv.pszText = TEXT("Hint");
	m_list.SetColumn(3, &lv);
	lv.pszText = TEXT("ApiName");
	m_list.SetColumn(4, &lv);

	//输出
	auto pImportDis  = m_pe.is64() ?
		m_pe.pNt_Headers64->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT] 
		: m_pe.pNt_Headers32->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT];
	
	auto pImport = (PIMAGE_IMPORT_DESCRIPTOR)(m_pe.Rva2Offset( pImportDis.VirtualAddress) + m_pe.m_buffer);
	WCHAR  Dllbuffer[256]{};
	auto index = 0;
	while (pImport->Name)
	{

		 MultiByteToWideChar(CP_ACP,NULL,
			 (LPCCH)(m_pe.Rva2Offset(pImport->Name) + m_pe.m_buffer),
			  strnlen_s((LPCCH)(m_pe.Rva2Offset(pImport->Name) + m_pe.m_buffer),256),
			 Dllbuffer,
			  256);

		 
		 auto   p64 =(PIMAGE_THUNK_DATA64)(m_pe.Rva2Offset(pImport->OriginalFirstThunk) + m_pe.m_buffer);
		 auto   p32 = (PIMAGE_THUNK_DATA32)(m_pe.Rva2Offset(pImport->OriginalFirstThunk) + m_pe.m_buffer);

		 while (true)
		 {
			 ULONG64 temp = 0;
			 ULONG64 temp_value = 0;
			 if (m_pe.is64())
			 {
				 temp = p64->u1.AddressOfData;
				 temp_value = (ULONG64)p64;
			 }
			 else
			 {
				 temp = p32->u1.AddressOfData;
				 temp_value = (ULONG32)p32;
			 }
			 if (temp == 0)
				 break;

			 auto p = (PIMAGE_IMPORT_BY_NAME)(m_pe.Rva2Offset(temp) + m_pe.m_buffer) ;



			 auto szName = p->Name;
			 WCHAR APIName[256]{};
			 MultiByteToWideChar(CP_ACP, NULL,
				 (LPCCH)szName,
				 strnlen_s((LPCCH)szName, 256),
				 APIName,
				 256);

			 CString	str;
			 m_list.InsertItem(index, Dllbuffer);  //0  Dll
			 str.Format(TEXT("%x"), (unsigned char *)temp_value - m_pe.m_buffer); 
			 m_list.SetItemText(index, 1, str);
			 str.Format(TEXT("%x"), temp); //offset
			 m_list.SetItemText(index, 2, str);
			 str.Format(TEXT("%x"), p->Hint);       
			 m_list.SetItemText(index, 3, str);     //Hint
			 m_list.SetItemText(index, 4, APIName); //Name

			 index++;
			 p32++;
			 p64++;
		 }
		 pImport++;
	}

}

void CpemapMFCDlg::HeandleResource()
{
	//0. 插入根节点
	HTREEITEM hRoot = nullptr;
	if (m_pe.is64())
		hRoot = (HTREEITEM)m_pe.Nt_Headers_Optional_Header64Contents[m_pe.Nt_Headers_Optional_Header64Contents.size()-1].ExData[0];
	else
		hRoot = (HTREEITEM)m_pe.Nt_Headers_Optional_Header32Contents[m_pe.Nt_Headers_Optional_Header32Contents.size() - 1].ExData[0];
	hRoot = m_tree.GetChildItem(hRoot);
	hRoot = m_tree.GetNextItem(hRoot, TVGN_NEXT);
	hRoot = m_tree.GetNextItem(hRoot, TVGN_NEXT);
	hRoot = m_tree.InsertItem(TEXT("Resources"),1,1, hRoot, TVI_LAST);

	
	for (auto &item : m_pe.m_vecRes)
	{
	auto _Root =	m_tree.InsertItem(item.strResName, 1, 1, hRoot, TVI_LAST);
	auto index = 0;
		for (auto &it : item.vec)
		{

			CString  str;
			str.Format(TEXT("%x"),index);
			auto __Root = m_tree.InsertItem(str, 1, 1, _Root, TVI_LAST);
			str.Format(TEXT("Size:%x"),it.dwSize);
			m_tree.InsertItem(str, 1, 1, __Root, TVI_LAST);
			str.Format(TEXT("Rva:%x"),it.dwResRVA);
			m_tree.InsertItem(str, 1, 1, __Root, TVI_LAST);
			str.Format(TEXT("ResFO:%x"), it.dwResFO);
			m_tree.InsertItem(str, 1, 1, __Root, TVI_LAST);
			index++;
		}
	}

}

void CpemapMFCDlg::HeandleRelocation()
{
	CDialogDaleyTable  Dlg(m_pe);
	Dlg.DoModal();
}

void CpemapMFCDlg::HandleTLS()
{
	auto&  tls = m_pe.is64() ? m_pe.pNt_Headers64->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_TLS]
		: m_pe.pNt_Headers32->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_TLS];
	auto pTlsDir = (PIMAGE_TLS_DIRECTORY)(m_pe.Rva2Offset(tls.VirtualAddress) + m_pe.m_buffer);
	
	//0. 插入根节点
	HTREEITEM hRoot = nullptr;
	if (m_pe.is64())
		hRoot = (HTREEITEM)m_pe.Nt_Headers_Optional_Header64Contents[m_pe.Nt_Headers_Optional_Header64Contents.size() - 1].ExData[0];
	else //找到目录表的根节点
		hRoot = (HTREEITEM)m_pe.Nt_Headers_Optional_Header32Contents[m_pe.Nt_Headers_Optional_Header32Contents.size() - 1].ExData[0];

	hRoot = m_tree.GetChildItem(hRoot);
	for (int i = 0; i < IMAGE_DIRECTORY_ENTRY_TLS;i++)
		hRoot = m_tree.GetNextItem(hRoot, TVGN_NEXT);//找到目录表的Resource节点
	
	//插入TLS节点
	if (tls.Size == 0)
	{
		hRoot = m_tree.InsertItem(TEXT("Non TLS"), 1, 1, hRoot, TVI_LAST);
		return;
	}   hRoot = m_tree.InsertItem(TEXT("TLS Info"), 1, 1, hRoot, TVI_LAST);

	//有tls节点
	CString  str;
	str.Format(TEXT("StartAddressOfRawData:%xRva:%x"),pTlsDir->StartAddressOfRawData,\
		m_pe.Rva2Offset(pTlsDir->StartAddressOfRawData));
	m_tree.InsertItem(str, 1, 1, hRoot, TVI_LAST);

	str.Format(TEXT("EndAddressOfRawData:%xRva:%x"), pTlsDir->EndAddressOfRawData,\
		m_pe.Rva2Offset(pTlsDir->EndAddressOfRawData));

	m_tree.InsertItem(str, 1, 1, hRoot, TVI_LAST);
	str.Format(TEXT("AddressOfCallBacks:%xRva:%x"), pTlsDir->AddressOfCallBacks, \
		m_pe.Rva2Offset(pTlsDir->AddressOfCallBacks));


		
}

void CpemapMFCDlg::HandleIAT()
{

}

void CpemapMFCDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: 在此处添加消息处理程序代码
	CRect rect;
	GetClientRect(&rect);
	auto old_right = rect.right;

		rect.right = rect.right / 2;

		m_tree.MoveWindow(&rect);
		rect.left = old_right / 2;
		rect.right = old_right;
		m_list.MoveWindow(&rect);
}


void CpemapMFCDlg::OnDropFiles(HDROP hDropInfo)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	//


	// TODO: 在此添加额外的初始化代码
	static bool  cond = true;
	if (cond)
	{
		cond = false;
		CRect		rect{};

		m_list.DeleteAllItems();
		m_list.GetClientRect(&rect);
		m_list.InsertColumn(0, TEXT("Ordinal"), 0, rect.right / 10 * 1.5);
		m_list.InsertColumn(1, TEXT("Rva"), 0, rect.right / 5);
		m_list.InsertColumn(2, TEXT("Offset"), 0, rect.right / 5);
		m_list.InsertColumn(3, TEXT("inOrdinal"), 0, rect.right / 10 * 1.5);
		m_list.InsertColumn(4, TEXT("Name"), 0, rect.right / 10 * 3);
	}




	[&]()
	{
		CDialogEx::OnDropFiles(hDropInfo);
		CString  strFileName('\0', 256);
		DragQueryFile(hDropInfo, 0, strFileName.GetBuffer(), 256);
		DragFinish(hDropInfo);

		CString  str('\0', 256);

		auto myInsertFunc = [&](vector<FieldInfo>& v, HTREEITEM&const \
			hroot) {
			for (auto & item : v)
			{
				//处理 .data  .txt .rdata 等字符串
				if (false == item.isStr)
				{
					//处理正常数据
					if (item.Roffset != 0)
					{
						size_t number = 0;
						switch (item.size)
						{
						case 1:
							number = *((PBYTE)item.Roffset);
							break;
						case 2:
							number = *((PWORD)item.Roffset);
							break;
						case 4:
							number = *((PDWORD)item.Roffset);
							break;
						case 8:
							number = *((ULONG64*)item.Roffset);
							break;
						default:
							number = '-';
						}

						if (number != '-')
							str.Format(TEXT("%s	  %x	[size:%x]Roffset:	%x"), item.Name,
								number,
								item.size, \
								item.Roffset - (size_t)m_pe.m_buffer);
						else
							str.Format(TEXT("%s	  %c	[size:%x]Roffset:	%x"), item.Name,
								number,
								item.size, \
								item.Roffset - (size_t)m_pe.m_buffer);
					}
					else //处理节表的根节点
					{
						str.Format(TEXT("%s"), item.Name);
					}
				}
				else
				{
					TCHAR _buffer[256]{};
					MultiByteToWideChar(
						CP_ACP,
						NULL,
						(LPCCH)item.Roffset,
						strnlen_s((LPCCH)item.Roffset, 256),
						_buffer,
						256
						);
					str.Format(TEXT("%s	  %s	[size:%x]Roffset:	%x"), item.Name,
						_buffer,
						item.size, \
						item.Roffset - (size_t)m_pe.m_buffer);
				}
				item.ExData[0] = (int)m_tree.InsertItem(str, 1, 1, hroot, TVI_LAST);
			}
		};



		//处理拖拽事件
		// 1.传递文件路径

		//不是PE文件
		auto _ret = m_pe.DropFile(strFileName.GetBuffer());
		if (NOT_PE_FILE == _ret )
		{
			MessageBox(TEXT("不是PE文件!"));
			return;
		}
		if (INVALID_FILE_SIZE == _ret)
		{
			MessageBox(TEXT("打开文件失败!"));
			return;
		}
		str.Format(TEXT("[%s]:%s"), strFileName, m_pe.is64() ? TEXT("pe64") : TEXT("pe32"));
		SetWindowText(str);
		Clean(); //清理数据




		//插入Dos头节点
		m_hDosRoot = m_tree.InsertItem(TEXT("Dos Header"));
		myInsertFunc(m_pe.Dos_HeaderContents, m_hDosRoot);

		//插入Nt头节点
		m_hNtHeaderRoot = m_tree.InsertItem(TEXT("Nt Headers"));
		decltype(m_pe.Nt_Headers32Contents) temp_v;
		temp_v.emplace_back(m_pe.Nt_Headers32Contents[0]);
		myInsertFunc(temp_v, m_hNtHeaderRoot);

		str.Format(TEXT("FileHeader"));
		//插入File头节点
		m_hNtFileHeaderRoot = m_tree.InsertItem(str, 1, 1, m_hNtHeaderRoot, TVI_LAST);
		myInsertFunc(m_pe.Nt_Headers_File_HeaderContents, m_hNtFileHeaderRoot);


		str.Format(TEXT("OptionalHeader"));
		//插入扩展头节点
		m_hNtOptionalHeaderRoot = m_tree.InsertItem(str, 1, 1, m_hNtHeaderRoot, TVI_LAST);
		myInsertFunc(m_pe.is64() ? m_pe.Nt_Headers_Optional_Header64Contents :
			m_pe.Nt_Headers_Optional_Header32Contents, m_hNtOptionalHeaderRoot);


		auto myFullInfo = [](vector<FieldInfo>& v, unsigned char * first_p)
		{
			for (auto &item : v)
			{
				item.Roffset = (size_t)first_p;
				first_p += item.size;
			}
		};



		//补充DataDrictory信息
		auto hRoot = (HTREEITEM)(m_pe.is64()
			? m_pe.Nt_Headers_Optional_Header64Contents[m_pe.Nt_Headers_Optional_Header64Contents.size() - 1].ExData[0]
			: m_pe.Nt_Headers_Optional_Header32Contents[m_pe.Nt_Headers_Optional_Header32Contents.size() - 1].ExData[0]);


		myInsertFunc(m_pe.Data_DrictoryTable, hRoot);
		auto _temp = 0;
		for (auto &item : m_pe.Data_DrictoryTable)
		{


			//填充字段
			myFullInfo(m_pe.Data_Drictory, (unsigned char *)
				(m_pe.is64()
					? &m_pe.pNt_Headers64->OptionalHeader.DataDirectory[_temp]
					: &m_pe.pNt_Headers32->OptionalHeader.DataDirectory[_temp])
				);

			HTREEITEM hRoot = (HTREEITEM)item.ExData[0];
			myInsertFunc(m_pe.Data_Drictory, hRoot);
			_temp++;
		}



		//循环插入节表头信息
		auto  pSH = m_pe.pSection_Header;
		for (size_t i = 0; i < m_pe.pNt_Headers32->FileHeader.NumberOfSections; i++)
		{


			myFullInfo(m_pe.Section_HeaderContents, (unsigned char *)(pSH));
			str.Format(TEXT("%s"), &pSH->Name);
			TCHAR _buffer[256]{};
			MultiByteToWideChar(
				CP_ACP,
				NULL,
				(LPCCH)pSH->Name,
				strnlen_s((LPCCH)pSH->Name, 256),
				_buffer,
				256
				);
			m_hSectionHeaderRoot.push_back(m_tree.InsertItem(_buffer));
			myInsertFunc(m_pe.Section_HeaderContents,
				m_hSectionHeaderRoot[i]);
			pSH++;
		}


		//处理资源表
		HeandleResource();
		HandleTLS();

	}();

}


void CpemapMFCDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}


void CpemapMFCDlg::OnSelchangedTreePreview(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	

	auto Select =  0;
	//处理选中--只处理Data_Drictory子项的选中
	for (auto &item : m_pe.Data_DrictoryTable)
	{
		//比对句柄
		if (pNMTreeView->itemNew.hItem == (HTREEITEM)item.ExData[0])
			break;
			Select++;
	}



	switch (Select)
	{
	case IMAGE_DIRECTORY_ENTRY_EXPORT:
		HeandleExportTable();
		break;
	case IMAGE_DIRECTORY_ENTRY_IMPORT:
		HeandleImportTable();
		break;
	case IMAGE_DIRECTORY_ENTRY_RESOURCE:
		break;
	case IMAGE_DIRECTORY_ENTRY_EXCEPTION:
		break;
	case IMAGE_DIRECTORY_ENTRY_SECURITY:
		break;
	case IMAGE_DIRECTORY_ENTRY_BASERELOC:
		HeandleRelocation();
		break;
	case IMAGE_DIRECTORY_ENTRY_DEBUG:
		break;
	case IMAGE_DIRECTORY_ENTRY_ARCHITECTURE:
		break;
	case IMAGE_DIRECTORY_ENTRY_GLOBALPTR:
		break;
	case IMAGE_DIRECTORY_ENTRY_TLS:
		break;
	case IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG:
		break;
	case IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT:
		break;
	case IMAGE_DIRECTORY_ENTRY_IAT:
		HeandleImportTable();
		break;
	case IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT:
		break;
	case IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR:
		break;
	default:
		break;
	}


	*pResult = 0;
}


void CpemapMFCDlg::OnLvnItemchangedListReport(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}
