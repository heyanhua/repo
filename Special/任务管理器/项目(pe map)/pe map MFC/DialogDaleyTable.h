#pragma once
#include "afxcmn.h"
#include "PEMAP.h"

// CDialogDaleyTable 对话框

class CDialogDaleyTable : public CDialogEx
{
	DECLARE_DYNAMIC(CDialogDaleyTable)

public:
//	CDialogDaleyTable(CWnd* pParent = NULL);   // 标准构造函数
	CDialogDaleyTable(PEMAP& obj,CWnd* pParent = NULL);
	virtual ~CDialogDaleyTable();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_DALEY_TABLE };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	PEMAP & m_pe;
	CListCtrl m_list1;
	CListCtrl m_list2;
	virtual BOOL OnInitDialog();
	afx_msg void OnClickList0(NMHDR *pNMHDR, LRESULT *pResult);
};
