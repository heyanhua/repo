#pragma once

#include <windows.h>
#include <vector>
using std::vector;
#include <map>
using std::pair;



//区段信息
typedef struct _MYPESECTION
{
	DWORD dwRVA;//相对虚拟地址
	DWORD dwVirtualSize;//内存大小
	DWORD dwFO;//区段在文件中的偏移
	DWORD dwFileSize;//区段在文件中的大小
	DWORD dwAttribute;//区段加载属性
	CString   strName;
}MYPESECTION;


//资源数据信息
typedef struct _RESOURCE_DATA_INFO
{

	CString strID;			//资源ID
	DWORD dwResRVA;			// 资源RVA
	DWORD dwResFO;			// 偏移
	DWORD dwSize;			// 资源大小

}RESOURCE_DATA_INFO, *PRESOURCE_DATA_INFO;

//资源种类信息
typedef struct _RESOURCE_INFO
{
	DWORD m_dwIDNum;//ID资源数目
	DWORD m_dwNameNum;//名称资源数目
	CString strResName;				//资源种类名称
	BOOL bOneLayer = FALSE;			//是否只有1层目录
	DWORD dwIDNum;					// ID资源数
	DWORD dwNameNum;				// 名称资源数
	vector<RESOURCE_DATA_INFO> vec;
}RESOURCE_INFO, *PRESOURCE_INFO;

typedef struct _TYPEOFFSET
{
	WORD offset : 12;//偏移量
	WORD Type : 4;//重定位属性(方式)
}TYPEOFFSET, *PTYPEOFFSET;

//重定位数据信息
typedef struct _RELOC_DATA
{
	DWORD dwDataRVA;		// 重定位数据RVA
	DWORD dwDataFO;			// 重定位数据偏移
	DWORD dwRelocValue;		// 重定位数据中的值
	BYTE bType;				// 重定位方式
	BYTE bData[10];			// dwRelocValue中的值
}RELOC_DATA, *PRELOC_DATA;
//重定位区域信息
typedef struct _RELOC_SECTION
{

	CString strSecName;		// 重定位区段名字
	DWORD dwSecRVA;			// 重定位区段RVA
	DWORD dwNumOfReloc;		// 重定位个数
	vector<RELOC_DATA> vec;	// 重定位数据信息
}RELOC_SECTION, *PRELOC_SECTION;


















typedef struct _FieldInfo
{
	wchar_t *const	Name; 
	size_t	 const	size;
	bool	 const	isStr;
	size_t			Roffset; //数据地址
	size_t			ExData[4];//扩展数据,自行定制使用
							  //ExData[0],在MFC中被用作了m_tree的节点句柄

} FieldInfo, *pFieldInfo;


#define		NOT_PE_FILE		 INVALID_FILE_SIZE-1

class PEMAP
{
public:
	PEMAP();
	~PEMAP();
public:
	BOOL	DropFile(TCHAR* pFileName);
	bool	Init(); //初始化数据
	bool    Clean(); //清理数据
	bool	is64();
	size_t	Rva2Offset(size_t Rva);


	// 获取资源表信息 ------------
	bool GetResourceInfo();
	// 获取重定位表信息  ---------------
	bool GetRelocInfo();
	// 获取区段信息
	void GetSectionInfo();
	// 区段信息
	vector<MYPESECTION> m_vecSec;
	// 资源表信息
	vector<RESOURCE_INFO> m_vecRes;
	// 重定位表信息
	vector<RELOC_SECTION> m_vecReloc;


	//四个pe头信息
	unsigned char *			m_buffer = nullptr;
	size_t					m_buffer_size = 0;
	PIMAGE_DOS_HEADER		pDos_Header = nullptr;
	PIMAGE_NT_HEADERS32		pNt_Headers32 = nullptr;
	PIMAGE_NT_HEADERS64		pNt_Headers64 = nullptr;
	PIMAGE_SECTION_HEADER	pSection_Header = nullptr;
	PIMAGE_DATA_DIRECTORY	pData_Dirictory = nullptr;

	vector< FieldInfo>		Dos_HeaderContents
	{
		{L"e_magic:" , sizeof(WORD),false,0},
		{L"e_cblp:" , sizeof(WORD),false,0},
		{L"e_cp:" , sizeof(WORD),false,0},
		{L"e_crlc:" , sizeof(WORD),false,0},
		{L"e_cparhdr:" , sizeof(WORD),false,0},
		{L"e_minalloc:" , sizeof(WORD),false,0},
		{L"e_maxalloc:" , sizeof(WORD),false,0},
		{L"e_ss:" , sizeof(WORD),false,0},
		{L"e_sp:" , sizeof(WORD),false,0},
		{L"e_csum:" , sizeof(WORD),false,0},
		{L"e_ip:" , sizeof(WORD),false,0},
		{L"e_cs:" , sizeof(WORD),false,0},
		{L"e_lfarlc:" , sizeof(WORD),false,0},
		{L"e_ovno:" , sizeof(WORD),false,0},
		{L"e_res[4]:" , sizeof(WORD) * 4,false,0},
		{L"e_oemid:" , sizeof(WORD),false,0},
		{L"e_oeminfo:" , sizeof(WORD),false,0},
		{L"e_res2[10]:" , sizeof(WORD) * 10,false,0},
		{L"e_lfanew:" , sizeof(LONG),false,0}
	};
	vector< FieldInfo>		Nt_Headers32Contents
	{
		{L"Signature:", sizeof(DWORD),false,0},
		{L"FileHeader:", sizeof(IMAGE_FILE_HEADER),false,0},
		{L"OptionalHeader:",sizeof(IMAGE_OPTIONAL_HEADER32),false,0}
	};

	vector< FieldInfo>		Nt_Headers64Contents
	{
		{ L"Signature:", sizeof(DWORD),false,0 },
		{ L"FileHeader:", sizeof(IMAGE_FILE_HEADER),false,0 },
		{ L"OptionalHeader:",sizeof(IMAGE_OPTIONAL_HEADER64),false,0 }
	};

	vector< FieldInfo>		Nt_Headers_File_HeaderContents
	{
		{L"Machine:" , sizeof(WORD),false,0},
		{ L"NumberOfSections:" , sizeof(WORD),false,0},
		{ L"TimeDateStamp:" , sizeof(DWORD),false,0},
		{ L"PointerToSymbolTable:" , sizeof(DWORD),false,0},
		{ L"NumberOfSymbols:" , sizeof(DWORD),false,0},
		{ L"SizeOfOptionalHeader:" , sizeof(WORD),false,0},
		{ L"Characteristics:" , sizeof(WORD),false,0}
	};


	vector< FieldInfo>		Nt_Headers_Optional_Header32Contents
	{
	   {L"Magic:", sizeof(WORD),false,0},
	   {L"MajorLinkerVersion:", sizeof(BYTE),false,0 },
	   {L"MinorLinkerVersion:", sizeof(BYTE),false,0 },
	   {L"SizeOfCode:", sizeof(DWORD),false,0 },
	   {L"SizeOfInitializedData:", sizeof(DWORD),false,0 },
	   {L"SizeOfUninitializedData:", sizeof(DWORD),false,0 },
	   {L"AddressOfEntryPoint:", sizeof(DWORD),false,0 },
	   {L"BaseOfCode:", sizeof(DWORD),false,0 },
	   {L"BaseOfData:", sizeof(DWORD),false,0 },
	   {L"ImageBase:", sizeof(DWORD),false,0 },
	   {L"SectionAlignment:", sizeof(DWORD),false,0 },
	   {L"FileAlignment:", sizeof(DWORD),false,0 },
	   {L"MajorOperatingSystemVersion:", sizeof(WORD),false,0 },
	   {L"MinorOperatingSystemVersion:", sizeof(WORD),false,0 },
	   {L"MajorImageVersion:", sizeof(WORD),false,0 },
	   {L"MinorImageVersion:", sizeof(WORD),false,0 },
	   {L"MajorSubsystemVersion:", sizeof(WORD),false,0 },
	   {L"MinorSubsystemVersion:", sizeof(WORD),false,0 },
	   {L"Win32VersionValue:", sizeof(DWORD),false,0 },
	   {L"SizeOfImage:", sizeof(DWORD),false,0 },
	   {L"SizeOfHeaders:", sizeof(DWORD),false,0 },
	   {L"CheckSum:", sizeof(DWORD),false,0 },
	   {L"Subsystem:", sizeof(WORD),false,0 },
	   {L"DllCharacteristics:", sizeof(WORD),false,0 },
	   {L"SizeOfStackReserve:", sizeof(DWORD),false,0 },
	   {L"SizeOfStackCommit:", sizeof(DWORD),false,0 },
	   {L"SizeOfHeapReserve:", sizeof(DWORD),false,0 },
	   {L"SizeOfHeapCommit:", sizeof(DWORD),false,0 },
	   {L"LoaderFlags:", sizeof(DWORD),false,0 },
	   {L"NumberOfRvaAndSizes:", sizeof(DWORD),false,0 },
	   {L"DataDirectory[0x10]:", sizeof(IMAGE_DATA_DIRECTORY) * 0x10,false,0 }
	};

	vector< FieldInfo>		Nt_Headers_Optional_Header64Contents
	{
		{ L"Magic:", sizeof(WORD),false,0 },
		{ L"MajorLinkerVersion:", sizeof(BYTE),false,0 },
		{ L"MinorLinkerVersion:", sizeof(BYTE),false,0 },
		{ L"SizeOfCode:", sizeof(DWORD),false,0 },
		{ L"SizeOfInitializedData:", sizeof(DWORD),false,0 },
		{ L"SizeOfUninitializedData:", sizeof(DWORD),false,0 },
		{ L"AddressOfEntryPoint:", sizeof(DWORD),false,0 },
		{ L"BaseOfCode:", sizeof(DWORD),false,0 },
		{ L"ImageBase:", sizeof(ULONGLONG),false,0 },
		{ L"SectionAlignment:", sizeof(DWORD),false,0 },
		{ L"FileAlignment:", sizeof(DWORD),false,0 },
		{ L"MajorOperatingSystemVersion:", sizeof(WORD),false,0 },
		{ L"MinorOperatingSystemVersion:", sizeof(WORD),false,0 },
		{ L"MajorImageVersion:", sizeof(WORD),false,0 },
		{ L"MinorImageVersion:", sizeof(WORD),false,0 },
		{ L"MajorSubsystemVersion:", sizeof(WORD),false,0 },
		{ L"MinorSubsystemVersion:", sizeof(WORD),false,0 },
		{ L"Win32VersionValue:", sizeof(DWORD),false,0 },
		{ L"SizeOfImage:", sizeof(DWORD),false,0 },
		{ L"SizeOfHeaders:", sizeof(DWORD),false,0 },
		{ L"CheckSum:", sizeof(DWORD),false,0 },
		{ L"Subsystem:", sizeof(WORD),false,0 },
		{ L"DllCharacteristics:", sizeof(WORD),false,0 },
		{ L"SizeOfStackReserve:", sizeof(ULONGLONG),false,0 },
		{ L"SizeOfStackCommit:", sizeof(ULONGLONG),false,0 },
		{ L"SizeOfHeapReserve:", sizeof(ULONGLONG),false,0 },
		{ L"SizeOfHeapCommit:", sizeof(ULONGLONG),false,0 },
		{ L"LoaderFlags:", sizeof(DWORD),false,0 },
		{ L"NumberOfRvaAndSizes:", sizeof(DWORD),false,0 },
		{ L"DataDirectory[0x10]:", sizeof(IMAGE_DATA_DIRECTORY) * 0x10,false,0 }

	};


	vector< FieldInfo>		Section_HeaderContents
	{
		{L"Name[0x8]:", sizeof(BYTE) * 0x8,true,0},
		{L"VirtualSize|| PhysicalAddress:", sizeof(DWORD),false,0 },
		{L"VirtualAddress:", sizeof(DWORD),false,0 },
		{L"SizeOfRawData:", sizeof(DWORD),false,0 },
		{L"PointerToRawData:", sizeof(DWORD),false,0 },
		{L"PointerToRelocations:", sizeof(DWORD),false,0 },
		{L"PointerToLinenumbers:", sizeof(DWORD),false,0 },
		{L"NumberOfRelocations:", sizeof(WORD),false,0 },
		{L"NumberOfLinenumbers:", sizeof(WORD),false,0 },
		{L"Characteristics:", sizeof(DWORD),false,0 }
	};


	vector <FieldInfo>		Data_Drictory
	{
		{L"VirtualAddress:", sizeof(DWORD), false, 0 },
		{ L"Size:", sizeof(DWORD),false,0 }
	};

	//单独处理
	// // Directory Entries
	vector <FieldInfo>		Data_DrictoryTable
	{
		{L"Export Directory:",0,false,0 },
		{L"Import Directory:",0,false,0 },
		{L"Resource Directory:",0,false,0 },
		{L"Exception Directory:" ,0,false,0 },
		{L"Security Directory:",0,false,0 },
		{L"Base Relocation Table: " ,0,false,0 },
		{L"Debug Directory: " ,0,false,0 },
		{L"Architecture Specific Data	: " ,0,false,0 },
		{L"RVA of GP: " ,0,false,0 },
		{L"TLS Directory: " ,0,false,0 },
		{L"Load Configuration Directory: ",0,false,0 },
		{L"Bound Import Directory in headers: " ,0,false,0 },
		{L"Import Address Table: " ,0,false,0 },
		{L"Delay Load Import Descriptors: " ,0,false,0 },
		{L"COM Runtime descriptor: " ,0,false,0 }
	};		 




	


};