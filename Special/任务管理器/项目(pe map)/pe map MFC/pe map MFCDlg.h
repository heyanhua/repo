
// pe map MFCDlg.h : 头文件
//

#pragma once
#include "afxcmn.h"
#include "afxmaskededit.h"
#include "PEMAP.h"


// CpemapMFCDlg 对话框
class CpemapMFCDlg : public CDialogEx
{
// 构造
public:
	CpemapMFCDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PEMAPMFC_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	PEMAP					m_pe;
	CListCtrl				m_list;
	CTreeCtrl				m_tree;
	HTREEITEM				m_hDosRoot;
	HTREEITEM				m_hNtHeaderRoot;
	HTREEITEM				m_hNtFileHeaderRoot;
	HTREEITEM				m_hNtOptionalHeaderRoot;
	vector<HTREEITEM>		m_hSectionHeaderRoot;
			
	void Clean();//清理数据
	void HeandleExportTable();
	void HeandleImportTable();
	void HeandleResource();
	void HeandleRelocation();
	void HandleTLS();
	void HandleIAT();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSelchangedTreePreview(NMHDR *pNMHDR, LRESULT *pResult);


	afx_msg void OnLvnItemchangedListReport(NMHDR *pNMHDR, LRESULT *pResult);
};
