// DialogDaleyTable.cpp : 实现文件
//

#include "stdafx.h"
#include "pe map MFC.h"
#include "DialogDaleyTable.h"
#include "afxdialogex.h"


// CDialogDaleyTable 对话框

IMPLEMENT_DYNAMIC(CDialogDaleyTable, CDialogEx)

//CDialogDaleyTable::CDialogDaleyTable(CWnd* pParent /*=NULL*/)
//	: 
//{
//
//}

CDialogDaleyTable::CDialogDaleyTable(PEMAP& obj, CWnd * pParent)
	: CDialogEx(IDD_DIALOG_DALEY_TABLE, pParent),m_pe(obj)
{

}

CDialogDaleyTable::~CDialogDaleyTable()
{
}

void CDialogDaleyTable::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST0, m_list1);
	DDX_Control(pDX, IDC_LIST1, m_list2);
}


BEGIN_MESSAGE_MAP(CDialogDaleyTable, CDialogEx)
	ON_NOTIFY(NM_CLICK, IDC_LIST0, &CDialogDaleyTable::OnClickList0)
END_MESSAGE_MAP()


// CDialogDaleyTable 消息处理程序


BOOL CDialogDaleyTable::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_list1.SetExtendedStyle( LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_list1.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	// TODO:  在此添加额外的初始化
	m_list1.DeleteAllItems();

	CString		str;
	CRect       rect;
	m_list1.GetClientRect(&rect);
	m_list1.InsertColumn(0, TEXT("Index"), 0, rect.right / 10 * 1);
	m_list1.InsertColumn(1, TEXT("Section"), 0, rect.right / 5);
	m_list1.InsertColumn(2, TEXT("Rva"), 0, rect.right / 5);
	m_list1.InsertColumn(3, TEXT("Count"), 0, rect.right / 10 * 1);
	m_list1.InsertColumn(4, TEXT("Size"), 0, rect.right / 10 * 3);

	m_list2.GetClientRect(&rect);
	m_list2.InsertColumn(0, TEXT("Index"), 0, rect.right / 10 * 1);
	m_list2.InsertColumn(1, TEXT("Rva"), 0, rect.right / 5);
	m_list2.InsertColumn(2, TEXT("Offset"), 0, rect.right / 5);
	m_list2.InsertColumn(3, TEXT("Type"), 0, rect.right / 10 * 1);
	m_list2.InsertColumn(4, TEXT("Data"), 0, rect.right / 10 * 3);





	auto index = 0;
	for (auto & item : m_pe.m_vecReloc)
	{
		str.Format(TEXT("%x"),index);
		m_list1.InsertItem(index,str);
		m_list1.SetItemText(index,1,item.strSecName);
		str.Format(TEXT("%x"),item.dwSecRVA);
		m_list1.SetItemText(index, 2, str);
		str.Format(TEXT("%x"), item.dwNumOfReloc);
		m_list1.SetItemText(index, 3, str);
		str.Format(TEXT("%x"), item.vec.size());
		m_list1.SetItemText(index, 4, str);
		index++;
	}


	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}


void CDialogDaleyTable::OnClickList0(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	

	if (pNMItemActivate->iItem < 0 || pNMItemActivate->iItem >= m_pe.m_vecReloc.size())
		return;

	CString str;
	auto  index = 0;
	m_list2.DeleteAllItems();
	for (auto & item : m_pe.m_vecReloc[pNMItemActivate->iItem].vec)
	{
		str.Format(TEXT("%x"), index);
		m_list2.InsertItem(index, str);
		str.Format(TEXT("%x"), item.dwDataRVA);
		m_list2.SetItemText(index, 1,str) ;
		str.Format(TEXT("%x"), item.dwDataFO);
		m_list2.SetItemText(index, 2, str);
		str.Format(TEXT("%x"), item.bType);
		m_list2.SetItemText(index, 3, str);
		str.Format(TEXT("%s"), item.bData);
		str.GetBuffer()[10] = 0;
		m_list2.SetItemText(index, 4, str);
		index++;
	}
	*pResult = 0;
}
