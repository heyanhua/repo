
#include "stdafx.h"
#include "PEMAP.h"



PEMAP::PEMAP()
{
}


PEMAP::~PEMAP()
{
	if (pDos_Header)
		delete[]pDos_Header;
}

BOOL PEMAP::DropFile(TCHAR * pFileName)
{


	Clean();
	auto hFile = CreateFile(pFileName,
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		NULL, NULL);

	auto size = GetFileSize(hFile, NULL);
	
		if ( INVALID_FILE_SIZE == size)
		{
			CloseHandle(hFile);
			return INVALID_FILE_SIZE;
		}
		m_buffer_size = size;
	m_buffer = new unsigned char[size];

	ReadFile(hFile, m_buffer, size, &size, NULL);
	CloseHandle(hFile);

	pDos_Header = (PIMAGE_DOS_HEADER)m_buffer;
	PIMAGE_NT_HEADERS  _pNT_Header = (PIMAGE_NT_HEADERS)(m_buffer + pDos_Header->e_lfanew);

	//判断是否为PE文件
	if (pDos_Header->e_magic != IMAGE_DOS_SIGNATURE
		|| _pNT_Header->Signature != IMAGE_NT_SIGNATURE)
	{
		delete[] m_buffer;
		m_buffer = nullptr;
		pDos_Header = nullptr;
		return NOT_PE_FILE;
	}


	//初始化数据
	Init();
	return true;	
}

bool PEMAP::Init()
{

		pNt_Headers32 = (PIMAGE_NT_HEADERS32)(m_buffer + pDos_Header->e_lfanew);
		pNt_Headers64 = (PIMAGE_NT_HEADERS64)(m_buffer + pDos_Header->e_lfanew);
		if(is64())
		pSection_Header = (PIMAGE_SECTION_HEADER)( pNt_Headers64+1);
		else
		pSection_Header = (PIMAGE_SECTION_HEADER)( pNt_Headers32+1);


		auto myFullInfo = [](vector<FieldInfo>& v , unsigned char * first_p)
		{
			for (auto &item : v)
			{
				item.Roffset = (size_t)first_p;
				first_p += item.size;
			}
		};

		//填充固定格式头信息
		myFullInfo(Dos_HeaderContents, (unsigned char *)pDos_Header);
		myFullInfo(Nt_Headers32Contents, (unsigned char *)pNt_Headers32);
		myFullInfo(Nt_Headers64Contents, (unsigned char *)pNt_Headers64);
		myFullInfo(Nt_Headers_File_HeaderContents, (unsigned char *)(&pNt_Headers32->FileHeader));
		myFullInfo(Nt_Headers_Optional_Header32Contents, (unsigned char *)(&pNt_Headers32->OptionalHeader));
		myFullInfo(Nt_Headers_Optional_Header64Contents, (unsigned char *)(&pNt_Headers64->OptionalHeader));
		myFullInfo(Section_HeaderContents, (unsigned char *)(&pSection_Header));


		//填充几个结构体
		GetSectionInfo();
		GetResourceInfo();
		GetRelocInfo();
		

	return true;
}

bool PEMAP::is64()
{
	PIMAGE_NT_HEADERS  _pNT_Header = (PIMAGE_NT_HEADERS)(m_buffer + pDos_Header->e_lfanew);
	return _pNT_Header->FileHeader.SizeOfOptionalHeader == sizeof(IMAGE_OPTIONAL_HEADER64);
}

size_t PEMAP::Rva2Offset(size_t Rva)
{
	
	auto    _pSection_Header = pSection_Header;
	for (int i = 0; i < pNt_Headers32->FileHeader.NumberOfSections; i++)
	{

		if (_pSection_Header->VirtualAddress <= Rva
			&& Rva <= _pSection_Header->Misc.VirtualSize
			+ _pSection_Header->VirtualAddress)
			return Rva - _pSection_Header->VirtualAddress + _pSection_Header->PointerToRawData;
		_pSection_Header++;
	}

	return 0;
}


bool PEMAP::Clean()
{
	if (m_buffer != nullptr)
		delete[]m_buffer;
	m_buffer = nullptr;
	m_buffer_size = 0;
	pDos_Header = nullptr;
	pNt_Headers32 = nullptr;
	pNt_Headers64 = nullptr;
	pSection_Header = nullptr;

	auto myClean = [](decltype(Dos_HeaderContents) & v)
	{
		for (auto &item : v)
			item.Roffset = 0;
	};

	myClean(Dos_HeaderContents);
	myClean(Nt_Headers32Contents);
	myClean(Nt_Headers64Contents);
	myClean(Nt_Headers_File_HeaderContents);
	myClean(Nt_Headers_Optional_Header32Contents);
	myClean(Nt_Headers_Optional_Header64Contents);
	myClean(Section_HeaderContents);
	


	decltype(m_vecSec)		tmp_vecSec;
	decltype(m_vecRes)		tmp_vecRec;
	decltype(m_vecReloc)	tmp_vecReloc;
	m_vecSec.swap(tmp_vecSec);
	m_vecRes.swap(tmp_vecRec);
	m_vecReloc.swap(tmp_vecReloc);
	return false;
}



void PEMAP::GetSectionInfo()
{
	auto  pSec = is64() ? (PIMAGE_SECTION_HEADER)(pNt_Headers64 + 1)
		: (PIMAGE_SECTION_HEADER)(pNt_Headers32 + 1);
	for (DWORD i = 0; i < pNt_Headers32->FileHeader.NumberOfSections; i++)//区段数目
	{
		MYPESECTION stcTemp = {};
		//区段在内存中的大小
		stcTemp.dwVirtualSize = pSec->Misc.VirtualSize;
		// 区段的相对虚拟地址
		stcTemp.dwRVA = pSec->VirtualAddress;
		// 区段的文件偏移
		stcTemp.dwFO = pSec->PointerToRawData;
		// 区段在文件中的大小
		stcTemp.dwFileSize = pSec->SizeOfRawData;
		// 区段加载时的属性
		stcTemp.dwAttribute = pSec->Characteristics;
		//区段名字
		stcTemp.strName = pSec->Name;
		m_vecSec.push_back(stcTemp);
		pSec++;
	}
}

// 获取资源表信息
bool PEMAP::GetResourceInfo()
{
	// 资源类型
	TCHAR *szResType[] = { L"",L"鼠标指针",L"位图",L"图标",L"菜单",L"对话框",L"字符串列表",L"字体目录",
		L"字体",L"快捷键",L"非格式化资源",L"消息列表",L"鼠标指针数组",L"",L"图标组",L"" ,
		L"版本信息" };
	// 获取资源表地址
	DWORD dwResRVA = is64() ? pNt_Headers64->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_RESOURCE].VirtualAddress \
		: pNt_Headers32->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_RESOURCE].VirtualAddress;

	
	
	PIMAGE_RESOURCE_DIRECTORY pResAddr = (PIMAGE_RESOURCE_DIRECTORY)((PBYTE)m_buffer + Rva2Offset(dwResRVA));
	// 资源种类数量
	// 静态，写在后面
	//m_dwIDNum = pResAddr->NumberOfIdEntries;
	//m_dwNameNum = pResAddr->NumberOfNamedEntries;

	DWORD dwResTypeNum = pResAddr->NumberOfIdEntries + pResAddr->NumberOfNamedEntries;
	// 第一层循环
	// 第1次循环开始地址
	auto pFirLoop = (PIMAGE_RESOURCE_DIRECTORY_ENTRY)((PBYTE)pResAddr + 16);
	for (DWORD i = 0; i < dwResTypeNum; i++)
	{
		RESOURCE_INFO stcResTemp = {};
		// 是否字符串资源
		if (pFirLoop[i].NameIsString)
		{
			// 名称地址
			auto pName = (PIMAGE_RESOURCE_DIR_STRING_U)((PBYTE)pResAddr + pFirLoop[i].NameOffset);
			// 把名称拷出来
			WCHAR *szName = new WCHAR[pName->Length + 1];
			ZeroMemory(szName, (pName->Length + 1) * sizeof(WCHAR));
			memcpy_s(szName, (pName->Length + 1) * sizeof(WCHAR), pName->NameString, pName->Length  * sizeof(WCHAR));
			stcResTemp.strResName = szName;
			delete szName;
		}
		else
		{
			if (pFirLoop[i].Id < 17)
			{
				stcResTemp.strResName = szResType[pFirLoop[i].Id];
			}
			// 非固定资源
			if (!stcResTemp.strResName.GetLength())
			{
				stcResTemp.strResName.Format(L"%d", pFirLoop[i].Name);
			}
		}
		// 是否目录（是否有第2层）
		if (pFirLoop[i].DataIsDirectory)
		{
			// 找2层遍历的开始目录地址
			PIMAGE_RESOURCE_DIRECTORY pSecAddr = (PIMAGE_RESOURCE_DIRECTORY)((PBYTE)pResAddr + pFirLoop[i].OffsetToDirectory);
			stcResTemp.dwIDNum = pSecAddr->NumberOfIdEntries;
			stcResTemp.dwNameNum = pSecAddr->NumberOfNamedEntries;
			// 开始遍历资源
			// 第2层循环地址
			PIMAGE_RESOURCE_DIRECTORY_ENTRY pSecLoop = (PIMAGE_RESOURCE_DIRECTORY_ENTRY)((PBYTE)pSecAddr + 16);
			// 第2圈开始
			for (DWORD j = 0; j < stcResTemp.dwIDNum + stcResTemp.dwNameNum; j++)
			{
				RESOURCE_DATA_INFO temp = {};
				// 是否字符串资源
				if (pSecLoop[j].NameIsString)
				{
					// 名称地址
					PIMAGE_RESOURCE_DIR_STRING_U pName = (PIMAGE_RESOURCE_DIR_STRING_U)((PBYTE)pResAddr + pSecLoop[j].NameOffset);
					// 把名称拷出来
					WCHAR *szName = new WCHAR[pName->Length + 1];
					ZeroMemory(szName, (pName->Length + 1) * sizeof(WCHAR));
					memcpy_s(szName, (pName->Length + 1) * sizeof(WCHAR), pName->NameString, pName->Length  * sizeof(WCHAR));
					temp.strID = szName;
					delete szName;
				}
				else
				{
					temp.strID.Format(L"%d", pSecLoop[j].Name);
				}
				// 是否目录（是否有第3层）
				if (pSecLoop[j].DataIsDirectory)
				{
					// 第3层入口
					PIMAGE_RESOURCE_DIRECTORY pThirAddr = (PIMAGE_RESOURCE_DIRECTORY)((PBYTE)pResAddr + pSecLoop[j].OffsetToDirectory);
					PIMAGE_RESOURCE_DIRECTORY_ENTRY pThirLoop = (PIMAGE_RESOURCE_DIRECTORY_ENTRY)((PBYTE)pThirAddr + 16);
					PIMAGE_RESOURCE_DATA_ENTRY pData = (PIMAGE_RESOURCE_DATA_ENTRY)((PBYTE)pResAddr + pThirLoop->OffsetToData);
					temp.dwResRVA = pData->OffsetToData;
					temp.dwResFO = Rva2Offset(temp.dwResRVA);
					temp.dwSize = pData->Size;
				}
				stcResTemp.vec.push_back(temp);
			}
		}
		else
		{
			/*没碰到这种情况，未检测，先注释掉*/
			////没目录，就1层
			//stcResTemp.bOneLayer = TRUE;
			//// 资源信息偏移
			//DWORD dwDataOffSet = pResSec[i].OffsetToData;
			//// 资源信息地址
			//PIMAGE_RESOURCE_DATA_ENTRY pData = (PIMAGE_RESOURCE_DATA_ENTRY)((ULONG)pResFirst + dwDataOffSet);
			//RESOURCE_DATA_INFO temp = {};
			//temp.dwResRVA = pData->OffsetToData;
			//temp.dwSize = pData->Size;
			//stcResTemp.vec.push_back(temp);
		}
		m_vecRes.push_back(stcResTemp);
	}
	m_vecRes[0].m_dwIDNum = pResAddr->NumberOfIdEntries;
	m_vecRes[0].m_dwNameNum = pResAddr->NumberOfNamedEntries;
	return false;
}

// 获取重定位表信息
bool PEMAP::GetRelocInfo()
{
	// 获取重定位表信息
	auto dwResRVA = is64() ? pNt_Headers64->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress
		: pNt_Headers32->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress;
	auto dwRelSize = is64() ? pNt_Headers64->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].Size
		: pNt_Headers32->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].Size;
	// 计算重定位信息表地址
	auto pRelocTabAddr = (PIMAGE_BASE_RELOCATION)((PBYTE)m_buffer + Rva2Offset(dwResRVA));

	DWORD dwCountRelSize = 0;
	while (dwCountRelSize != dwRelSize)
	{
		RELOC_SECTION stcRelSec = {};
		// 已检索的重定位数据信息字节数
		dwCountRelSize += pRelocTabAddr->SizeOfBlock;
		// 重定位数据的起始RVA基数：
		stcRelSec.dwSecRVA = pRelocTabAddr->VirtualAddress;
		// 根据重定位数据的起始RVA找到对应区段
		DWORD j = 0;
		for (DWORD j = 0; j < m_vecSec.size(); j++)
		{
			if (stcRelSec.dwSecRVA >= m_vecSec[j].dwRVA && stcRelSec.dwSecRVA < m_vecSec[j].dwRVA + m_vecSec[j].dwVirtualSize)
			{
				stcRelSec.strSecName = m_vecSec[j].strName;
				break;
			}
		}
		// 循环次数和开始地址
		DWORD nCount = (pRelocTabAddr->SizeOfBlock - 8) / sizeof(WORD);
		stcRelSec.dwNumOfReloc = nCount;
		PTYPEOFFSET pLoop = (PTYPEOFFSET)((PBYTE)pRelocTabAddr + 8);
		for (DWORD i = 0; i < nCount; i++)
		{
			RELOC_DATA stcRelData = {};
			// 重定位类型
			stcRelData.bType = pLoop[i].Type;
			// 重定位数据的RVA
			stcRelData.dwDataRVA = pLoop[i].offset + pRelocTabAddr->VirtualAddress;
			// 重定位数据的文件偏移
			stcRelData.dwDataFO = Rva2Offset(stcRelData.dwDataRVA);
			// 根据重定位数据的文件偏移找到重定位数据地址
			PDWORD pDataAddr = (PDWORD)((PBYTE)m_buffer + stcRelData.dwDataFO);
			stcRelData.dwRelocValue = *pDataAddr;
			// 根据重定位数据的值找到它指向的值
			ULONG64 dwBaseImage = is64() ? pNt_Headers64->OptionalHeader.ImageBase :
				pNt_Headers32->OptionalHeader.ImageBase;
			DWORD dwRVAOfRelDataPoint = stcRelData.dwRelocValue - dwBaseImage;//默认加载基址
			PBYTE pDataOfRelPoint = (PBYTE)((PBYTE)(m_buffer + Rva2Offset(dwRVAOfRelDataPoint)));
			memcpy_s(stcRelData.bData, 10, pDataOfRelPoint, 10);
			stcRelSec.vec.push_back(stcRelData);
		}
		pRelocTabAddr = (PIMAGE_BASE_RELOCATION)((PBYTE)pRelocTabAddr + pRelocTabAddr->SizeOfBlock);
		m_vecReloc.push_back(stcRelSec);
	}
	return false;
}