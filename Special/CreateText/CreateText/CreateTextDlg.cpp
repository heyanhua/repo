
// CreateTextDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "CreateText.h"
#include "CreateTextDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCreateTextDlg 对话框



CCreateTextDlg::CCreateTextDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_CREATETEXT_DIALOG, pParent)
	, m_0(false)
	, m_1(false)
	, m_2(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCreateTextDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_edit);
	DDX_Control(pDX, IDCANCEL, b_0);
	DDX_Control(pDX, IDC_CHECK1, m_ch0);
	DDX_Control(pDX, IDC_CHECK2, m_ch1);
	DDX_Control(pDX, IDC_CHECK4, m_ch2);
	DDX_Control(pDX, IDC_CHECK3, m_ch3);
	DDX_Control(pDX, IDC_CHECK5, m_ch4);
	DDX_Control(pDX, IDC_CHECK6, m_ch5);
	DDX_Control(pDX, IDC_CHECK8, m_ch6);
	DDX_Control(pDX, IDC_CHECK7, m_ch7);
	DDX_Control(pDX, IDC_CHECK9, m_ch8);
	DDX_Control(pDX, IDC_CHECK10, m_ch9);
	DDX_Control(pDX, IDC_CHECK12, m_cha);
	DDX_Control(pDX, IDC_CHECK11, m_chb);
	DDX_Control(pDX, IDC_CHECK13, m_chc);
	DDX_Control(pDX, IDC_CHECK14, m_chd);
	DDX_Control(pDX, IDC_CHECK16, m_che);
	DDX_Control(pDX, IDC_CHECK15, m_chf);
}

BEGIN_MESSAGE_MAP(CCreateTextDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CCreateTextDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CCreateTextDlg 消息处理程序

BOOL CCreateTextDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CCreateTextDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CCreateTextDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


#include <stdint.h>
void CCreateTextDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码


	

		//BOOL SHGetSpecialFolderPath(
		//	HWND hwndOwner,
		//	LPTSTR lpszPath, //路径，长度至少为MAX_PATH，Windows下即260
		//	CSIDL_DESKTOPDIRECTORY,//MSDN上搜索CSIDL，可以知道常用的特殊路径的宏定义
		//	BOOL fCreate//如果为true，表示不存在指示的特殊路径，则创建
		//	);

	//获取桌面路径
	TCHAR szDeskTopDirPath[MAX_PATH];
	ZeroMemory(szDeskTopDirPath, MAX_PATH);
	SHGetSpecialFolderPath(NULL, szDeskTopDirPath, CSIDL_DESKTOPDIRECTORY, FALSE);//CSIDL_INTERNET_CACHE，IE临时文件目录
	CString szFileName = szDeskTopDirPath;
	szFileName += TEXT("\\number.txt");


	//获取排除字符表
	bool removech[0x10] = {};
	auto checkchar = [](  CButton&  ctrl) ->bool
	{
		return  BST_CHECKED == ctrl.GetCheck();
	};
	
	removech[0] = checkchar(m_ch0);
	removech[1] = checkchar(m_ch1);
	removech[2] = checkchar(m_ch2);
	removech[3] = checkchar(m_ch3);
	removech[4] = checkchar(m_ch4);
	removech[5] = checkchar(m_ch5);
	removech[6] = checkchar(m_ch6);
	removech[7] = checkchar(m_ch7);
	removech[8] = checkchar(m_ch8);
	removech[9] = checkchar(m_ch9);
	removech[0xa] = checkchar(m_cha);
	removech[0xb] = checkchar(m_chb);
	removech[0xc] = checkchar(m_chc);
	removech[0xd] = checkchar(m_chd);
	removech[0xe] = checkchar(m_che);
	removech[0xf] = checkchar(m_chf);
	



	//将选中的字符放入排除表中
	char   removelist[0x11] = {};
	size_t    listsize = 0;

	int  index = 0;
	for (int i = 0; i < 0x10; i++)
	{
		if (removech[i] == true)
		{
			removelist[index++] = '0' + i;

		}
	}


	CStringA str;
	m_edit.GetWindowText(str);
	uint64_t  value = _ttoi64(str);
	
	

	str = TEXT("");
	CStringA tmp=TEXT("");
	value = value * 1024 / 4;  //字符数
	//排除字符
	for (int i = 0; i < value; i++)
	{
		tmp.Format(TEXT("%04x"),i);
		//查找子串
		if (-1 == tmp.FindOneOf(removelist))  //没有找到符合要求]
		{
			str += tmp;
		}
		else  //找到了[不符合要求]
		{
			value++;
		}


		
	}

	auto hFile =  CreateFile(szFileName, GENERIC_WRITE,NULL,NULL, CREATE_ALWAYS,NULL,NULL);
	DWORD  WrittenSize = 0;
	WriteFile(hFile, str.GetBuffer(),str.GetLength(), &WrittenSize,NULL);

	CloseHandle(hFile);
	CDialogEx::OnOK();
}
