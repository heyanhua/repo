
// CreateTextDlg.h : 头文件
//

#pragma once
#include "afxwin.h"


// CCreateTextDlg 对话框
class CCreateTextDlg : public CDialogEx
{
// 构造
public:
	CCreateTextDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CREATETEXT_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edit;
	afx_msg void OnBnClickedOk();
	CButton b_0;
	bool m_0;
	bool m_1;
	int m_2;
	CButton m_ch0;
	CButton m_ch1;
	CButton m_ch2;
	CButton m_ch3;
	CButton m_ch4;
	CButton m_ch5;
	CButton m_ch6;
	CButton m_ch7;
	CButton m_ch8;
	CButton m_ch9;
	CButton m_cha;
	CButton m_chb;
	CButton m_chc;
	CButton m_chd;
	CButton m_che;
	CButton m_chf;
};
