#include "stdafx.h"
#include "Instruction.h"


Instruction::Instruction(void)
{
	condSIMD = false; //默认不解析SIMD指令
	condx64  = false; //默认不解析x64指令
	condRealmode = false; //默认不是实地址模式
	condx87  = false; //默认不解析x87指令
	condx86 =  true;
	Clean();

}




Instruction::~Instruction(void)
{
}

void Instruction::Clean()
{

	//初始化x86前缀段
	x86_prefix.Group_1.second = 0;
	x86_prefix.Group_1.first = false;
	x86_prefix.Group_2.second = 0;
	x86_prefix.Group_2.first = false;
	x86_prefix.Group_3.second = 0;
	x86_prefix.Group_3.first = false;
	x86_prefix.Group_4.second = 0;
	x86_prefix.Group_4.first = false;

	//初始化x64前缀段
	x64_prefix.byte.second = 0;
	x64_prefix.byte.first = false;

	//初始化Opcode段
	m_Opcode.SIMD_prefix.second = 0;
	m_Opcode.SIMD_prefix.first = false;
	m_Opcode.escape_opcode.second = 0;
	m_Opcode.escape_opcode.first = false;
	m_Opcode.escape_opcode_plus.second = 0;
	m_Opcode.escape_opcode_plus.first = false;
	m_Opcode.opcode.second = 0;
	m_Opcode.opcode.first = false;

	//初始化Mod/RM段
	m_ModRM.modrm.second = 0;
	m_ModRM.modrm.first = false;

	//初始化SIB段
	m_SIB.sib.second = 0;
	m_SIB.sib.first = false;

	//初始化Disp段 和 Imme段

	for (int i = 0; i < 0x8; i++)
	{
		m_Disp.byte[i].second = 0;
		m_Disp.byte[i].first  = false;
		m_Imme.byte[i].second = 0;
		m_Imme.byte[i].first  = false;
	}

}

unsigned char Instruction::SetLegacy_Prefix( unsigned char  byte)
{
	//判断并设置Group1
	if(false == x86_prefix.Group_1.first )
		if ((unsigned char)Legacy_Prefix::Group_1_byte::LOCK_prefix == byte \
			|| (unsigned char)Legacy_Prefix::Group_1_byte::REPNE_REPNZ == byte \
			|| (unsigned char)Legacy_Prefix::Group_1_byte::REP_REPE_REPZ == byte \
			|| (unsigned char)Legacy_Prefix::Group_1_byte::Bound_prefix == byte )
		{
			x86_prefix.Group_1.first = true;
			x86_prefix.Group_1.second = byte;
			return  byte;
		}

	//判断并设置Group2
	if (false == x86_prefix.Group_2.first)
		if ((unsigned char)Legacy_Prefix::Group_2_byte::SS_segment_override_prefix == byte \
			|| (unsigned char)Legacy_Prefix::Group_2_byte::DS_segment_override_prefix == byte \
			|| (unsigned char)Legacy_Prefix::Group_2_byte::ES_segment_override_prefix == byte \
			|| (unsigned char)Legacy_Prefix::Group_2_byte::FS_segment_override_prefix == byte \
			|| (unsigned char)Legacy_Prefix::Group_2_byte::GS_segment_override_prefix == byte \
			|| (unsigned char)Legacy_Prefix::Group_2_byte::Branch_not_taken == byte \
			|| (unsigned char)Legacy_Prefix::Group_2_byte::Branch_taken == byte )
		{
			x86_prefix.Group_2.first = true;
			x86_prefix.Group_2.second = byte;
			return  byte;
		}

	//判断并设置Group3
	if (false == x86_prefix.Group_3.first)
		if ((unsigned char)Legacy_Prefix::Group_3_byte::Operand_size_override_prefix == byte )
		{
			x86_prefix.Group_3.first = true;
			x86_prefix.Group_3.second = byte;
			return  byte;
		}

	//判断并设置Group4
	if (false == x86_prefix.Group_4.first)
		if ((unsigned char)Legacy_Prefix::Group_4_byte::Address_size_override_prefix == byte)
		{
			x86_prefix.Group_4.first = true;
			x86_prefix.Group_4.second = byte;
			return  byte;
		}
	return byte-1;
}

unsigned char Instruction::SetREX_prefix( unsigned char  byte)
{
	//判断并设置Group4
	if (true == condx64 && false == x86_prefix.Group_4.first )
		if (REX_prefix::REX::begin<= byte &&  byte< REX_prefix::REX::end)
		{
			x86_prefix.Group_4.first = true;
			x86_prefix.Group_4.second = byte;
			return  byte;
		}
	return  byte-1;
}

unsigned char Instruction::SetOpcode( unsigned char  byte)
{


	//判断并设置SIMD_prefix --- //启用SIMD指令开关时
	if (false == m_Opcode.SIMD_prefix.first && true == condSIMD)
	{
		if ((unsigned char)Opcode::non_real_opcode::SIMD_prefix_0x66 == byte \
			|| (unsigned char)Opcode::non_real_opcode::SIMD_prefix_0xF2 == byte \
			|| (unsigned char)Opcode::non_real_opcode::SIMD_prefix_0xF3 == byte )
		{
			m_Opcode.SIMD_prefix.first = true;
			m_Opcode.SIMD_prefix.second = byte;
			return byte;
		}
	}

	//判断并设置escape_opcode
	if (false == m_Opcode.escape_opcode.first && \
		(unsigned char)Opcode::non_real_opcode::escape == byte)
	{
		m_Opcode.escape_opcode.first = true;
		m_Opcode.escape_opcode.second = byte;
		return byte;
	}

	//判断并设置escape_opcode_plus
	if (false == m_Opcode.escape_opcode_plus.first && true == m_Opcode.SIMD_prefix.first)
	{
		if ((unsigned char)Opcode::non_real_opcode::escape_plus_0x3A == byte \
			|| (unsigned char)Opcode::non_real_opcode::escape_plus_0x38 == byte)
		{
			m_Opcode.escape_opcode_plus.first = true;
			m_Opcode.escape_opcode_plus.second = byte;
			return byte;
		}
	}

	//判断并设置opcode
	if (false == m_Opcode.opcode.first)
	{
		m_Opcode.opcode.first = true;
		m_Opcode.opcode.second = byte;
		return byte;
	}

	return  byte - 1;
}


unsigned char Instruction::SetModRM( unsigned char  byte)
{

	if (false == m_ModRM.modrm.first)
	{
		if (m_ModRM.arrNonModRM[byte >> 4][(byte << 4) >> 4]) /*查表*/
		{
			m_ModRM.modrm.first = true;
			m_ModRM.modrm.second = byte;
			

			return byte;
		}
	}
	return  byte - 1;
}

unsigned char Instruction::SetSIB( unsigned char  byte)
{
	if (true == m_ModRM.modrm.first && false == m_SIB.sib.first)
	{
		//分析是否需要sib  --RM区域 == 100,  0x4  且 mod != 11  0x3
		if (0x4 == m_ModRM.GitR_M() && 0x3!= m_ModRM.GetMod())
		{
			m_SIB.sib.second = byte;
			m_SIB.sib.first = true;
			return  byte;
		}

	}
	return  byte - 1;
}

unsigned char Instruction::SetDisplacement( unsigned char  byte)
{
	//Disp依赖Mod
	if (false == m_ModRM.modrm.first )
	{
		return byte - 1;
	}
	//有Mod但是没有Disp
	if (0x0 == m_ModRM.GetMod() && 0x4 == m_ModRM.GetMod())
	{
		return byte - 1;
	}




	//设置byte0     
	if( false ==m_Disp.byte[0].first)
		return m_Disp.SetDisp(byte,0);
	//判断是否有设置byte1   
	if (0x2 != m_ModRM.GetMod())
		return byte - 1;
	
	//设置byte1
	if (false == m_Disp.byte[1].first)
		return m_Disp.SetDisp(byte,1);
	//判断是否有byte2  ---------实地址模式下的16位
	if (true == condRealmode && false == x86_prefix.Group_4.first)
		return byte - 1;

	//判断是否有byte2  ---------32位下的16位
	if (true == condx86 && true == x86_prefix.Group_4.first)
		return byte - 1;

	// 设置byte2  
	if (false == m_Disp.byte[2].first)
		return m_Disp.SetDisp(byte,2);


	//判断是否有byte3  byte4
	if (false == condx64)
		return byte - 1;
	if (true == condx64 && true == x86_prefix.Group_4.first)
		return byte - 1;

	// 设置byte3      
	if (false == m_Disp.byte[3].first)
		return m_Disp.SetDisp(byte,3);
	//设置byte4      
	if (false == m_Disp.byte[4].first)
		return m_Disp.SetDisp(byte,4);

	//设置byte5      
	if (false == m_Disp.byte[5].first)
		return m_Disp.SetDisp(byte,5);

	//设置byte6      
	if (false == m_Disp.byte[6].first)
		return m_Disp.SetDisp(byte,6);

	//设置byte7      
	if (false == m_Disp.byte[7].first)
		return m_Disp.SetDisp(byte,7);

	return  byte - 1;
}

unsigned char Instruction::SetImmediate(unsigned char  byte)
{
	//1.表外例外情况
	if (0x05 == byte && 0x0F == m_Opcode.escape_opcode.second)
		return byte - 1;
	if (0xF6 == byte || 0xF7 == byte)
	{
		if (0x00 == m_ModRM.GetREG() || 0x01 == m_ModRM.GetREG())
			return   byte - 1;
	}

	//判断是否需要Immediate
	//1.查表判断
	if (true == m_ModRM.arrNonImme[byte >> 4][(byte << 4) >> 4]) /*查表*/
	{
		return byte - 1;
	}



	//设置byte0      
	if (false == m_Imme.byte[0].first)
		return m_Imme.SetImme(byte,0);


	//判断是否有byte2  ---------实地址模式下的16位
	if (true == condRealmode && false == x86_prefix.Group_3.first)
		return byte - 1;

	//判断是否有byte2  ---------32位下的16位
	if (true == condx86 && true == x86_prefix.Group_3.first)
		return byte - 1;

	//设置byte1      
	if (false == m_Imme.byte[1].first)
		return m_Imme.SetImme(byte,1);

	//设置byte2      
	if (false == m_Imme.byte[2].first)
		return m_Imme.SetImme(byte,2);

	//设置byte3      
	if (false == m_Imme.byte[3].first)
		return m_Imme.SetImme(byte,3);

	//设置byte4      
	if (false == m_Imme.byte[4].first)
		return m_Imme.SetImme(byte,4);

	//设置byte5      
	if (false == m_Imme.byte[5].first)
		return m_Imme.SetImme(byte,5);

	//设置byte6      
	if (false == m_Imme.byte[6].first)
		return m_Imme.SetImme(byte,6);

	//设置byte7      
	if (false == m_Imme.byte[7].first)
		return m_Imme.SetImme(byte,7);

	return  byte - 1;

}

