#pragma once

#include "Instruction.h"

#define  MAX_OPCODEBUFFER_LENTH   16
//解码类
class Encoding: public Instruction
{
public:
	Encoding(void);
	~Encoding(void);

	
	Encoding( char * pstr);
	//汇编指令地址
	void SetMnemonic( char * pstr);
	void CleanOpCodeBuffer();
	pair<int , char*>&  GetOpCode();


private:
	//字符串  ---入参
	char * m_pMnemonic;	
	//数据    ---结果
	char m_OpCodeBuffer[MAX_OPCODEBUFFER_LENTH];
	//输出
	pair<int , char * >   m_Out;


};

