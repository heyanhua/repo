#pragma once

#include "Instructions_struct.h"
class Instruction
{
public:
	Instruction(void);
	~Instruction(void);
	void Clean();

	//------该系列函数会测试给定值是否为本字段
	//------如果设置成功则返回原来的值
	//------如果设置失败则！=原来的值
	//------应该这样测试返回值     if( byte  == SetLegacy_Prefix( byte))    successful;
	unsigned char    SetLegacy_Prefix( unsigned char  byte);
	unsigned char    SetREX_prefix( unsigned char  byte);
	//------如果返回失败,说明opcode已经设置过  exsit --需要解析opcode才能进行下面的操作
	unsigned char    SetOpcode( unsigned char  byte);
	unsigned char    SetModRM( unsigned char  byte);
	unsigned char    SetSIB( unsigned char  byte);
	unsigned char    SetDisplacement( unsigned char  byte);
	unsigned char    SetImmediate( unsigned char  byte);


private:
	//各种解析开关

	bool             condSIMD; //是否启用SIMD指令集
	bool             condx86;  //x86指令集
	bool             condx64;  //是否启用x64指令集
	bool             condRealmode; //实地址模式, 16位地址
	bool             condx87;  //是否启用x87指令集
	bool             CS_D;  //CS寄存器的D标志
	bool             CS_L;  //CS寄存器的L标志
public:
	Legacy_Prefix    x86_prefix;
	REX_prefix	     x64_prefix;
	Opcode		     m_Opcode;
	ModRM		     m_ModRM;
	SIB			     m_SIB;
	Displacement     m_Disp;
	Immediate	     m_Imme;
};

