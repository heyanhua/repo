#pragma once
#include "stdafx.h"
#include "Instructions_struct.h"


//所有的没有Mod模式的指令.  true表示没有, false表示有mod
const bool ModRM::arrNonModRM[0x10][0x10] = {
	 /* 0x0,  0x1,  0x2,  0x3,     0x4,  0x5,  0x6,  0x7,     0x8,  0x9,  0xA,  0xB,     0xC,  0xD,  0xE,  0xF  */
/*0x0*/ false,false,false,false,   true ,true ,true ,true ,   false,false,false,false,   true ,true ,true ,false,
/*0x1*/ false,false,false,false,   true ,true ,true ,true ,	  false,false,false,false,   true ,true ,true ,true ,
/*0x2*/ false,false,false,false,   true ,true ,false,true ,	  false,false,false,false,   true ,true ,true ,true ,
/*0x3*/ false,false,false,false,   true ,true ,false,true ,	  false,false,false,false,   true ,true ,true ,true ,

/*0x4*/ true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,
/*0x5*/ true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,
/*0x6*/ true ,true ,false,false,   false,false,false,false,   true ,false,true ,false,   true ,true ,true ,true ,
/*0x7*/ true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,

/*0x8*/ false,false,false,false,   false,false,false,false,   false,false,false,false,   false,false,false,false,
/*0x9*/ true ,true ,true ,true ,   true ,true ,true ,true ,   false,false,false,false,   true ,true ,true ,true ,
/*0xA*/ true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,
/*0xB*/ true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,

/*0xC*/ false,false,true ,true ,   true ,true ,false,false,   true ,true ,true ,true ,   true ,true ,true ,true ,
/*0xD*/ false,false,false,false,   true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,
/*0xE*/ true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,
/*0xF*/ true ,true ,true ,true ,   true ,true ,false,false,   true ,true ,true ,true ,   true ,true ,false,false
};

//所有的没有Imme字节的指令.  true表示没有, false表示有-------包括但不仅限于此
//例外情况1.      为:  0F引导的 opcode  0F 05  无需imme
//        2.      为:  F6需要  ModRM.reg 为 000 或  001时     有 1byte imme
//        3.      为:  F7需要  ModRm.reg 为 000 和  001时     有 2byte 或 4byte imme
const bool ModRM::arrNonImme[0x10][0x10] = {
	 /* 0x0,  0x1,  0x2,  0x3,     0x4,  0x5,  0x6,  0x7,     0x8,  0x9,  0xA,  0xB,     0xC,  0xD,  0xE,  0xF  */
/*0x0*/ true ,true ,true ,true ,   false,false,true ,true ,   true ,true ,true ,true ,   false,false,true ,true ,
/*0x1*/ true ,true ,true ,true ,   false,false,true ,true ,   true ,true ,true ,true ,   false,false,true ,true ,
/*0x2*/ true ,true ,true ,true ,   false,false,true ,true ,   true ,true ,true ,true ,   false,false,true ,true ,
/*0x3*/ true ,true ,true ,true ,   false,false,true ,true ,   true ,true ,true ,true ,   false,false,true ,true ,

/*0x4*/ true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,
/*0x5*/ true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,
/*0x6*/ true ,true ,true ,true ,   true ,true ,true ,true ,   false,false,false,false,   true ,true ,true ,true ,
/*0x7*/ true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,

/*0x8*/ false,false,true ,false,   true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,
/*0x9*/ true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,
/*0xA*/ true ,true ,true ,true ,   true ,true ,true ,true ,   false,false,true ,true ,   true ,true ,true ,true ,
/*0xB*/ false,false,false,false,   false,false,false,false,   false,false,false,false,   false,false,false,false,

/*0xC*/ false,false,false,true ,   true ,true ,false,false,   true ,true ,false,true ,   true ,true ,true ,true ,
/*0xD*/ true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,
/*0xE*/ true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,
/*0xF*/ true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true ,   true ,true ,true ,true 
};
