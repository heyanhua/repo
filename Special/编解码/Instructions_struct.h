#include <map>
using std::pair;
using std::make_pair;

//x86  Prefix     
struct Legacy_Prefix
{

	enum Group_1_byte
	{
		// Lock and repeat prefixes
		LOCK_prefix = 0xF0,
		REPNE_REPNZ = 0xF2,
		REP_REPE_REPZ  = 0xF3,
		//Bound prefix
		Bound_prefix = 0xF2
	};
    //if bool equs true,  the  char  is  Group_1_byte's values,  ****single selection****
	pair<bool,unsigned char>  Group_1;




	enum Group_2_byte
	{
        //segment override (use with any branch instruction is reserved)  ----change memory operand segment-selector
		SS_segment_override_prefix = 0x36,
		DS_segment_override_prefix = 0x2E,
		ES_segment_override_prefix = 0x26,
		FS_segment_override_prefix = 0x64,
		GS_segment_override_prefix = 0x65,
		// Branch hints:   (used only with Jcc instructions)
		Branch_not_taken =  0x2E,
        Branch_taken     =  0x3E
	};

	pair<bool,unsigned char>  Group_2;

	enum Group_3_byte
	{
		Operand_size_override_prefix  = 0x66   //change default operand size
	};

	pair<bool,unsigned char>  Group_3;

	enum Group_4_byte
	{
		Address_size_override_prefix  = 0x67  //change default operand address size
	};
	pair<bool,unsigned char>  Group_4;

	 
};


//use  x64 operand,   change default operand size (can be use x64 operands) --64bit 's  change default operand address size
//use  x64 register extend eg, r8 - r15，xmm8 - xmm15 
struct REX_prefix
{
	//the unsigned char is  REX's values
	pair <bool, unsigned char>  byte;
	//in x64
	enum REX
	{
		begin = 0x40,
		null = 0x40, //NULL  REX_prefix
		B =	   0x45, //SIB.base  x64  extend 
		X =    0x46, //SIB.index x64  extend
		R =    0x47, //ModRM.r/m x64  extend  
		W =    0x48,  //width	---- 此标志使用64位操作数           若无则使用缺省操作数大小  default operand size
		Opcode_Ex = 0x49, //扩展Opcode里的reg(在Imme模式下Mod/RM的reg是被闲置的)
		end       = 0x50
	}; 


};


//1, 2, or 3 bytes
struct Opcode
{
	enum Opcode_byte_test
	{
		Operand_size_8			= 0x00,//0000 0000    第0位置0
		Operand_size_16			= 0x01,//如果要表达16bit的 操作数，  需要 prefix前缀， 这个字段将被忽略       第0位置1
		Operand_size_32			= 0x01,//0000 0001    第0位置1
		Immediate_size_8		= 0x01,//0000 0001    8bit
		Immediate_size_16       = 0x00,
		Immediate_size_32       = 0x00,
		Operand_direction_register_to_memory	=  0x00,//0000 0000  第一位置0
		Operand_direction_memory_to_register    =  0x02,//0000 0010  第一位置1
		Operand_Immediate_mode					=  0x80,//1000 0000  第七位置1
		

	};


	struct Opcode_byte
	{
		unsigned char  Operand_size:1;//0000 0001    0   size  8   1   16or 32  
		unsigned char  Operand_direction:1; //0000 0010    0    reg -> r/m     1  r/m  ->  reg
		unsigned char  Operand_OpcodeCore:5; // 0111 1100      Opcode 的核心码
		unsigned char  Operand_Immediate_mode:1;  //1000  0000      0  no Immediate   1  have Immediate
	    //如果 Operand_Immediate_mode被置为1,则 Operand_direction 不再是表示方向，因为 立即数不能当作目的操作数，
		//since,  Operand_Immediate_mode 为1 时,不需要方向字段,  方向字段被转意为:
		//也就是它的方向永远为0，即  目的操作数在 MOD的R/M字段, 而将REG字段不用， 也就是置空
		//而原来的方向D被设置为 x  bit,  标识 immediate 的操作数长度，  immediate 被cpu当作有符号数
		//而且， REG被空置出来，  reg又被重新使用， REG的三个bits  在  Immediate mode 被转意
	};





	//Opcode的字节数， 如果第一个字节为 0f, 
	//the unsigned char is  REX's values   single selection
	
	//正常Opcode是一个字节,使用byte[0],
	
	//若第一个字节为0F，证明这个Opcode至少两个字节，  使用byte[0]和 byte[1], 3字节情况暂时不讨论

	enum non_real_opcode
	{
		SIMD_prefix_0x66 = 0x66,
		SIMD_prefix_0xF2 = 0xF2,
		SIMD_prefix_0xF3 = 0xF3,
		escape = 0x0F,
		escape_plus_0x3A = 0x3A,
		escape_plus_0x38 = 0x38,
	};

	pair<bool, unsigned char>  SIMD_prefix; //escape opcode  0x0f  SIMD prefix
	pair<bool, unsigned char>  escape_opcode; //only 0x0F
	pair<bool, unsigned char>  escape_opcode_plus; //only used SIMB   mean  SIMD_prefix_plus
	pair<bool, unsigned char>  opcode; //real opcode

	

};

 

//1byte
struct ModRM
{
	pair<bool, unsigned char> modrm;

	unsigned char  GitR_M() { return modrm.first << 5 >> 5; }; /*0,1,2 bits*/
	unsigned char  GetREG() { return modrm.first << 2 >> 5; }; /*3,4,5 bits*/
	unsigned char  GetMod() { return modrm.first >> 6;	    }; /*6,7   bits*/

//The R/M field, combined with MOD, specifies either
//the second operand in a two-operand instruction, or
//the only operand in a single-operand instruction like NOT or NEG.
//如果是双操作数指令， 则R_M标识第二个操作数，
//如果是单操作数指令， 则R_M标识操作数
	enum R_M_field
	{
		//关于命名:  mem_EAX    == [EAX],标识EAX是个内存地址,mem_SIB 引导 
		// mem_EAX_plus_disp8   == [EAX + n] n是一个8bit   signed byte  or  signed char
		// mem_EAX_plus_disp32  == [EAX + n] n是一个32bit  signed int
		mem_EAX    = 0x0,				    
		mem_ECX    = 0x1,
		mem_EDX    = 0x2,
		mem_EBX	   = 0x3,
		mem_SIB    = 0x4, //100
		mem_disp32 = 0x5,
		mem_ESI    = 0x6,
		mem_EDI    = 0x7,

		mem_EAX_plus_disp8    = 0x0,				    
		mem_ECX_plus_disp8    = 0x1,
		mem_EDX_plus_disp8    = 0x2,
		mem_EBX_plus_disp8	  = 0x3,
		mem_SIB_plus_disp8    = 0x4, //100
		mem_EBP_plus_disp8    = 0x5,
		mem_ESI_plus_disp8    = 0x6,
		mem_EDI_plus_disp8    = 0x7,

		mem_EAX_plus_disp32    = 0x0,				    
		mem_ECX_plus_disp32    = 0x1,
		mem_EDX_plus_disp32    = 0x2,
		mem_EBX_plus_disp32	   = 0x3,
		mem_SIB_plus_disp32	   = 0x4, //100
		mem_EBP_plus_disp32    = 0x5,
		mem_ESI_plus_disp32    = 0x6,
		mem_EDI_plus_disp32    = 0x7,
												  
	  _AL = 0x0,   _AX = 0x0,      _EAX = 0x0,    _MM0 = 0x0,     _XMM0 = 0x0,  
	  _CL = 0x1,   _CX = 0x1,	   _ECX = 0x1,	  _MM1 = 0x1,     _XMM1 = 0x1,  
	  _DL = 0x2,   _DX = 0x2,	   _EDX = 0x2,	  _MM2 = 0x2,     _XMM2 = 0x2,  
	  _BL = 0x3,   _BX = 0x3,	   _EBX = 0x3,	  _MM3 = 0x3,     _XMM3 = 0x3,  
	  _AH = 0x4,   _SP = 0x4,	   _ESP = 0x4,	  _MM4 = 0x4,     _XMM4 = 0x4,  
	  _CH = 0x5,   _BP = 0x5,	   _EBP = 0x5,	  _MM5 = 0x5,     _XMM5 = 0x5,  
	  _DH = 0x6,   _SI = 0x6,	   _ESI = 0x6,	  _MM6 = 0x6,     _XMM6 = 0x6,  
	  _BH = 0x7,   _DI = 0x7,	   _EDI = 0x7,	  _MM7 = 0x7,     _XMM7 = 0x7  
	};


	enum REG_field
	{
	  AL = 0x0,   AX = 0x0,    EAX = 0x0,    MM0 = 0x0,     XMM0 = 0x0,  
	  CL = 0x1,   CX = 0x1,	   ECX = 0x1,	 MM1 = 0x1,     XMM1 = 0x1,  
	  DL = 0x2,   DX = 0x2,	   EDX = 0x2,	 MM2 = 0x2,     XMM2 = 0x2,  
	  BL = 0x3,   BX = 0x3,	   EBX = 0x3,	 MM3 = 0x3,     XMM3 = 0x3,  
	  AH = 0x4,   SP = 0x4,	   ESP = 0x4,	 MM4 = 0x4,     XMM4 = 0x4,  
	  CH = 0x5,   BP = 0x5,	   EBP = 0x5,	 MM5 = 0x5,     XMM5 = 0x5,  
	  DH = 0x6,   SI = 0x6,	   ESI = 0x6,	 MM6 = 0x6,     XMM6 = 0x6,  
	  BH = 0x7,   DI = 0x7,	   EDI = 0x7,	 MM7 = 0x7,     XMM7 = 0x7  
	};

	enum Mod_field
	{
		Register_indirect_addressing			= 0x0,//00
		Register_displacement8_addressing       = 0x1,//01
		Register_displacement32_addressing      = 0x2,//10
		Register_addressing                     = 0x3 //11
	};

	static const  bool  arrNonModRM[0x10][0x10];
	static const  bool  arrNonImme[0x10][0x10];

};





//1byte
struct SIB
{
	//Base +  index  *  1 or  2  or  4  or  8    00  01  10   11
	pair<bool, unsigned char> sib;
	enum IndexORBase_Register
	{
	   EAX    = 0x0,
	   ECX    = 0x1,
	   EDX    = 0x2,
	   EBX	  = 0x3,
	   none   = 0x4, 
	   EBP    = 0x5,
	   ESI    = 0x6,
	   EDI    = 0x7
	};
	//the unsigned char is  Index_Register's values   ****single selection****
	//unsigned char Base :3;//bit[0,1,2]   
	//the unsigned char is  Index_Register's values   ****single selection****
	//unsigned char Index:3;//bit[3,4,5]
	//unsigned char Scale:2;//bit[6,7]

	unsigned char  GitBase()  { return sib.first << 5 >> 5; };  /*0,1,2 bits*/
	unsigned char  GetIndex() { return sib.first << 2 >> 5; }; /*3,4,5 bits*/
	unsigned char  GetScale() { return sib.first >> 6;      }; /*6,7 bits*/
};
//1,2 or 4 byte
struct Displacement
{
	pair<bool, unsigned char>   byte[0x8];
	unsigned char SetDisp(unsigned char bt,unsigned int index) {  
			byte[index].first = true;
			byte[index].second = bt;
			return bt;
	};
};

//1,2 or 4 byte
struct Immediate
{
	pair<bool, unsigned char>   byte[0x8];
	unsigned char SetImme(unsigned char bt,unsigned int index) {
		byte[index].first = true;
		byte[index].second = bt;
		return bt;
	};
};