
#include "stdafx.h"
#include "CmyProcessInfo.h"



CmyProcessInfo::CmyProcessInfo()
{
}

CmyProcessInfo::~CmyProcessInfo()
{
}

void CmyProcessInfo::FreshProcessInfo()
{
	//清理数据
	decltype(m_Processes) tmpV;
	m_Processes.swap(tmpV);


	//1 创建一个快照
	HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32  item{sizeof(PROCESSENTRY32)};
	//2 找到第一个进程
	if (FALSE == Process32First(hSnap, &item))
	{
		CloseHandle(hSnap);
		return;
	}
	//3 循环遍历进程
	do
	{
		m_Processes.push_back(item);
	} while ( TRUE == Process32Next(hSnap, &item));
	CloseHandle(hSnap);
}

void CmyProcessInfo::FreshModuesInfo(DWORD pid)
{


	//1 创建一个快照
	HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pid);
	MODULEENTRY32  item{ sizeof(MODULEENTRY32) };
	//2 找到第一个模块
	if (FALSE == Module32First(hSnap, &item))
	{
		CloseHandle(hSnap);
		return;
	}
	decltype(m_Modues[0].second)  tempV_m;
	//3 循环遍历模块
	do
	{
		tempV_m.push_back(item);
		
	} while (TRUE == Module32Next(hSnap, &item));


	//4 改写信息
	bool condfind = false;
	for (auto &it : m_Modues)
	{
		if (it.first == pid)
		{
			it.second.swap(tempV_m);  //改写信息
			condfind = true;
			break;
		}
	}
	if (condfind == false)
		m_Modues.emplace_back(pid,tempV_m);

	CloseHandle(hSnap);

}

void CmyProcessInfo::FreshThreadsInfo()
{
	decltype(m_Threads) tmpV;
	m_Threads.swap(tmpV);

	
	//1 创建一个快照
	HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
	THREADENTRY32 item{sizeof(THREADENTRY32)};
	//2 找到第一个线程
	if (FALSE == Thread32First(hSnap, &item))
	{
		CloseHandle(hSnap);
		return;
	}
	//3 循环遍历线程
	do
	{
		bool condFind = false; 
		for (auto &it : m_Threads)
		{
			if (it.first == item.th32OwnerProcessID)
			{
				condFind = true; //找到
				it.second.push_back(item);
				break;
			}
		}
		if (!condFind)
		{
			decltype(m_Threads[0].second)  v;
			v.emplace_back(item);
			m_Threads.emplace_back(item.th32OwnerProcessID, v);
		}

		decltype(item) TmpItem{ sizeof(THREADENTRY32) };
		item = TmpItem;
	} while (TRUE == Thread32Next(hSnap, &item));
	CloseHandle(hSnap);

}

void CmyProcessInfo::FreshHeapsInfo(DWORD pid)
{

	   HANDLE hHeapSnap = CreateToolhelp32Snapshot(TH32CS_SNAPHEAPLIST, GetCurrentProcessId());
	   if (hHeapSnap == INVALID_HANDLE_VALUE)
		   return ;

	   decltype(m_Heaps[0].second)  ProcessHeapInfo;
	   HEAPLIST32 hl{ sizeof(HEAPLIST32) };  //list
	   if (Heap32ListFirst(hHeapSnap, &hl))
	   {
		   
		  do
		  {
			   decltype(ProcessHeapInfo[0].entry) entry;
				   HEAPENTRY32 he{ sizeof(HEAPENTRY32) }; //entry

			   if (Heap32First(&he, GetCurrentProcessId(), hl.th32HeapID))
			   {

				   do
				   {
					
					   //最终块的信息
					   entry.push_back(he);
					   he.dwSize = sizeof(HEAPENTRY32);
				   } while (Heap32Next(&he));
			   }
			   myPROCESSHEAP32		item;
			   item.list = hl;
			   item.entry = entry;
			   ProcessHeapInfo.push_back(item);
			   hl.dwSize = sizeof(HEAPLIST32);
		  } while (Heap32ListNext(hHeapSnap, &hl));

		   bool condfind = false;
		   for (auto &it : m_Heaps)
		   {
			   if (it.first == pid)
			   {
				   it.second.swap(ProcessHeapInfo);
				   condfind = true;
				   break;
			   }
		   }
		   if (condfind == false)
			   m_Heaps.emplace_back(pid, ProcessHeapInfo);

	   }

	   CloseHandle(hHeapSnap);
	   


}
