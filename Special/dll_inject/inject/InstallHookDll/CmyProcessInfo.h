#pragma once



#include <vector>
using std::vector;
#include <map>
using std::pair;
using std::make_pair;
#include <Windows.h>   
#include <TlHelp32.h>  //need   CreateToolhelp32Snapshot functions


typedef struct _myPROCESSHEAP32
{
	HEAPLIST32			 list;
	vector<HEAPENTRY32>  entry;
}myPROCESSHEAP32;



class CmyProcessInfo
{
public:
	CmyProcessInfo();
	~CmyProcessInfo();

	void FreshProcessInfo();
	void FreshModuesInfo(DWORD pid);
	void FreshThreadsInfo();
	void FreshHeapsInfo(DWORD pid);
public:
	//Data
	vector<PROCESSENTRY32>									 m_Processes;				
	vector<pair<DWORD/*pid*/, vector<THREADENTRY32>>>		 m_Threads;
	vector<pair<DWORD/*pid*/, vector<MODULEENTRY32>>>		 m_Modues;
	vector<pair<DWORD/*pid*/, vector<myPROCESSHEAP32>>>		 m_Heaps;
};

