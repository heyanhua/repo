/**********************************************************************************************//**
 * @file	源.cpp
 *
 * @brief	Implements the  class.
 **************************************************************************************************/

#include <thread>
#include <iostream>
#include <windows.h>

/**********************************************************************************************//**
 * @fn	bool lock()
 *
 * @brief	Locks this object.
 *
 * @author	Win 7
 * @date	2016/3/24
 *
 * @return	true if it locked, false if it unlock.
 **************************************************************************************************/

bool lock()
{
	static  unsigned long long  number = 0;
	InterlockedIncrement(&number);
	return InterlockedOr(&number, 0);		
}

/**********************************************************************************************//**
 * @fn	void fun()
 *
 * @brief	Funs this object.
 *
 * @author	Win 7
 * @date	2016/3/24
 **************************************************************************************************/

void fun()
{

	for (int i = 0; i < 10000; i++)
	{
		//一直等待获取解锁
		while (true)
		{
			std::cout << "未解锁在等待";
			if (lock())
			{
				std::cout << "已经解锁";
				break;
			}
		}
	}


	return;
	//执行xxx
	//
}

/**********************************************************************************************//**
 * @fn	int main()
 *
 * @brief	Main entry-point for this application.
 *
 * @author	Win 7
 * @date	2016/3/24
 *
 * @return	Exit-code for the process - 0 for success, else an error code.
 **************************************************************************************************/

int  main()
{
	std::thread  t(fun);
	std::thread  t2(fun);
	t.join();
	t2.join();

	system("pause");
}