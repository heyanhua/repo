/**********************************************************************************************//**
 * @file	源.cpp
 *
 * @brief	Implements the  class.
 **************************************************************************************************/

/**********************************************************************************************//**
 * @struct	st
 *
 * @brief	A st.
 *
 * @author	Win 7
 * @date	2016/3/24
 **************************************************************************************************/

struct st
{
	/** @brief	The x coordinate. */
	int x;	
	/** @brief	The y coordinate. */
	int y;	
	/** @brief	The z coordinate. */
	int z;

};



#include <thread>
#include <mutex> //标准库的互斥量
#include <stack>


/** @brief	The mtx. */
std::mutex  g_mtx;

/**********************************************************************************************//**
 * @fn	int main()
 *
 * @brief	Main entry-point for this application.
 *
 * @author	Win 7
 * @date	2016/3/24
 *
 * @return	Exit-code for the process - 0 for success, else an error code.
 **************************************************************************************************/

int main()
{
	//int  i = 0;
	//st  obj= {1,2,4};
	//int const & ref = i;
	//st  const & refobj = obj;

	g_mtx.unlock();

	//自动打开  	g_mtx.unlock();
	std::lock_guard<std::mutex>  lg(g_mtx);
	
	
	std::stack<int>  stk;
	//在 lg析构中调用  g_mtx.lock();
}