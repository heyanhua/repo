/**********************************************************************************************//**
 * @file	源.cpp
 *
 * @brief	Implements the  class.
 **************************************************************************************************/

#include <iostream>
using std::cout;
#include <thread>
using std::thread;


#include <functional>
using std::hash;

/**********************************************************************************************//**
 * @fn	void main()
 *
 * @brief	Main entry-point for this application.
 *
 * @author	Win 7
 * @date	2016/3/24
 **************************************************************************************************/


void hello()
{
	std::cout << "hello, c++ 并发\n";
	system("pause");
}



void main()
{
	hash<std::thread::id>  hash_thread;
	
	//
	cout<<"当前线程id"<< std::this_thread::get_id();
//	thread t(hello);
//	t.join(); //等待t 结束
//	t.detach(); //不等待t 结束
//	t.joinable();

//	t.get_id();
	cout<<"当前机器合理的并发数量为"<< std::thread::hardware_concurrency();
	system("pause");
}