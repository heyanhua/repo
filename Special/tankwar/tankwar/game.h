#pragma once



//用户界面类，简单封装一些操作

#include <windows.h>
#include <iostream>
#include <conio.h>   
#include <vector>
#include "engine.h"
using std::vector;

class game:public engine
{
public:
	game(void);
	~game(void);
	

	int select_cos();
	int select_player();
	
private:
	int SetTheWindow();
	int Welcome();
};

