#include "stdafx.h"
#include "AStar.h"
#include "Tank.h"



AStar::AStar(SHORT shWidth, SHORT shHigh, int(*pSpace)[40])
{
	m_MaxX = shWidth;                                          //地图的宽度
	m_MaxY = shHigh;                                           //地图的高度
	m_stcStart.G = m_MaxX*m_MaxY;
	memset(m_pSpace, 0, m_MaxX*m_MaxY*4);                       //对容器进行初始化为0的操作
	memcpy_s(m_pSpace, m_MaxX*m_MaxY*4, pSpace, m_MaxX*m_MaxY*4);//进行内存拷贝

}



AStar::~AStar()
{
	m_vec_Open.clear();
}

bool AStar::FindInOpen(COORD stcPos)                                  //判断节点是否在open表中
{
	if (!m_vec_Open.empty())
	{
		for (int i = 0; i < m_vec_Open.size(); i++)
		{
			if ((stcPos.X == m_vec_Open[i].stc_Nowvious.X) && (stcPos.Y == m_vec_Open[i].stc_Nowvious.Y))//判断传入的点是否在open表中
			{
				return true;
			}
		}
	}
	return false;
}

void AStar::UpdateFourPoin(START_NODE stcMinFNode)                //获取四个点的信息
{    //定义用于存储扩散点的节点数组
	START_NODE stc_Nodes[4] = {};
	for (int i = 0; i < 4; i++)
	{//对每个节点赋值为传入的节点的坐标
		stc_Nodes[i].stc_Nowvious.X = stcMinFNode.stc_Nowvious.X;
		stc_Nodes[i].stc_Nowvious.Y = stcMinFNode.stc_Nowvious.Y;
		stc_Nodes[i].stc_Previous.X = stcMinFNode.stc_Nowvious.X;
		stc_Nodes[i].stc_Previous.Y = stcMinFNode.stc_Nowvious.Y;
	}
	stc_Nodes[0].stc_Nowvious.X++;//向右扩散
	stc_Nodes[1].stc_Nowvious.X--;//向左扩散
	stc_Nodes[2].stc_Nowvious.Y++;//向下扩散
	stc_Nodes[3].stc_Nowvious.Y--;//向上扩散
	for (int i = 0; i < 4; i++)
	{
		if (stc_Nodes[i].stc_Nowvious.X == 1 || stc_Nodes[i].stc_Nowvious.Y == 1 || stc_Nodes[i].stc_Nowvious.Y == 38 || stc_Nodes[i].stc_Nowvious.X == 38)
		{//如果判断扩散出的点是否是地图边界
			continue;
		}
		SHORT X = stc_Nodes[i].stc_Nowvious.X;
		SHORT Y = stc_Nodes[i].stc_Nowvious.Y;
		int flag = 0;
		//判断该点是否是地图上的墙
		for (int k = Y - 1; k <= Y + 1; k++)
		{
			for (int t = X - 1; t <= X + 1; t++)
			{
				if (m_pSpace[t][k]>0 && m_pSpace[t][k]!=6)
				{
					flag++;
				}
			}
			
		}
		if (flag)
			continue;
		//G移动一次G+1
		stc_Nodes[i].G = stcMinFNode.G-2;
		//H计算
		stc_Nodes[i].H = abs(stc_Nodes[i].stc_Nowvious.X - m_stcEnd.stc_Nowvious.X) + abs(stc_Nodes[i].stc_Nowvious.Y - m_stcEnd.stc_Nowvious.Y);
		//计算当前节点的F值
		stc_Nodes[i].F = stc_Nodes[i].G + stc_Nodes[i].H;

		if (!FindInOpen(stc_Nodes[i].stc_Nowvious) && !FindIncolse(stc_Nodes[i].stc_Nowvious))
		{
			m_vec_Open.push_back(stc_Nodes[i]);
		}
		

	}



}
bool AStar::GetPathNode(COORD stcStart, COORD stcEnd, COORD& NowMove)             //获取路径中的每个点
{
	vector<START_NODE> Path;
	m_stcStart.stc_Nowvious = stcStart;
	m_stcEnd.stc_Nowvious   = stcEnd; 
	//将起点H（X轴与Y轴的差之和）
	m_stcStart.H = abs(m_stcStart.stc_Nowvious.X - m_stcEnd.stc_Nowvious.X) + abs(m_stcStart.stc_Nowvious.Y - m_stcEnd.stc_Nowvious.Y);
	//F是起点具终点的距离总和来判断那个路径为最短
	m_stcStart.F = m_stcStart.G + m_stcStart.H;
	//将起点放入open表中
	m_vec_Open.push_back(m_stcStart);
	while (!m_vec_Open.empty())
	{   // 用一个变量来获取open表中的最小值
		START_NODE stc_Min_Node;
		//获取open表最小的值也就是F最小的值
		GetOpenTable_F_Min(stc_Min_Node);
		//WriteChar(stc_Min_Node.stc_Previous.Y, stc_Min_Node.stc_Previous.X, "*", FOREGROUND_BLUE);//测试使用
		//Sleep(500);
		//将获取的点存入close表，这个要放在判断终点之前，否则不会将最后一个点存入close表
		m_vec_Close.push_back(stc_Min_Node);
		//printf("%d,%d", stc_Min_Node.stc_Nowvious.X, stc_Min_Node.stc_Nowvious.Y);
		//判断刚从open表中获得点是否为寻路终点
		if ((stc_Min_Node.stc_Nowvious.X == m_stcEnd.stc_Nowvious.X) && (stc_Min_Node.stc_Nowvious.Y == m_stcEnd.stc_Nowvious.Y))
		{//如果缺定该点就是要寻路的终点时，返回。
			break;
		}
		//再判断所得到的点不是终点时，再次将这个点作为下次扩散的中心点，向外扩散
		UpdateFourPoin(stc_Min_Node); 
	if (m_vec_Close.size() >100)
			break;
	}
	//在对open表进行判断，如果此时该表为空则表明路径不同，会来回将它删空
	if (m_vec_Open.empty())//！！！！！ empty为空返回真，不为空返回假
	{
		return false;
	}
	//再对上述循环完结，表明以寻路到终，但close表中的节点并不全是要走的节点，所以对close表进行遍历，找到起点到终点的路径
	int i = m_vec_Close.size() - 1;
	for (int j = i; j > 0; j--)
	{
		if ((m_vec_Close[i].stc_Previous.X == m_vec_Close[j].stc_Nowvious.X) && (m_vec_Close[i].stc_Previous.Y == m_vec_Close[j].stc_Nowvious.Y))
		{//倒序遍历close表，用i标记正确的节点，然后每次遍历对每次下表为i的元素的上一次坐标判断，如果与当前遍历的到的某个元素的当前节点的坐标相等，
			//就说明是从当前节点移动过来的就将当前节点放入传入的容器中
			//printf("%d,%d",m_vec_Close[j].stc_Nowvious.X, m_vec_Close[j].stc_Nowvious.Y);
			Path.push_back(m_vec_Close[j]);
			i = j;
			//printf("\n%d,%d\n", m_vec_Close[j].stc_Nowvious.X, m_vec_Close[j].stc_Nowvious.Y);
		}

	}
	int counts = 0;
	if (!Path.empty())
	{
		 counts = Path.size() - 1;
		 NowMove = Path[counts].stc_Nowvious;
	}
	m_vec_Close.clear();
	m_vec_Open.clear();
	Path.clear();
	return true;
}
bool AStar::GetOpenTable_F_Min(START_NODE&stcNode)                //获取open表中F的最小值
{ //定义一个f存储F的标示
	int Temp_Min_F = m_vec_Open[0].F;
	//储存最小F值所在下标
	int Min_F_Index = 0;
	//遍历open表找到最小的F值
	for (int i = 1; i < m_vec_Open.size(); i++)
	{
		if (Temp_Min_F >=m_vec_Open[i].F)
		{//如果Temp_Min_F的值大于，就存起来下次比较，
			Temp_Min_F = m_vec_Open[i].F;
			//储存最小值下标
			Min_F_Index = i;
		}
	}
	//将找到的最小值传给参数，返回使用
	stcNode = m_vec_Open[Min_F_Index];
	//删除该元素
	m_vec_Open.erase(m_vec_Open.begin() + Min_F_Index);
	return true;
}
bool AStar::FindIncolse(COORD stcPos)                                 //所给的点是否在CLOse表中
{
	if (!m_vec_Close.empty())
	{
		for (int i = 0; i < m_vec_Close.size(); i++)
		{
			if ((stcPos.X == m_vec_Close[i].stc_Nowvious.X) && (stcPos.Y == m_vec_Close[i].stc_Nowvious.Y))//判断传入的点是否在close表中
				return true;
		}
	}
	return false;
}