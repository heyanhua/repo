#pragma once
#include <vector>
#include <map>
#include <fstream>   // need  std::ifstream



//数据类,所有的数据存储在这里





#include "tank.h"
#include "bullet.h"
#include "AStar.h"

using std::vector;
using std::map;
using std::pair;







struct _map_info
{
public:
    static int    MAP[40][40];         //初始地图
	static int    MAP2[40][40];       
	bool initMAP(char * filename) { 	//读取地图
	std::ifstream ifs(filename, std::ifstream::binary);
	ifs.read((char *)MAP, sizeof(int)* 40 * 40);
	ifs.close();   return true;
	};


	bool saveMAP(char * filename){ 
	std::ofstream ofs(filename,std::ofstream::binary);
	ofs.write((char *)MAP,sizeof(int )* 40* 40);
	ofs.close();   return true;
	};

};



class data: public _map_info
{
public:
//地图结构


public:
	data();
	~data(void);

public:
	static char * bitShape[7];  //绘制的图形
	static int Tankshape[4][3][3]; //坦克形状
	static MAP_pt DATA[40][40];    //复合地图


	
	int    player1ID;
	int    player2ID;
	
	//key tank.id    pair< tank,  子弹>
	//所有的tank,和属于它的所有子弹
	

	//  坦克id    坦克数据  子弹id  子弹数据
	vector<tank>	vtanker;
	vector<bullet>  vbullet;
	tank			Astar_tank;

	void MAP2DATA(int type=0); //普通地图转换为复合地图
	void DATA2MAP();

};

