#pragma once

//---------移动物体方向
#define   DRIECTION_UP     0
#define   DRIECTION_LEFT     2
#define   DRIECTION_DOWN     1
#define   DRIECTION_RIGHT     3


//游戏界面返回值
#define  ONEPLAYER_FIRST      0
#define  ONEPLAYER_SECOND     1
#define  TWOPLAYER_FIRST      2
#define  TWOPLAYER_SECOND     3

//全局id起始位置
#define  ID_BEGIN             100

//调试键宏
#define  _DEBUG_RUN			 0
#define  _DEBUG_ACTIVET_ID   100  //地图id 减去 _DEBUG_ACTIVET_ID，在屏幕上打印
#define  _DEBUG_DRAW         2


// 字体颜色
#define F_BLUE     FOREGROUND_BLUE      // 深蓝
#define F_H_BLUE   0x0001|0x0008        // 亮蓝
#define F_GREEN    0x0002               // 深绿
#define F_H_GREEN  0x0002|0x0008        // 亮绿
#define F_RED      0x0004               // 深红
#define F_H_RED    0x0004|0x0008        // 亮红
#define F_YELLOW   0x0002|0x0004        // 深黄
#define F_H_YELLOW 0x0002|0x0004|0x0008 // 亮黄
#define F_PURPLE   0x0001|0x0004        // 深紫
#define F_H_PURPLE 0x0001|0x0004|0x0008 // 亮紫
#define F_CYAN     0x0002|0x0004        // 深青
#define F_H_CYAN   0x0002|0x0004|0x0008 // 亮青
#define F_WHITE    0x0004|0x0002|0x0001
#define F_H_WHITE  0x0004|0x0002|0x0001|0x0008
#define F_TEST     0x0007|0x000b|0x0003

// 背景颜色
#define B_BLUE     BACKGROUND_BLUE      // 深蓝
#define B_H_BLUE   0x0010|0x0080        // 亮蓝
#define B_GREEN    0x0020               // 深绿
#define B_H_GREEN  0x0020|0x0080        // 亮绿
#define B_RED      0x0040               // 深红
#define B_H_RED    0x0040|0x0080        // 亮红
#define B_YELLOW   0x0020|0x0040        // 深黄
#define B_H_YELLOW 0x0020|0x0040|0x0080 // 亮黄
#define B_PURPLE   0x0010|0x0040        // 深紫
#define B_H_PURPLE 0x0010|0x0040|0x0080 // 亮紫
#define B_CYAN     0x0020|0x0040        // 深青
#define B_H_CYAN   0x0020|0x0040|0x0080 // 亮青
#define B_WHITE    0x0010|0x0020|0x0040
#define B_H_WHITE  0x0002|0x0002



//子弹移动的返回在值， 用于执行后续数据处理操作
#define BULLET_MOVED				 0			   //正常移动
#define BULLET_MOVE_RET_FREAM         1            //遇到边框
#define BULLET_HITED_WALL             2            //伤害墙体
#define BULLET_HITED_FRIEND           9
#define BULLET_HITED                  10           //tankid + 10



//具体绘制内容存储在字符串数组  bitShape中
#define  BIT_NULL					0   
#define  BIT_WATER				    1   //水，  可覆盖
#define  BIT_GRASS				    2   //草地， 可隐藏
#define  BIT_WALL                   3   //1点生命的墙
#define  WALL_DIA					4   //无限生命的墙
#define  TANK_NULL                  5   //坦克空白
#define  TANK_POINT		 			6   //坦克实心
#define  BULLET                     7   //子弹

#define  BIT_BULLET                 10
#define  BIT_TANK                   11


//地图上的每个点
typedef  struct _str_point
{
	unsigned int bit :4; //地图图像类型
	unsigned int unrun :1; // 为1则不能通行  移动中会不断修改这个值，  进入修改为1  ， 离开修改为0
	unsigned int undraw :1; //为1则能隐藏 --可以通行但是不用画自己
	unsigned int alive_id :26; //有生命的物体id
}MAP_pt;