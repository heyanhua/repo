#include "stdafx.h"
#include "engine.h"
#include <windows.h>
#include <iostream>
#include <conio.h>
#include <time.h>
#include <algorithm>

using namespace std;
//-------------------前置声明
int mrand(int max);


//-------------------AI总数，     最大值
engine::engine(void):AI_Count(0),AI_Max(3)
{
}


engine::~engine(void)
{
}

//登记指针，以便于将来回收
int engine::regist_pointer(char * pointer)
{
	m_pointers.insert(std::pair<int, char  * > (id_index, pointer));
	return id_index++;
}

//回收废弃的对象
void engine::garbage(int id)
{
	auto iter = m_pointers.find(id);
	if(iter != m_pointers.end())
	{
		m_garbages.push_back(iter->second);
		m_pointers.erase(id);
	}

}

//统一管理废弃的指针
void engine::delete_garbaged()
{	
	while(m_garbages.empty())
	{
		delete *(m_garbages.end());
		m_garbages.pop_back();
	}
}


//单人模式
void engine::oneplayer(void)
{
	tank tk;
	tk.m_id = getID();
	tk.m_x = 20;
	tk.m_y = 37;
	tk.m_blood = 1;
	tk.m_isDead = 0;
	tk.m_direction = 0;
	tk.m_count = 0;

	Inster_Tank(tk);
	player1ID =  tk.m_id;
	player2ID =  tk.m_id;


}

//双人游戏
void engine::twoplayer(void)
{
	tank tk1,tk2;
	tk1.m_id = getID();
	tk1.m_x =  13;
	tk1.m_y =  37;
	tk1.m_blood = 1;

	//插入玩家进入容器
	Inster_Tank(tk1);
	player1ID =  tk1.m_id;
	//-------------------tk1.
	tk2.m_id = getID();
	tk2.m_x = 26;
	tk2.m_y = 37;
	tk2.m_blood = 1;

	Inster_Tank(tk2);
	player2ID =  tk2.m_id;

}
	//根据id绘制坦克，默认绘制，可首选第二个参数为true，执行擦除
	void  engine::Draw(int tank_id, bool erase)
	{
		for(auto & vt: vtanker)
			if(vt.m_id == tank_id)
				Draw(vt,erase);
	}


//绘制坦克
void engine::Draw(tank & tk, bool erase)
{
	if (!erase)
	{
		for(int i=0;i< 3;i++)	
		for(int j=0;j<  3;j++)
		{ 
			//判断是否处于隐藏状态 ----不处于隐藏状态才绘制
			if(!DATA[tk.m_y+i][tk.m_x+ j ].undraw)
			 WriteCharShape(tk.m_x+j ,tk.m_y+ i , Tankshape[tk.m_direction][i][j] + TANK_NULL /*绘制时加5*/, 0xF0 );	
				
		}

	}
	else
	{
		//擦除
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				if (true)
				{
					//画出原来的	
					WriteCharShape(tk.m_x+j ,tk.m_y+ i , DATA[tk.m_y+ i][tk.m_x+ j].bit,0xF0);
					//std::cout<<"2";
					//死亡擦除， 重置地图标志
					if(tk.m_isDead)
					{
					//清理活动id
					DATA[tk.m_y+i][tk.m_x+j].alive_id = 0;
					//标记为可进入
					DATA[tk.m_y+i][tk.m_x+j].unrun = 0;
					}
				}
			}
		}

	}
}


//移动坦克
void engine::MoveTank(tank & tk, int driction)
{
		if(tk.m_isDead)
		return ;
	//擦除
	Draw(tk,true);
	//移动
	//处理AI转向
	if(!Move(tk,driction))
	{
		if(tk.m_id>player2ID)
			tk.m_direction = mrand(4);
	}

	//重画
	Draw(tk,false);
}
  


bool engine::Move(tank & tk, int dr)
{

	//判断转向
	if(dr == -1)
		dr = tk.m_direction;

	if(dr!=tk.m_direction )
	{
		tk.m_direction = dr;
		return true;
	}



	//清空上一步骤不可通行标志
	for(int i=0; i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
		
			DATA[tk.m_y + i][tk.m_x + j].unrun = 0;
			//清理活动id
			DATA[tk.m_y + i ][tk.m_x+ j].alive_id = 0;

		}
	}

	//备份数据
	int  backup_x = tk.m_x;
	int  backup_y = tk.m_y;

	switch (tk.m_direction)
	{
	case 0:
		tk.m_y--;
		break;
	case 1:
		tk.m_y++;
		break;
	case 2:
		tk.m_x--;
		break;
	case 3:
		tk.m_x++;
		break;
	default:
		break;
	}

	//判断边框
	bool  cond_run=true;
	if(tk.m_x >37 || tk.m_y >37  || tk.m_x<0 || tk.m_y <0)
	{
		tk.m_x = backup_x;  tk.m_y = backup_y;
		cond_run = false;
	}


	//判断通行
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>        离开某位置， 清空不可通行标志

	//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

	if(cond_run)
	for(int i=0; i<3;i++)
	{
		for(int j=0;j<3;j++)
			if(DATA[tk.m_y + i][tk.m_x + j].unrun)
			{

				// 3*3 只要有一个点不可通行， 则为不可通行
					cond_run = false;
					break;
			}
			//if(cond_run)
			//	break;
	}

		//判断坦克

	//不可通行 --恢复数据
	if(!cond_run)
	{
		tk.m_x = backup_x;  tk.m_y = backup_y;
	}



	//设置不可通行
	for(int i=0; i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			DATA[tk.m_y + i][tk.m_x + j].unrun = 1;
			//id
			DATA[tk.m_y+i][tk.m_x+j].alive_id = tk.m_id;
		}
	}

	return cond_run;
}

//移动子弹
int engine::MoveBullet(bullet & bt, bool isCreat)
{
	if(bt.m_isDead)
		return false;
	int ret = Move(bt,isCreat);
	//伤害，  计分    死亡标记， 
	if( BULLET_HITED <= ret)
	{

		//被伤害tank重新绘制
		Draw(bt.hit_tank_id ,true);
		for(auto &vt:vtanker)
		{
	
			if(vt.m_id == bt.hit_tank_id)
			{   

 				vt.m_blood-- ;
				if( vt.m_blood == 0)
				{
					//死亡，删除
					vt.m_isDead = true;
					Draw(vt,true);
					Delete_Tank(vt.m_id);


					if(isAI(vt.m_id))
					{
						AI_Count --;
						for(auto &vt_mine: vtanker)
						{
							//敌方死亡，  我方没死  计分
							if(vt_mine.m_id==bt.tank_id && vt_mine.m_isDead != true)
							{
								vt_mine.m_count++;
								if(!vt_mine.m_isDead)
								ShowCount( vt_mine.m_id);
								break;
							}
						}
						break;

					}

				}

				if(!isAI(vt.m_id) && vt.m_blood == 0)
				{
						vt.m_isDead = true;
						Draw(vt,true);
						//玩家死亡
						if(player1ID == vt.m_id )
						{
							SetCoutPos( 5,  41);
							cout<<"player1结束了,得分:"<<vtanker[0].m_count<<" 血量:0         ";
							break ;
						}
						if(player2ID== vt.m_id )
						{
							SetCoutPos( 22,  41);
							cout<<"player2结束了,得分:"<<vtanker[1].m_count<<" 血量:0";
							break;
						}
						if(player2ID== player1ID)
						{
							SetCoutPos( 3,  41);
							cout<<"您的游戏结束了,得分:"<<vtanker[0].m_count<<" 血量:0";
							break;
						}
				}
			}

		}


	}


	//遇到边框
	if( BULLET_MOVE_RET_FREAM == ret  ||   BULLET_HITED_WALL == ret  )
	{
  			bt.m_isDead = true;								
	}

	if( BULLET_HITED_FRIEND == ret)
	{
		Draw(bt.hit_tank_id ,false);
		bt.m_isDead = true;		
	}
	return  ret;
}



//子弹不用改变地图状态，  不用设置不可通行，   进行特殊不可通行判断
int engine::Move(bullet & bt, bool isCreat)
{

	//擦除旧的子弹
	if(!isCreat)
 	WriteChar(bt.m_x,bt.m_y,bitShape[DATA[bt.m_y][bt.m_x].bit],0xF0);
	//子弹的新坐标
	switch (bt.m_direction)
	{
	case 0:
		bt.m_y-=1;
		break;
	case 1:
		bt.m_y+=1;
		break;
	case 2:
		bt.m_x-=1;
		break;
	case 3:
		bt.m_x+=1;
		break;
	default:
		break;
	}
	//判断边框
	if(bt.m_x >39 || bt.m_y >39  || bt.m_x<0 || bt.m_y < 0)
		return  BULLET_MOVE_RET_FREAM;

	//判断伤害
	if(DATA[bt.m_y][bt.m_x].alive_id)
	{
		//敌军友方
		if((DATA[bt.m_y][bt.m_x].alive_id  <= (unsigned int)player2ID) &&  (bt.tank_id <=  (unsigned int)player2ID))
		{
			bt.hit_tank_id = DATA[bt.m_y][bt.m_x].alive_id;
			return   BULLET_HITED_FRIEND;
		}
		//敌方
		if(  (DATA[bt.m_y][bt.m_x].alive_id  > (unsigned int)player2ID) &&  (bt.tank_id >   (unsigned int)player2ID))
		{
			bt.hit_tank_id = DATA[bt.m_y][bt.m_x].alive_id;
			return   BULLET_HITED_FRIEND;
		}
		   bt.hit_tank_id = DATA[bt.m_y][bt.m_x].alive_id;
		return   BULLET_HITED + DATA[bt.m_y][bt.m_x].alive_id ;
	}


		//是否隐藏通行
	if(DATA[bt.m_y][bt.m_x].undraw)
	{
		return BULLET_MOVED;
	}

	//伤害墙体
	if(DATA[bt.m_y][bt.m_x].bit == BIT_WALL)
	{
		
		DATA[bt.m_y][bt.m_x].bit = BIT_NULL; 
		DATA[bt.m_y][bt.m_x].unrun = 0;
		DATA[bt.m_y][bt.m_x].undraw = 0;
		DATA[bt.m_y][bt.m_x].alive_id = 0;
		WriteCharShape(bt.m_x,bt.m_y,BIT_NULL, 0XF0);
		return BULLET_HITED_WALL;
	}
	//被墙体阻挡
	if(DATA[bt.m_y][bt.m_x].bit == WALL_DIA)
	{
		return BULLET_MOVE_RET_FREAM;
	}
	//过草地
	if(DATA[bt.m_y][bt.m_x].bit == BIT_GRASS)
	{

		return BULLET_MOVED;
	}

	//过水坑
	if(DATA[bt.m_y][bt.m_x].bit == BIT_WATER)
	{
 		WriteCharShape(bt.m_x,bt.m_y,BULLET, 0XF0);
		return BULLET_MOVED;
	}

	 WriteCharShape(bt.m_x,bt.m_y,BULLET, 0XF0);
	return BULLET_MOVED;
}
	


//函数功能：指定位置打印字符
//参数：wide，high->打印位置
//pszchar->打印内容
//wArr->打印字符属性
template<typename T>
void engine::WriteChar(int Wide,int High,T pszChar,WORD wArr)
{
	CONSOLE_CURSOR_INFO cci;
	cci.dwSize=1;
	cci.bVisible=FALSE;//是否显示光标
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE),&cci);
	COORD loc;
	loc.X=Wide*2;
	loc.Y=High;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),wArr);
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),loc);
	std::wcout<<pszChar;
}


//绘制图形
void engine::WriteCharShape(int i,int j,int shape_bit,WORD wArr)
{
	switch (shape_bit)
	{
	case BIT_NULL:
		WriteChar(i,j,"  ",wArr);
		break;
	case BIT_WATER:
		WriteChar(i,j,"≈",wArr);
		break;
	case BIT_GRASS:
		WriteChar(i,j,"∷",wArr);
		break;
	case BIT_WALL:
		WriteChar(i,j,"〓",wArr);
		break;
	case WALL_DIA:
		WriteChar(i,j,"▓",wArr);
		break;
	case TANK_NULL:
		WriteChar(i,j," ",wArr);
		break;
	case TANK_POINT:
		WriteChar(i,j,"▇",wArr);
		break;
	case BULLET:
		WriteChar(i,j,"●",wArr);
	default:
		break;
	}
}


//调试功能支持
void engine:: _debug_DrawMap(int TYPE)
{
	for (int i = 0; i < 40; i++)
	{
		for (int j = 0; j < 40; j++)
		{
			if(_DEBUG_RUN == TYPE)
			{
				if(DATA[j][i].unrun)
				{
					SetCoutPos(i,j);
					cout<<1;
				}
			}

			if(_DEBUG_DRAW == TYPE)
			{
				if(DATA[j][i].undraw)
				{
					SetCoutPos(i,j);
					cout<<1;
				}
			}

			if(_DEBUG_ACTIVET_ID == TYPE)
			{
				if(DATA[j][i].alive_id)
				{
					SetCoutPos(i,j);
					cout<<DATA[j][i].alive_id - _DEBUG_ACTIVET_ID;
				}
			}
		}
	}

	SetCoutPos(0,40);
	if(_DEBUG_RUN == TYPE)
		cout<<std::endl<<"_DEBUG_RUN";
	if(_DEBUG_DRAW == TYPE)
		cout<<std::endl<<"_DEBUG_DRAW";
	if(_DEBUG_ACTIVET_ID == TYPE)
		cout<<std::endl<<"_DEBUG_ACTIVET_ID";
	system("pause");
	DrawMap();

}









bool engine::DrawMap()
{
	for (int i = 0; i < 40; i++)
	{
		for (int j = 0; j < 40; j++)
		{
			
			WriteCharShape(i,j,DATA[j][i].bit,0xF0);
			
		}
	}

			for (auto &vt : vtanker)
				Draw(vt);

		SetCoutPos(0,40);
	cout<<".................................................................................";
	ShowCount(player1ID);
	ShowCount(player2ID);
	return true;
}


//按键定义
#define KEYDOWN(vk_code) ((GetAsyncKeyState(vk_code)&0x8000)?1:0)
#define KEYUP(vk_code) ((GetAsyncKeyState(vk_code)&0x8000)?1:0)


//按键判断
bool engine::KeyJudge()
{

	//玩家1
	if(KEYDOWN('W')||KEYDOWN('w'))//上
	{
		if(!vtanker[player1ID-ID_BEGIN].m_isDead)
		MoveTank(vtanker[player1ID-ID_BEGIN],0);
		
	}
	if(KEYDOWN('A')||KEYDOWN('a'))//左
	{
		if(!vtanker[player1ID-ID_BEGIN].m_isDead)
		MoveTank(vtanker[player1ID-ID_BEGIN],2);

	}
	if(KEYDOWN('S')||KEYDOWN('s'))//下
	{
		if(!vtanker[player1ID-ID_BEGIN].m_isDead)
		MoveTank( vtanker[player1ID-ID_BEGIN],1);

	}
	if(KEYDOWN('D')||KEYDOWN('d'))//右
	{
		if(!vtanker[player1ID-ID_BEGIN].m_isDead)
		MoveTank( vtanker[player1ID-ID_BEGIN],3);

	}
	if(KEYDOWN('p')||KEYDOWN('P'))//暂停
	{
				
		_getch();
	}
	if (KEYDOWN(VK_SPACE))//子弹
	{
		if(!vtanker[player1ID-ID_BEGIN].m_isDead)
         fire(vtanker[player1ID-ID_BEGIN]);	
	}
	if (KEYDOWN(VK_ESCAPE))//退出
	{
		return false;
	}
	//玩家2
	if(KEYDOWN(VK_UP))//上
	{
		if(!vtanker[player2ID-ID_BEGIN].m_isDead)
		MoveTank( vtanker[player2ID-ID_BEGIN],0);
	}
	if(KEYDOWN(VK_LEFT))//左
	{
		if(!vtanker[player2ID-ID_BEGIN].m_isDead)
		MoveTank( vtanker[player2ID-ID_BEGIN],2);
	}
	if(KEYDOWN(VK_DOWN))//下
	{
		if(!vtanker[player2ID-ID_BEGIN].m_isDead)
		MoveTank( vtanker[player2ID-ID_BEGIN],1);
	}
	if(KEYDOWN(VK_RIGHT))//右
	{
		if(!vtanker[player2ID-ID_BEGIN].m_isDead)
		MoveTank( vtanker[player2ID-ID_BEGIN],3);
	}
	if(KEYDOWN('K')||KEYDOWN('k'))//子弹
	{
		if(!vtanker[player2ID-ID_BEGIN].m_isDead)
		 fire(vtanker[player2ID-ID_BEGIN]);			
	}

#ifdef _DEBUG  // ------------------调试功能  查看40 * 40 地图位信息
		//调试键 通行标志
	if(KEYDOWN('L')||KEYDOWN('l'))
	{
		 _debug_DrawMap(_DEBUG_RUN);
	}
		//调试键  活动ID
	if(KEYDOWN('J')||KEYDOWN('j'))
	{
		_debug_DrawMap(_DEBUG_ACTIVET_ID);
	}
		//调试键  隐藏标识
	if(KEYDOWN('H')||KEYDOWN('h'))
	{
		_debug_DrawMap(_DEBUG_DRAW);	
	}
		//调试键 静态全图
	if(KEYDOWN('I')||KEYDOWN('i'))
	{
		DrawMap();	
	}

	if(KEYDOWN('V')||KEYDOWN('v'))//上
	{
		CreatAI();
		
	}

#endif
	return true;
}



//插入到坦克容器
void engine::Inster_Tank(tank & tk)
{
	//判断是否重复插入

	bool cond=true;
	for(auto & vt :vtanker)
	{
		if(vt.m_id==tk.m_id)
			cond = false;
	}
	if(cond)
		vtanker.push_back(tk);

}

//插入到子弹容器
void engine::Inster_Bullet(bullet & bt)
{
	vbullet.push_back(bt);
}

//删除掉坦克， 如果是AI
void engine::Delete_Tank(int tank_id)
{
	if(isAI(tank_id))
	for( auto iter = vtanker.begin(); iter<vtanker.end();iter++)
	{
		if(iter->m_id == tank_id)
		{
			vtanker.erase(iter);
			return ;
		}
	}
	


}
//将炮弹标记为死亡
void engine::Delete_Bullet(int bullet_id)
{
	bool cond=true;
	for(auto & vb :vbullet)
	{
		if(vb.m_id==bullet_id)
		{
			vb.m_isDead = true;
			return ;
		}
	}
}


//获取id，取值范围ID_BEGIN~int最大值， 顺序增长
int  engine::getID(int ID)
{
	static int id=ID_BEGIN;
	if(id>=ID && ID != 0)
		return ID;
	return  id++;
}

//发射炮弹
void  engine::fire(tank &tk)
{
	
	bullet  bt;
	bt.m_blood = 1;
	bt.m_x = tk.m_x +1;
	bt.m_y = tk.m_y +1;
	bt.m_direction = tk.m_direction;
	bt.tank_id = tk.m_id;
	switch (bt.m_direction)
	{
	case 0:
		bt.m_y-=1;
		break;
	case 1:
		bt.m_y+=1;
		break;
	case 2:
		bt.m_x-=1;
		break;
	case 3:
		bt.m_x+=1;
		break;
	default:
		break;
	}

	int back_x = bt.m_x,  back_y = bt.m_y;
	if(BULLET_MOVED == MoveBullet(bt,true))
	{
		bt.m_id = getID();
		Inster_Bullet(bt);
	}

}


//AI只有三滴血
void  engine::CreatAI()
{
	//超过AI最大值返回
	if(AI_Count >= AI_Max)
		return ;


	tank tk;
	tk.m_blood = 1;
	//构造个临时tank, 测试能否出生
	for(int i=0; i<3;i++)
	{
		for(int j=0;j<3;j++)
		{
			if(1== DATA[tk.m_y + i][tk.m_x + j].unrun )
				return;
		}
	}
	

	//随机方向
	tk.m_direction = mrand(4);
	AI_Count++;
	tk.m_id = getID();
	Inster_Tank(tk);
	Draw(tk,false);
			//设置不可通行标志  
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   
	for(int i=0; i<3;i++)
		for(int j=0;j<3;j++)
		{
			DATA[tk.m_y + i][tk.m_x + j].unrun = 1;
			DATA[tk.m_y + i][tk.m_x + j].alive_id = tk.m_id;
		}
	//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

}

//当前时间为种子，生产随机数
int mrand(int max)
{
	static unsigned seek = 0;
	srand((unsigned)time(0));
	srand(rand()%max +  seek++);
	return rand() % max;
}




//延时
void sleep(int k)
{
	clock_t now = clock();
	while ((clock() - now) < k);
}



//引擎的主循环
bool  engine::loop()
{
	clock_t tMoveBulletStart = clock(),//子弹移动时间
		tMoveTankStart	 = clock(),//敌方坦克移动时间
		tKeyBegin		 = clock(),//玩家坦克移动时间
		tCreatAI         = clock(),//生产AI
		tAIFire          = clock();//敌方玩家发子弹
	clock_t tMoveBulletEnd,tMoveTankEnd,tKeyEnd, tCreatAIEND, tAIFireEnd;


			srand((unsigned int)time(NULL));
			DrawMap();
	bool cond = true;
	while(cond)
	{
		

		//玩家坦克动
		tKeyEnd = clock();
		if (tKeyEnd - tKeyBegin > 5)
		{	
			tKeyBegin = tKeyEnd;
			if(!KeyJudge())
			return true;
		}
 		//子弹自动移动
 		tMoveBulletEnd = clock();
 		if (tMoveBulletEnd - tMoveBulletStart > 50)
 		{
 			tMoveBulletStart = tMoveBulletEnd;

			
 			Rontine_MoveBullet();
			
 		}

		//坦克自动移动
 		tMoveTankEnd = clock();
 		if (tMoveTankEnd - tMoveTankStart > 100)
 		{
 			tMoveTankStart = tMoveTankEnd;

					
 			Rontine_MoveTank();
		
 		}

		//AI坦克坦克发子弹
 		tAIFireEnd = clock();
 		if (tAIFireEnd - tAIFire > 300)
 		{
 			tAIFire = tAIFireEnd;
			Rontine_AiFire();
 		}


		//生产AI
		 		tCreatAIEND = clock();
		if (tCreatAIEND - tCreatAI > 2000)
 		{
 			tCreatAI = tCreatAIEND;
			CreatAI();
			CleanBullet();
 		}


	}
	return true;
}

	//遍历所有AI，让它们发射， 随机发射, 每次有三分之一的概率发射
  	void  engine::Rontine_AiFire()
	{	
		for (auto &vt : vtanker)
		{
			if(!(player1ID ==  vt.m_id || player2ID == vt.m_id ))
			{
				if(mrand(3))
					fire(vt);
			}
					
		}


	}

	//所有的炮弹移动
	void  engine::Rontine_MoveBullet()
	{
		for(auto &vb:vbullet ) 
			MoveBullet(vb);
	}

	//移动所有Ai坦克
	void  engine::Rontine_MoveTank()
	{	
		static  unsigned int  seek=0;
		for (auto &vt : vtanker)
		{
			if(!(player1ID ==  vt.m_id || player2ID == vt.m_id ))
			{
				//每五步随机
				if((mrand(25) + seek++)%25==0)
				MoveTank(vt,mrand(4));
				else
				MoveTank(vt);
			}
					
		}
	}

	//判断是否AI
	bool  engine::isAI(int tank_id)
	{
		return  tank_id != player1ID && tank_id != player2ID;
	}


	//设置光标位置
	void engine::SetCoutPos(int x, int y)
	{
		COORD loc;
		loc.X=x*2;
		loc.Y=y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),loc);
	}

	//int, player id
	void engine::ShowCount(int player_id)
	{
		if(player1ID == player2ID)
		{
					SetCoutPos( 10,  41);
		cout<<"您的得分:"<<vtanker[0].m_count<<" 血量:"<<vtanker[0].m_blood;
		return ;
		}
		if(player1ID==player_id)
		{
		SetCoutPos( 3,  41);
		cout<<"player1得分:"<<vtanker[0].m_count<<" 血量:"<<vtanker[0].m_blood;
		}

		if(player2ID==player_id)
		{
		SetCoutPos( 22,  41);
		cout<<"player2得分:"<<vtanker[1].m_count<<" 血量:"<<vtanker[1].m_blood;
		}

	}

	//清理坦克
	void engine::CleanBullet()
	{
		for (int i = 0; i < vbullet.size(); i++)
		{
			if (vbullet[i].m_isDead == true)
			{
				vbullet.erase( vbullet.begin()+i);
				i--;
			}
		}
	}

	//初始化玩家
	void  engine::InitPlayer()
	{
		int index_A = player1ID - ID_BEGIN;
		int index_B = player2ID - ID_BEGIN;
	//设置不可通行标志  
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   进入某位置， 清空不可通行标志
	for(int i=0; i<3;i++)
		for(int j=0;j<3;j++)
		{
			DATA[vtanker[index_A].m_y + i][vtanker[index_A].m_x + j].unrun = 1;
			DATA[vtanker[index_A].m_y + i][vtanker[index_A].m_x + j].alive_id= vtanker[index_A].m_id;
			DATA[vtanker[index_B].m_y + i][vtanker[index_B].m_x + j].unrun = 1;
			DATA[vtanker[index_B].m_y + i][vtanker[index_B].m_x + j].alive_id =vtanker[index_B].m_id;
		}
	//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	}