#pragma once

#include <windows.h>
#include <map>
#include <vector>



//引擎，游戏的绝大部分逻辑在此


#include "data.h"
class engine:public data
{
public:
	engine(void);
	~engine(void);
public:
	//选择玩家模式
	void  oneplayer();
	void  twoplayer();
	void  InitPlayer();
	//游戏逻辑相关
	//绘制坦克，根据id绘制
	void  Draw(int tank_id, bool erase);
	//绘制坦克
	void  Draw(tank &,bool erase = false);
	//移动子弹
	int   MoveBullet(bullet & bt, bool isCreat = false);
	//移动坦克
	void  MoveTank(tank &, int driction = -1);
	//发射子弹
	void  fire(tank &tk);
	//创建AI
	void  CreatAI();
	//绘制地图
	bool  DrawMap();
	//循环任务
	bool  loop();
	//按键判断
	bool  KeyJudge();
	
	//时间片任务------------->
	//根据时间移动所有子弹
	void  Rontine_MoveBullet();
	//根据时间移动所有AI
	void  Rontine_MoveTank();
	//根据时间AI发射炮弹
	void  Rontine_AiFire();
	//<----------------------

	//调试支持
	//--------------------->
	void  _debug_DrawMap(int TYPE);
	//<----------------------


	//--------------------

	//指针管理---------------------------------------------------->
	//登记--登记之后会返回一个id
	int  regist_pointer(char * pointer);
	//废弃指针,由具体的对象调用，  表明生存期已到， 可以delete
	void garbage(int id);
	//由engine调用,用于delete掉所有废弃的指针. //可以选择闲时调用,
	//<------------------------------------------------------------
	void delete_garbaged();
private:
	//内部使用  移动炮弹
	int   Move(bullet & bt, bool isCreat = false);
	//移动坦克
	bool  Move(tank &, int driction);


    //默认无参会返回一个新的id
	//可以测试一个id是否被分配，
	//参数为传进去的id，  如果返回值还是这个值，  说明已经存在，
	int  getID(int ID=0);



	//绘制ui相关函数
	template<typename T>
	void WriteChar(int Wide,int High,T pszChar,WORD wArr);
	void WriteCharShape(int i,int j,int shape_bit,WORD wArr);
	void SetCoutPos(int x, int y);
	void ShowCount(int player_id = 100);
	

	//对容器数据的便携封装-----------
	void Inster_Tank(tank & tk);
	void Inster_Bullet(bullet& bt);
	void Delete_Tank(int tank_id);
	void Delete_Bullet(int bullet_id);
	void CleanBullet();
	//-------------------------


	//-------------------指针管理------------------->
	std::map<int ,char *>  m_pointers;
	std::vector<char *>	   m_garbages;  
	int					   id_index;
	//<----------------------------------------------

	//判断是否为AI
	bool  isAI(int tank_id);


	//--------------------
	//当前AI总数
	int  AI_Count;
	//所允许的AI最大值
	int  AI_Max;
	//--------------------

};


