#pragma once
#include "macro_def.h"


//子弹类-简单的数据封装
class bullet
{
public:
	bullet();
	~bullet(void);

public:
	short    m_x;
	short    m_y;
	int		 m_id; 
	int      m_blood;
	int      m_direction;
	int		 tank_id;
	bool     m_isDead;
	int      hit_tank_id;
};

