#pragma once
#include <math.h>
#include <vector>
#include <windows.h>

using std::vector;
class AStar
{
	typedef struct _AStar
	{
		int G;             //开销
		int H;             //距离
		int F;             //G和H之和
		COORD stc_Nowvious;//当前坐标
		COORD stc_Previous;//上一个节点坐标
	}START_NODE, *pSTART_NODE;
public:
	AStar(SHORT shWidth, SHORT shHigh, int(*pSpace)[40]);
	~AStar();
public:
	bool GetPathNode(COORD stcStart, COORD stcEnd, COORD& NowMove);              //获取寻路结果
	void SetSpace(int(*pSpace)[40]);   //设置地图及最大宽度和最大高度
	bool FindIncolse(COORD stcPos);
private:
	bool GetOpenTable_F_Min(START_NODE&stcNode);                //获取open表中F的最小值
	void UpdateFourPoin(START_NODE stcMinFNode);                //获取四个点的信息
	bool FindInOpen(COORD stcPos);                              //判断节点是否在open表中
	

private:
	vector<START_NODE> m_vec_Open;                              //open 表
	vector<START_NODE>m_vec_Close;                              //close 表
	int m_MaxX;                                                 //X轴的最大距离
	int m_MaxY;                                                 //Y轴的最大距离
	START_NODE m_stcStart;                                      //起点坐标
	START_NODE m_stcEnd;                                        //终点坐标
	int        m_pSpace[40][40];                                //地图空间

};

