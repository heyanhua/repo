#pragma once

//基本的坦克数据 
class tank 
{
public:
	tank();
	~tank(void);

public:						
	int      m_x;			 //坐标 x
	int      m_y;			 //坐标 y 
	int		 m_id; 			 //id   统一获取基本不会重复     生成之后不会改变 
	int      m_blood;		 //血值,   当前版本为1
	int      m_direction;	 //方向，  
	bool     m_isDead;		//是否死亡
	int      m_count;		//记分器

};

