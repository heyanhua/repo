#include "stdafx.h"
#include "game.h"


game::game(void)
{
	//设置窗口
	SetTheWindow();
	//欢迎界面
	Welcome();

	//选择玩家
	select_player();
	//选择模式
	select_cos();

	system("cls");


	InitPlayer();
 	loop();

}


game::~game(void)
{
}

int game::SetTheWindow()
{
	//设置标题
	if(!SetConsoleTitle(L"boom!"))
		return GetLastError();
	//设置窗口颜色
	system("color F0");

	//隐藏光标
	CONSOLE_CURSOR_INFO cci;
	cci.dwSize = sizeof(CONSOLE_CURSOR_INFO);
	cci.bVisible = FALSE;	
	SetConsoleCursorInfo( GetStdHandle(STD_OUTPUT_HANDLE),&cci);

	system("Chcp 936");
	system("cls");
	system("mode con cols=80 lines=41");
	COORD BufferSize={80 + 1,41 + 1};
	//设置控制台缓冲区大小
	SetConsoleScreenBufferSize( GetStdHandle(STD_OUTPUT_HANDLE),BufferSize);
	return 0;
}

int   game::Welcome()
{
	for(int i=0;i<20;i++)
			std::cout<<std::endl;
		std::cout<<"\t\t\t\t"<<"欢迎进入 "<<std::endl;
		std::cout<<"\t\t\t\t"<<"   "<<std::endl;
		std::cout<<"\t\t\t\t"<<"  "<<std::endl;
	
		Sleep(100);
	system("cls");
	return 0;
}


	int game::select_cos()
	{
		char command = 0;
		system("cls");
		for(int i=0;i<20;i++)
			std::cout<<std::endl;
		std::cout<<"\t\t\t\t"<<"请选择关卡: "<<std::endl;
		std::cout<<"\t\t\t\t"<<"  1. 第一关 "<<std::endl;
		std::cout<<"\t\t\t\t"<<"  2. 第二关 "<<std::endl;
		while (true)
		{

			if (_kbhit()) 
				command = _getch();
			if(command == '1' )
			{
				MAP2DATA(0);
				return  1;
			}
			if(command == '2' )
			{
				
				MAP2DATA(1);
				return  2;
			}
		}

	}
	int game::select_player()
	{
		char command = 0;
		system("cls");
		for(int i=0;i<20;i++)
			std::cout<<std::endl;
		std::cout<<"\t\t\t\t"<<"请选择玩家数量: "<<std::endl;
		std::cout<<"\t\t\t\t"<<"  1. 一个人 "<<std::endl;
		std::cout<<"\t\t\t\t"<<"  2. 俩人玩 "<<std::endl;
		while (true)
		{

			if (_kbhit()) 
				command = _getch();
			if(command == '1' )
			{
				oneplayer();
				return  1;
			}
			if(command == '2' )
			{
				twoplayer();
				return  2;
			}
		}
	}
